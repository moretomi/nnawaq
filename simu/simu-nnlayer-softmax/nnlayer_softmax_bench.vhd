
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.all;

entity nnlayer_softmax_bench is
	generic (

		WDATA   : natural := 8;
		SDATA   : boolean := true;
		WOUT    : natural := 8;
		-- Frame size
		FSIZE   : natural := 4;
		-- Parameters for input and output parallelism
		PAR_IN  : natural := 1;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED  : boolean := false;

		-- Enable insertion of delay from input/output interfaces
		EN_DELAY_IN : boolean := false;
		DELAY_OUT_DATE : integer := 0;
		-- Number of images to simulate
		FRAMES : natural := 10;
		-- Output text file for simulation results
		OUTPUT_FILE : string := "simu_output.txt"
	);
end nnlayer_softmax_bench;

architecture simu of nnlayer_softmax_bench is

	signal clk           : std_logic := '0';
	signal clk_next      : std_logic := '0';
	signal clear         : std_logic := '1';
	signal clk_want_stop : boolean := false;

	-- Run-time frame dimensions
	signal user_fsize    : std_logic_vector(WOUT-1 downto 0) := std_logic_vector(to_unsigned(FSIZE, WOUT));

	-- Data input
	signal in_data       : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal in_rdy        : std_logic := '0';
	signal in_ack        : std_logic := '0';

	-- Data output
	signal out_data      : std_logic_vector(WOUT-1 downto 0) := (others => '0');
	signal out_rdy       : std_logic := '0';
	signal out_fifo_room : std_logic_vector(15 downto 0) := (others => '0');

	-- Testbench only, to debug erroneous output vectors in waveforms
	signal tb_out_error         : boolean := false;
	signal tb_out_count_all     : integer := 0;
	signal tb_out_count_inframe : integer := 0;

	-- The pre-generated activations

	type arract_type is array (0 to FRAMES-1, 0 to FSIZE*PAR_IN-1) of std_logic_vector(WDATA-1 downto 0);

	function init_arracts_conv_int(v : integer) return std_logic_vector is
		variable vec : std_logic_vector(WDATA-1 downto 0);
	begin
		vec :=
			std_logic_vector(to_unsigned(v, WDATA)) when SDATA = false else
			std_logic_vector(  to_signed(v, WDATA));
		return vec;
	end function;

	function init_arracts(phony : boolean) return arract_type is
		variable var_sub : integer;
		variable idx_pos : integer;
		variable arr : arract_type;
	begin
		-- Signed data with get a value subtracted to get centering around zero
		var_sub := 0;
		if SDATA = true then
			var_sub := FSIZE*PAR_IN/2;
		end if;
		-- Position of injection of the max
		idx_pos := 0;
		-- Input values are : fr*FSIZE+f, where fr is the frame index and f the position in the frame
		for fr in 0 to FRAMES-1 loop
			-- Fill most of the frame
			for f in 0 to FSIZE*PAR_IN-1 loop
				if fr mod 2 = 0 then
					-- Add a frame in increasing order
					arr(fr, f) := init_arracts_conv_int(f - var_sub);
				else
					-- Add a frame in decreasing order
					arr(fr, f) := init_arracts_conv_int( (FSIZE*PAR_IN-1 - f) - var_sub );
				end if;
			end loop;
			-- Override the desired max position
			arr(fr, idx_pos) := init_arracts_conv_int(FSIZE*PAR_IN-1 - var_sub + 1);
			-- Increment the max position and wrap around
			if fr mod 2 /= 0 then
				idx_pos := idx_pos + 1;
				if idx_pos = FSIZE*PAR_IN then
					idx_pos := 0;
				end if;
			end if;
		end loop;
		return arr;
	end function;

	constant arract : arract_type := init_arracts(true);

	-- Component declaration

	component nnlayer_softmax is
		generic(
			WDATA : natural := 8;
			SDATA : boolean := false;
			WOUT  : natural := 8;
			FSIZE : natural := 1024;
			PAR_IN : natural := 1;
			-- Take extra margin on the FIFO level, in case there is something outside
			FIFOMARGIN : natural := 0;
			-- Lock the layer parameters to the generic parameter value
			LOCKED : boolean := false
		);
		port(
			clk            : in  std_logic;
			clear          : in  std_logic;
			-- The user-specified frame size
			user_fsize     : in  std_logic_vector(WOUT-1 downto 0);
			-- Data input
			data_in        : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
			data_in_valid  : in  std_logic;
			data_in_ready  : out std_logic;
			-- Data output
			data_out       : out std_logic_vector(WOUT-1 downto 0);
			data_out_valid : out std_logic;
			-- The output data enters a FIFO. This indicates the available room.
			out_fifo_room  : in  std_logic_vector(15 downto 0)
		);
	end component;

begin

	softmax : nnlayer_softmax
		generic map (
			WDATA   => WDATA,
			SDATA   => SDATA,
			WOUT    => WOUT,
			FSIZE   => FSIZE,
			PAR_IN  => PAR_IN,
			-- Take extra margin on the FIFO level, in case there is something outside
			FIFOMARGIN => FIFOMARGIN,
			-- Lock the layer parameters to the generic parameter value
			LOCKED  => LOCKED
		)
		port map (
			clk            => clk,
			clear          => clear,
			-- Frame size
			user_fsize     => user_fsize,
			-- Data input
			data_in        => in_data,
			data_in_valid  => in_ack,
			data_in_ready  => in_rdy,
			-- Data output
			data_out       => out_data,
			data_out_valid => out_rdy,
			out_fifo_room  => out_fifo_room
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- The user-specified frame size and number of neurons
	user_fsize <= std_logic_vector(to_unsigned(FSIZE, user_fsize'length));

	-- Process that generates stimuli
	process
		variable var_vec   : std_logic_vector(31 downto 0);
		variable var_wrnb  : integer;
		variable var_total : integer;
	begin

		-- Clear
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		clear <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Send frame data
		-- Input values are : fr*FSIZE+f, where fr is the frame index and f the position in the frame

		for fr in 0 to FRAMES-1 loop
			report "# Sending frame " & to_string(fr);

			for f in 0 to FSIZE-1 loop

				-- Prepare the input vector
				in_data <= (others => '0');
				for pi in 0 to PAR_IN-1 loop
					in_data((pi+1)*WDATA-1 downto pi*WDATA) <= arract(fr, f*PAR_IN + pi);
				end loop;

				-- Add extra wait delay on input side, arbitrary
				var_total := fr / 3;
				if EN_DELAY_IN = true and var_total >= 1 then
					for i in 0 to var_total-1 loop
						wait until rising_edge(clk);
					end loop;
				end if;

				-- Send the acknowledge
				in_ack <= '1';
				wait until rising_edge(clk) and (in_rdy = '1');
				in_ack <= '0';

			end loop;  -- fsize
		end loop;  -- frames

		in_ack <= '0';

		report "# All input data is sent";

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Wait for end of simulation
		wait;

	end process;

	-- Process that generates the out_fifo_room signal
	process
		variable clk_cnt : integer;
	begin

		-- Enable data output
		out_fifo_room <= std_logic_vector(to_unsigned(64, out_fifo_room'length));

		-- Wait indefinitely if no interruption is scheduled
		if DELAY_OUT_DATE = 0 then
			wait;
		end if;

		-- Wait until end of reset
		wait until rising_edge(clk);
		wait until clear = '0';
		wait until rising_edge(clk);

		clk_cnt := 0;

		-- Wait until the scheduled beginning of the output interruption
		while clk_cnt < DELAY_OUT_DATE loop
			clk_cnt := clk_cnt + 1;
			wait until rising_edge(clk);
		end loop;

		-- Emulate an output fifo that is full
		out_fifo_room <= std_logic_vector(to_unsigned(0, out_fifo_room'length));
		for j in 0 to 30 loop
			wait until rising_edge(clk);
		end loop;

		-- Back to no limit
		out_fifo_room <= std_logic_vector(to_unsigned(64, out_fifo_room'length));

		-- Wait for end of simulation
		wait;

	end process;

	-- Process that reads outputs
	process
		file data_outfile : text open write_mode is OUTPUT_FILE;
		variable out_line : line;
		variable get_out_val : integer;
		variable want_out_val : integer;
		variable want_out_max : integer;  -- Not reported, just temp var
		variable val_act : integer;
	begin

		-- Wait until end of reset
		wait until rising_edge(clk);
		wait until clear = '0';
		wait until rising_edge(clk);

		tb_out_count_all <= 0;

		for fr in 0 to FRAMES-1 loop
			report "# Receiving frame " & to_string(fr);
			write(out_line, string'("# Beginning of frame ") & to_string(fr));
			writeline(data_outfile, out_line);

			tb_out_count_inframe <= 0;

			-- Wait for a valid output vector
			wait until rising_edge(clk) and (out_rdy = '1');

			-- Generate the expected output vector
			for f in 0 to FSIZE*PAR_IN-1 loop
				val_act :=
					to_integer(unsigned(arract(fr, f))) when SDATA = false else
					to_integer(  signed(arract(fr, f)));
				if (f = 0) or (val_act > want_out_max) then
					want_out_max := val_act;
					want_out_val := f;
				end if;
			end loop;

			-- Get the output vector
			get_out_val := to_integer(unsigned(out_data));

			-- Emit an error message
			tb_out_error <= false;
			if get_out_val /= want_out_val then
				tb_out_error <= true;
				report "ERROR Got " & to_string(get_out_val) & " expected " & to_string(want_out_val) & " for frame " & to_string(fr);
			end if;

			-- Write output data to file
			write(out_line, get_out_val);
			if get_out_val /= want_out_val then
				write(out_line, string'(" ERROR expected "));
				write(out_line, want_out_val);
			end if;
			writeline(data_outfile, out_line);

			-- Update counter signals to visualize in waveforms
			tb_out_count_all     <= tb_out_count_all + 1;
			tb_out_count_inframe <= tb_out_count_inframe + 1;

		end loop;  -- frame

		-- Write end message to the output file
		write(out_line, string'("# End of simulation"));
		writeline(data_outfile, out_line);

		-- End of simulation
		report "All output data is received";
		report "Stopping simulation";
		clk_want_stop <= true;
		wait;

	end process;

end architecture;

