
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.all;

entity nnlayer_pooling_bench is
	generic (

		WDATA   : natural := 8;
		SDATA   : boolean := true;
		WOUT    : natural := 8;
		-- Frame size and number of units
		FSIZE   : natural := 4;
		NPOOL   : natural := 1;
		-- The type of pooling
		OPMAX   : boolean := true;
		OPMIN   : boolean := false;
		OPADD   : boolean := false;
		-- Parameters for Average pooling
		MULT    : natural := 1;
		SHR     : natural := 0;
		-- Activate rounding to nearest integer (default is rounding is towards zero)
		ROUND_NEAR : boolean := false;
		-- Parameters for input and output parallelism
		PAR_IN  : natural := 1;
		PAR_OUT : natural := 1;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED  : boolean := false;

		-- Enable insertion of delay from input/output interfaces
		EN_DELAY_IN : boolean := false;
		DELAY_OUT_DATE : integer := 0;
		-- Number of images to simulate
		FRAMES : natural := 10;
		-- Output text file for simulation results
		OUTPUT_FILE : string := "simu_output.txt"
	);
end nnlayer_pooling_bench;

architecture simu of nnlayer_pooling_bench is

	signal clk           : std_logic := '0';
	signal clk_next      : std_logic := '0';
	signal clear         : std_logic := '1';
	signal clk_want_stop : boolean := false;

	-- Run-time frame dimensions
	signal user_fsize    : std_logic_vector(15 downto 0) := (others => '0');

	-- Data input
	signal in_data       : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal in_rdy        : std_logic := '0';
	signal in_ack        : std_logic := '0';

	-- Data output
	signal out_data      : std_logic_vector(PAR_OUT*WOUT-1 downto 0) := (others => '0');
	signal out_rdy       : std_logic := '0';
	signal out_fifo_room : std_logic_vector(15 downto 0) := (others => '0');

	-- The number of parallel inputs per pooling path
	constant TOTAL_NPOOL : natural := PAR_OUT * NPOOL;

	-- The number of parallel inputs per pooling path
	constant POOL_PAR_IN : natural := PAR_IN / TOTAL_NPOOL;

	-- Testbench only, to debug erroneous output vectors in waveforms
	signal tb_out_error         : boolean := false;
	signal tb_out_count_all     : integer := 0;
	signal tb_out_count_inframe : integer := 0;

	-- The pre-generated activations

	type arract_type is array (0 to FRAMES-1, 0 to FSIZE*PAR_IN-1) of std_logic_vector(WDATA-1 downto 0);

	function init_arracts(phony : boolean) return arract_type is
		variable arr : arract_type;
	begin
		-- Input values are : fr*FSIZE+f, where fr is the frame index and f the position in the frame
		for fr in 0 to FRAMES-1 loop
			for f in 0 to FSIZE*PAR_IN-1 loop
				arr(fr, f) := std_logic_vector(resize(to_unsigned(fr*FSIZE*PAR_IN + f, 32), WDATA));
			end loop;
		end loop;
		return arr;
	end function;

	constant arract : arract_type := init_arracts(true);

	-- Component declaration

	component nnlayer_pooling is
		generic(
			WDATA   : natural := 8;
			SDATA   : boolean := true;
			WOUT    : natural := 8;
			-- Frame size and number of units
			FSIZE   : natural := 4;
			NPOOL   : natural := 1;
			-- The type of pooling
			OPMAX   : boolean := true;
			OPMIN   : boolean := false;
			OPADD   : boolean := false;
			-- Parameters for Average pooling
			MULT    : natural := 1;
			SHR     : natural := 0;
			-- Parameters for input and output parallelism
			PAR_IN  : natural := 1;
			PAR_OUT : natural := 1;
			-- Take extra margin on the FIFO level, in case there is something outside
			FIFOMARGIN : natural := 0;
			-- Lock the layer parameters to the generic parameter value
			LOCKED  : boolean := false
		);
		port(
			clk           : in  std_logic;
			clear         : in  std_logic;
			-- Run-time frame dimensions
			user_fsize    : in  std_logic_vector(15 downto 0);
			-- Data input
			in_data       : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
			in_rdy        : out std_logic;
			in_ack        : in  std_logic;
			-- Data output
			out_data      : out std_logic_vector(PAR_OUT*WOUT-1 downto 0);
			out_rdy       : out std_logic;
			out_fifo_room : in  std_logic_vector(15 downto 0)
		);
	end component;

begin

	pool : nnlayer_pooling
		generic map (
			WDATA   => WDATA,
			SDATA   => SDATA,
			WOUT    => WOUT,
			-- Frame size and number of units
			FSIZE   => FSIZE,
			NPOOL   => NPOOL,
			-- The type of pooling
			OPMAX   => OPMAX,
			OPMIN   => OPMIN,
			OPADD   => OPADD,
			-- Parameters for Average pooling
			MULT    => MULT,
			SHR     => SHR,
			-- Parameters for input and output parallelism
			PAR_IN  => PAR_IN,
			PAR_OUT => PAR_OUT,
			-- Take extra margin on the FIFO level, in case there is something outside
			FIFOMARGIN => FIFOMARGIN,
			-- Lock the layer parameters to the generic parameter value
			LOCKED  => LOCKED
		)
		port map (
			clk           => clk,
			clear         => clear,
			-- Frame size
			user_fsize    => user_fsize,
			-- Data input
			in_data       => in_data,
			in_rdy        => in_rdy,
			in_ack        => in_ack,
			-- Data output
			out_data      => out_data,
			out_rdy       => out_rdy,
			out_fifo_room => out_fifo_room
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- The user-specified frame size and number of neurons
	user_fsize <= std_logic_vector(to_unsigned(FSIZE, user_fsize'length));

	-- Process that generates stimuli
	process
		variable var_vec   : std_logic_vector(31 downto 0);
		variable var_wrnb  : integer;
		variable var_total : integer;
	begin

		-- Clear
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		clear <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Send frame data
		-- Input values are : fr*FSIZE+f, where fr is the frame index and f the position in the frame

		for fr in 0 to FRAMES-1 loop
			report "# Sending frame " & to_string(fr);

			for f in 0 to FSIZE-1 loop

				-- Prepare the input vector
				in_data <= (others => '0');
				for pi in 0 to PAR_IN-1 loop
					in_data((pi+1)*WDATA-1 downto pi*WDATA) <= arract(fr, f*PAR_IN + pi);
				end loop;

				-- Add extra wait delay on input side, arbitrary
				var_total := fr / 3;
				if EN_DELAY_IN = true and var_total >= 1 then
					for i in 0 to var_total-1 loop
						wait until rising_edge(clk);
					end loop;
				end if;

				-- Send the acknowledge
				in_ack <= '1';
				wait until rising_edge(clk) and (in_rdy = '1');
				in_ack <= '0';

			end loop;  -- fsize
		end loop;  -- frames

		in_ack <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Wait for end of simulation
		wait;

	end process;

	-- Process that generates the out_fifo_room signal
	process
		variable clk_cnt : integer;
	begin

		-- Enable data output
		out_fifo_room <= std_logic_vector(to_unsigned(64, out_fifo_room'length));

		-- Wait indefinitely if no interruption is scheduled
		if DELAY_OUT_DATE = 0 then
			wait;
		end if;

		-- Wait until end of reset
		wait until rising_edge(clk);
		wait until clear = '0';
		wait until rising_edge(clk);

		clk_cnt := 0;

		-- Wait until the scheduled beginning of the output interruption
		while clk_cnt < DELAY_OUT_DATE loop
			clk_cnt := clk_cnt + 1;
			wait until rising_edge(clk);
		end loop;

		-- Emulate an output fifo that is full
		out_fifo_room <= std_logic_vector(to_unsigned(0, out_fifo_room'length));
		for j in 0 to 30 loop
			wait until rising_edge(clk);
		end loop;

		-- Back to no limit
		out_fifo_room <= std_logic_vector(to_unsigned(64, out_fifo_room'length));

		-- Wait for end of simulation
		wait;

	end process;

	-- Process that reads outputs
	process
		file data_outfile : text open write_mode is OUTPUT_FILE;
		variable out_line : line;
		variable get_out_vec : std_logic_vector(WOUT-1 downto 0);
		variable get_out_val : integer;
		variable want_out_val : integer;
		variable val_act : integer;
		variable vec_n : integer;
		variable vec_f : integer;
	begin

		-- Wait until end of reset
		wait until rising_edge(clk);
		wait until clear = '0';
		wait until rising_edge(clk);

		tb_out_count_all <= 0;

		for fr in 0 to FRAMES-1 loop
			report "# Receiving frame " & to_string(fr);
			write(out_line, string'("# Beginning of frame ") & to_string(fr));
			writeline(data_outfile, out_line);

			tb_out_count_inframe <= 0;

			for n in 0 to NPOOL-1 loop

				-- Wait for a valid output vector
				wait until rising_edge(clk) and (out_rdy = '1');

				for po in 0 to PAR_OUT-1 loop

					-- Generate the expected output vector
					want_out_val := 0;
					for f in 0 to FSIZE-1 loop
						for pi in 0 to POOL_PAR_IN-1 loop

							-- Compute the index of the pooling unit
							-- Compute the position in the vector of inputs
							vec_n := n*PAR_OUT + po;
							vec_f := f*PAR_IN + pi*TOTAL_NPOOL + vec_n;

							val_act :=
								to_integer(unsigned(arract(fr, vec_f))) when SDATA = false else
								to_integer(  signed(arract(fr, vec_f)));

							if f = 0 and pi = 0 then
								want_out_val := val_act;
							elsif OPMAX = true then
								want_out_val := maximum(want_out_val, val_act);
							elsif OPMIN = true then
								want_out_val := minimum(want_out_val, val_act);
							elsif OPADD = true then
								want_out_val := want_out_val + val_act;
							end if;

						end loop;
					end loop;

					if MULT > 1 then
						want_out_val := want_out_val * MULT;
					end if;
					if SHR > 0 then
						if ROUND_NEAR = true then
							want_out_val := integer(real(want_out_val) / real(2 ** SHR));
						else
							want_out_val := want_out_val / (2 ** SHR);
						end if;
					end if;

					-- Select the right part of the output vector
					get_out_vec := out_data((po+1)*WOUT-1 downto po*WOUT);
					get_out_val := to_integer(unsigned(get_out_vec)) when SDATA = false else to_integer(signed(get_out_vec));

					-- Emit an error message
					tb_out_error <= false;
					if get_out_val /= want_out_val then
						tb_out_error <= true;
						report "ERROR Got " & to_string(get_out_val) & " expected " & to_string(want_out_val) &
							" for frame " & to_string(fr) & " n " & to_string(n) & " po " & to_string(po);
					end if;

					-- Write output data to file
					write(out_line, get_out_val);
					if get_out_val /= want_out_val then
						write(out_line, string'(" ERROR expected "));
						write(out_line, want_out_val);
					end if;
					writeline(data_outfile, out_line);

					-- Update counter signals to visualize in waveforms
					tb_out_count_all     <= tb_out_count_all + 1;
					tb_out_count_inframe <= tb_out_count_inframe + 1;

				end loop;  -- par_out

			end loop;  -- nbneu

		end loop;  -- frame

		-- Write end message to the output file
		write(out_line, string'("# End of simulation"));
		writeline(data_outfile, out_line);

		-- End of simulation
		report "All output data is received";
		report "Stopping simulation";
		clk_want_stop <= true;
		wait;

	end process;

end architecture;

