-- Author : Adrien Prost-Boucle <adrien.prost-boucle@laposte.net>
-- This file is distributed under the GPLv3 license
-- License information : https://www.gnu.org/licenses/gpl-3.0.txt

-- This package provides functionality for a few Xilinx unisim components
-- It is meant to be compiled in library "unisim" to simulate and synthesize outside Vivado

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package vcomponents is

	component LUT6_2 is
		generic (
			INIT : bit_vector
		);
		port (
			O6 : out std_logic;
			O5 : out std_logic;
			I0 : in  std_logic;
			I1 : in  std_logic;
			I2 : in  std_logic;
			I3 : in  std_logic;
			I4 : in  std_logic;
			I5 : in  std_logic
		);
	end component;

	component LUT6 is
		generic (
			INIT : bit_vector
		);
		port (
			O  : out std_logic;
			I0 : in  std_logic;
			I1 : in  std_logic;
			I2 : in  std_logic;
			I3 : in  std_logic;
			I4 : in  std_logic;
			I5 : in  std_logic
		);
	end component;

	component LUT5 is
		generic (
			INIT : bit_vector
		);
		port (
			O  : out std_logic;
			I0 : in  std_logic;
			I1 : in  std_logic;
			I2 : in  std_logic;
			I3 : in  std_logic;
			I4 : in  std_logic
		);
	end component;

	component CARRY4 is
		port (
			CO     : out std_logic_vector(3 downto 0);
			O      : out std_logic_vector(3 downto 0);
			DI     : in  std_logic_vector(3 downto 0);
			S      : in  std_logic_vector(3 downto 0);
			CI     : in  std_logic;
			CYINIT : in  std_logic
		);
	end component;

end package;

