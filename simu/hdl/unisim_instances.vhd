-- Author : Adrien Prost-Boucle <adrien.prost-boucle@laposte.net>
-- This file is distributed under the GPLv3 license
-- License information : https://www.gnu.org/licenses/gpl-3.0.txt

-- This package provides functionality for a few Xilinx unisim components
-- It is meant to be compiled in library "unisim" to simulate outside Vivado

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LUT6_2 is
	generic (
		INIT : bit_vector
	);
	port (
		O6 : out std_logic;
		O5 : out std_logic;
		I0 : in  std_logic;
		I1 : in  std_logic;
		I2 : in  std_logic;
		I3 : in  std_logic;
		I4 : in  std_logic;
		I5 : in  std_logic
	);
end LUT6_2;

architecture simu of LUT6_2 is
	constant conf : std_logic_vector(63 downto 0) := to_stdlogicvector(INIT);
	signal   addr : unsigned(5 downto 0) := (others => '0');
begin

	addr(5) <= I5;
	addr(4) <= I4;
	addr(3) <= I3;
	addr(2) <= I2;
	addr(1) <= I1;
	addr(0) <= I0;

	O6 <= conf(to_integer(addr(5 downto 0)));
	O5 <= conf(to_integer(addr(4 downto 0)));

end architecture;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LUT6 is
	generic (
		INIT : bit_vector
	);
	port (
		O  : out std_logic;
		I0 : in  std_logic;
		I1 : in  std_logic;
		I2 : in  std_logic;
		I3 : in  std_logic;
		I4 : in  std_logic;
		I5 : in  std_logic
	);
end LUT6;

architecture simu of LUT6 is
	constant conf : std_logic_vector(63 downto 0) := to_stdlogicvector(INIT);
	signal   addr : unsigned(5 downto 0) := (others => '0');
begin

	addr(5) <= I5;
	addr(4) <= I4;
	addr(3) <= I3;
	addr(2) <= I2;
	addr(1) <= I1;
	addr(0) <= I0;

	O <= conf(to_integer(addr));

end architecture;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity LUT5 is
	generic (
		INIT : bit_vector
	);
	port (
		O  : out std_logic;
		I0 : in  std_logic;
		I1 : in  std_logic;
		I2 : in  std_logic;
		I3 : in  std_logic;
		I4 : in  std_logic
	);
end LUT5;

architecture simu of LUT5 is
	constant conf : std_logic_vector(31 downto 0) := to_stdlogicvector(INIT);
	signal   addr : unsigned(4 downto 0) := (others => '0');
begin

	addr(4) <= I4;
	addr(3) <= I3;
	addr(2) <= I2;
	addr(1) <= I1;
	addr(0) <= I0;

	O <= conf(to_integer(addr));

end architecture;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CARRY4 is
	port (
		CO     : out std_logic_vector(3 downto 0);
		O      : out std_logic_vector(3 downto 0);
		DI     : in  std_logic_vector(3 downto 0);
		S      : in  std_logic_vector(3 downto 0);
		CI     : in  std_logic;
		CYINIT : in  std_logic
	);
end CARRY4;

architecture simu of CARRY4 is
begin

	process(all)
		variable c : std_logic := '0';
	begin
		c := CI or CYINIT;
		for i in 0 to 3 loop
			O(i) <= S(i) xor c;
			c := (DI(i) and not S(i)) or (c and S(i));
			CO(i) <= c;
		end loop;
	end process;

end architecture;


