-- Author : Adrien Prost-Boucle <adrien.prost-boucle@laposte.net>
-- This file is distributed under the GPLv3 license
-- License information : https://www.gnu.org/licenses/gpl-3.0.txt

-- This package provides functionality for a few Xilinx unimacro components
-- It is meant to be compiled in library "unimacro" to simulate and synthesize outside Vivado

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package vcomponents is

	component BRAM_SDP_MACRO is
		generic (
			BRAM_SIZE   : string;    -- Target BRAM, 18Kb or 36Kb
			DEVICE      : string;    -- Target device: VIRTEX5, VIRTEX6, 7SERIES, SPARTAN6
			WRITE_WIDTH : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
			READ_WIDTH  : integer;   -- Valid values are 1-72 (37-72 only valid when BRAM_SIZE = 36Kb)
			DO_REG      : integer;   -- Optional output register (0 or 1)
			-- Set/Reset value for port output
			SRVAL       : bit_vector;
			-- Specify READ_FIRST for same clock or synchronous clocks, or WRITE_FIRST for asynchrononous clocks on ports
			WRITE_MODE  : string;
			-- Initial values on output port
			INIT        : bit_vector;
			-- Initial values when stored in a file
			INIT_FILE   : string
		);
		port (
			-- Output read data port, width defined by READ_WIDTH parameter
			DO     : out std_logic_vector;
			-- Input write data port, width defined by WRITE_WIDTH parameter
			DI     : in std_logic_vector;
			-- Input read address, width defined by read port depth
			RDADDR : in std_logic_vector;
			-- 1-bit input read clock
			RDCLK  : in std_logic;
			-- 1-bit input read port enable
			RDEN   : in std_logic;
			-- 1-bit input read output register clock enable
			REGCE  : in std_logic;
			-- 1-bit input reset
			RST    : in std_logic;
			-- Input write enable, width defined by write port depth
			WE     : in std_logic_vector;
			-- Input write address, width defined by write port depth
			WRADDR : in std_logic_vector;
			-- 1-bit input write clock
			WRCLK  : in std_logic;
			-- 1-bit input write port enable
			WREN   : in std_logic
		);
	end component;

end package;

