
################################
# Testcase-specific variables
################################

TOPNAME = nnlayer_window_bench

HDLDIR_BENCH = .

# Need Vivado simulation files for BRAM macros
UNISIM_DIR   = ../unisim-vivado
UNIMACRO_DIR = ../unimacro-vivado

GHDLGENERIC =

#GHDLSIMUFLAGS =
GHDLSIMUFLAGS = --stop-time=20us

WAVEFILE = wave.ghw

################################
# Most compilation and simulation recipes are in common Makefile
################################

include ../Makefile.inc

################################
# Dedicated simulation contexts
################################

# Note : To debug a simulation and have the waveforms, set the variable TARGET_WAVE=simu-wave
# Example : make TARGET_WAVE=simu-wave XILINX_VIVADO=<path> simu-f1n1
TARGET_WAVE ?= simu

# Simple tests, without padding and with padding

simu1 : simu1-output.txt
simu1-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gNWINX=2 -gNWINY=2 -gPADX=0 -gPADY=0 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu2 : simu2-output.txt
simu2-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gNWINX=4 -gNWINY=4 -gPADX=1 -gPADY=1 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Tests with window 1x1

simu1x1 : simu1x1-output.txt
simu1x1-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=4 -gDIMY=4 -gDIMZ=3 -gWINX=1 -gWINY=1 -gNWINX=4 -gNWINY=4 -gNWINZ=3 -gPADX=0 -gPADY=0 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Tests with window 7x7 and nwin 1x1

simu3x3 : simu3x3-output.txt
simu3x3-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=3 -gDIMY=3 -gDIMZ=3 -gWINX=3 -gWINY=3 -gNWINX=1 -gNWINY=1 -gNWINZ=3 -gPADX=0 -gPADY=0 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Test with different buffer size

simu3 : simu3-output.txt
simu3-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gNWINX=4 -gNWINY=4 -gPADX=1 -gPADY=1 -gBUFY=2 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu4 : simu4-output.txt
simu4-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gNWINX=4 -gNWINY=4 -gPADX=1 -gPADY=1 -gBUFY=4 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu32x32x3 : simu32x32x3-output.txt
simu32x32x3-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=32 -gDIMY=32 -gDIMZ=3 -gWINX=3 -gWINY=3 -gNWINX=32 -gNWINY=32 -gNWINZ=3 -gPADX=1 -gPADY=1 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" GHDLSIMUFLAGS="--stop-time=5000us" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Test with LOCKED

simu5 : simu5-output.txt
simu5-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gNWINX=4 -gNWINY=4 -gPADX=1 -gPADY=1 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu32x32x3-lock : simu32x32x3-lock-output.txt
simu32x32x3-lock-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=32 -gDIMY=32 -gDIMZ=3 -gWINX=3 -gWINY=3 -gNWINX=32 -gNWINY=32 -gNWINZ=3 -gPADX=1 -gPADY=1 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" GHDLSIMUFLAGS="--stop-time=5000us" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Test with PAR_OUT

simu8x8x9-p3 : simu8x8x9-p3-output.txt
simu8x8x9-p3-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=8 -gDIMY=8 -gDIMZ=3 -gWINX=3 -gWINY=3 -gNWINX=8 -gNWINY=8 -gNWINZ=3 -gPADX=1 -gPADY=1 -gPAR_IN=1 -gPAR_OUT=3 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" GHDLSIMUFLAGS="--stop-time=5000us" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu8x8x9-p9 : simu8x8x9-p9-output.txt
simu8x8x9-p9-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=8 -gDIMY=8 -gDIMZ=3 -gWINX=3 -gWINY=3 -gNWINX=8 -gNWINY=8 -gNWINZ=3 -gPADX=1 -gPADY=1 -gPAR_IN=1 -gPAR_OUT=9 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" GHDLSIMUFLAGS="--stop-time=5000us" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Test with PAR_OUT and PAR_OZ

simu8x8x9-p3z3 : simu8x8x9-p3z3-output.txt
simu8x8x9-p3z3-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=8 -gDIMY=8 -gDIMZ=3 -gWINX=3 -gWINY=3 -gNWINX=8 -gNWINY=8 -gNWINZ=3 -gPADX=1 -gPADY=1 -gPAR_IN=1 -gPAR_OUT=3 -gPAR_OZ=3 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" GHDLSIMUFLAGS="--stop-time=5000us" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu8x8x9-p9z3 : simu8x8x9-p9z3-output.txt
simu8x8x9-p9z3-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=8 -gDIMY=8 -gDIMZ=3 -gWINX=3 -gWINY=3 -gNWINX=8 -gNWINY=8 -gNWINZ=3 -gPADX=1 -gPADY=1 -gPAR_IN=1 -gPAR_OUT=9 -gPAR_OZ=3 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" GHDLSIMUFLAGS="--stop-time=5000us" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Test with PAR_IN

simu8x8x9-p3z3i3 : simu8x8x9-p3z3i3-output.txt
simu8x8x9-p3z3i3-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=8 -gDIMY=8 -gDIMZ=3 -gWINX=3 -gWINY=3 -gNWINX=8 -gNWINY=8 -gNWINZ=3 -gPADX=1 -gPADY=1 -gPAR_IN=3 -gPAR_OUT=3 -gPAR_OZ=3 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" GHDLSIMUFLAGS="--stop-time=5000us" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu8x8x9-p9z3i3 : simu8x8x9-p9z3i3-output.txt
simu8x8x9-p9z3i3-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=8 -gDIMY=8 -gDIMZ=3 -gWINX=3 -gWINY=3 -gNWINX=8 -gNWINY=8 -gNWINZ=3 -gPADX=1 -gPADY=1 -gPAR_IN=3 -gPAR_OUT=9 -gPAR_OZ=3 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" GHDLSIMUFLAGS="--stop-time=5000us" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Test with PAR_IN and max PAR_OUT for a 1-cycle read of each window

simu8x8x9-p81z9i9 : simu8x8x9-p81z9i9-output.txt
simu8x8x9-p81z9i9-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gDIMX=8 -gDIMY=8 -gDIMZ=1 -gWINX=3 -gWINY=3 -gNWINX=8 -gNWINY=8 -gNWINZ=3 -gPADX=1 -gPADY=1 -gPAR_IN=9 -gPAR_OUT=81 -gPAR_OZ=9 -gLOCKED=true -gFRAMES=15 -gOUTPUT_FILE=\"$@\"" GHDLSIMUFLAGS="--stop-time=5000us" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Tests with repeat

simu-repeat2 : simu-repeat2-output.txt
simu-repeat2-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gNWINX=2 -gNWINY=2 -gPADX=0 -gPADY=0 -gREPEAT=2 -gLOCKED=false -gFRAMES=2 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-repeat3 : simu-repeat3-output.txt
simu-repeat3-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gNWINX=2 -gNWINY=2 -gPADX=0 -gPADY=0 -gREPEAT=3 -gLOCKED=false -gFRAMES=2 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Launch all tests

simus-all :
	$(MAKE) simu1
	$(MAKE) simu2
	$(MAKE) simu1x1
	$(MAKE) simu3x3
	$(MAKE) simu3
	$(MAKE) simu4
	$(MAKE) simu5
	$(MAKE) simu32x32x3
	$(MAKE) simu32x32x3-lock
	$(MAKE) simu8x8x9-p3
	$(MAKE) simu8x8x9-p9
	$(MAKE) simu8x8x9-p3z3
	$(MAKE) simu8x8x9-p9z3
	$(MAKE) simu8x8x9-p3z3i3
	$(MAKE) simu8x8x9-p9z3i3
	$(MAKE) simu8x8x9-p81z9i9
	$(MAKE) simu-repeat2
	$(MAKE) simu-repeat3

simus-clean :
	rm -f simu*output.txt

