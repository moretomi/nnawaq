
################################
# Testcase-specific variables
################################

TOPNAME = nnlayer_neurons_bench

HDLDIR_BENCH = .

# Need Vivado simulation files for BRAM macros
UNISIM_DIR   = ../unisim-vivado
UNIMACRO_DIR = ../unimacro-vivado

GHDLGENERIC =

#GHDLSIMUFLAGS =
GHDLSIMUFLAGS = --stop-time=10us

WAVEFILE = wave.ghw


################################
# Most compilation and simulation recipes are in common Makefile
################################

include ../Makefile.inc

################################
# Dedicated simulation contexts
################################

# Note : To debug a simulation and have the waveforms, set the variable TARGET_WAVE=simu-wave
# Example : make TARGET_WAVE=simu-wave XILINX_VIVADO=<path> simu-f1n1
TARGET_WAVE ?= simu

# Simple tests, no parallelism

simu-f1n1 : simu-f1n1-output.txt
simu-f1n1-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=1 -gNBNEU=1 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f1n1-lock : simu-f1n1-lock-output.txt
simu-f1n1-lock-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=1 -gNBNEU=1 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f2n2 : simu-f2n2-output.txt
simu-f2n2-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=2 -gNBNEU=2 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f2n2-lock : simu-f2n2-lock-output.txt
simu-f2n2-lock-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=2 -gNBNEU=2 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f4n8 : simu-f4n8-output.txt
simu-f4n8-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=4 -gNBNEU=8 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f8n4 : simu-f8n4-output.txt
simu-f8n4-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=8 -gNBNEU=4 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f8n8 : simu-f8n8-output.txt
simu-f8n8-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=8 -gNBNEU=8 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f8n8-lock : simu-f8n8-lock-output.txt
simu-f8n8-lock-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=8 -gNBNEU=8 -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# With input and output parallelism

simu-f1n1-p2i2 : simu-f1n1-p2i2-output.txt
simu-f1n1-p2i2-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=1 -gNBNEU=1 -gPAR_IN=2 -gPAR_OUT=2 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f1n1-p2i4 : simu-f1n1-p2i4-output.txt
simu-f1n1-p2i4-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=1 -gNBNEU=1 -gPAR_IN=4 -gPAR_OUT=2 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f1n1-p4i2 : simu-f1n1-p4i2-output.txt
simu-f1n1-p4i2-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=1 -gNBNEU=1 -gPAR_IN=2 -gPAR_OUT=4 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f2n2-p4i2 : simu-f2n2-p4i2-output.txt
simu-f2n2-p4i2-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=2 -gNBNEU=2 -gPAR_IN=2 -gPAR_OUT=4 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f4n4-p4i4 : simu-f4n4-p4i4-output.txt
simu-f4n4-p4i4-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=4 -gNBNEU=4 -gPAR_IN=4 -gPAR_OUT=4 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# With storage types BRAM, LUTRAM, REGS
# Note : BRAM is the default, has already been tested

simu-f2n2-p2i2-lut : simu-f2n2-p2i2-lut-output.txt
simu-f2n2-p2i2-lut-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=2 -gNBNEU=2 -gPAR_IN=2 -gPAR_OUT=2 -gUSE_LUTRAM=true -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# Note : Intentionally using locked mode here because otherwise the present simulation context actually passes a larger FSIZE
simu-f1n2-p2i2-reg : simu-f1n2-p2i2-reg-output.txt
simu-f1n2-p2i2-reg-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=1 -gNBNEU=2 -gPAR_IN=2 -gPAR_OUT=2 -gUSE_REGS=true -gLOCKED=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# With TIME_MUX=2 and more

simu-f4n2-p2i2-tmux2 : simu-f4n2-p2i2-tmux2-output.txt
simu-f4n2-p2i2-tmux2-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=4 -gNBNEU=2 -gPAR_IN=2 -gPAR_OUT=2 -gTIME_MUX=2 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

simu-f1n1-p2i2-tmux4 : simu-f1n1-p2i2-tmux4-output.txt
simu-f1n1-p2i2-tmux4-output.txt : $(COMPILE_TARGETS)
	$(MAKE) GHDLGENERIC="-gFSIZE=1 -gNBNEU=1 -gPAR_IN=2 -gPAR_OUT=2 -gTIME_MUX=4 -gLOCKED=false -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE)
	test $$(grep "ERROR" $@ | wc -l) -eq 0
	grep -q "End of simulation" $@

# With wait delays on input and output sides

simu-delay-in : simu-delay-in-output.txt
simu-delay-in-output.txt : $(COMPILE_TARGETS)
	echo "Errors found : one per line" > errors_found ; \
	for f in 1 2 3 4 ; do \
	for n in 1 2 3 4 ; do \
		$(MAKE) GHDLGENERIC="-gFSIZE=$$f -gNBNEU=$$n -gLOCKED=false -gEN_DELAY_IN=true -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE) ; \
		test $$(grep "ERROR" $@ | wc -l) -eq 0 || echo "1" >> errors_found ; \
		grep -q "End of simulation" $@ || echo "2" >> errors_found ; \
	done; \
	done; \
	test "$$(cat errors_found | wc -l)" -eq 1

simu-delay-out : simu-delay-out-output.txt
simu-delay-out-output.txt : $(COMPILE_TARGETS)
	echo "Errors found : one per line" > errors_found ; \
	for f in 1 2 3 4 ; do \
	for n in 1 2 3 4 ; do \
		$(MAKE) GHDLGENERIC="-gFSIZE=$$f -gNBNEU=$$n -gLOCKED=false -gDELAY_OUT_DATE=20 -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE) ; \
		test $$(grep "ERROR" $@ | wc -l) -eq 0 || echo "1" >> errors_found ; \
		grep -q "End of simulation" $@ || echo "2" >> errors_found ; \
	done; \
	done; \
	test "$$(cat errors_found | wc -l)" -eq 1

simu-delays : simu-delays-output.txt
simu-delays-output.txt : $(COMPILE_TARGETS)
	echo "Errors found : one per line" > errors_found ; \
	for f in 1 2 3 4 ; do \
	for n in 1 2 3 4 ; do \
		$(MAKE) GHDLGENERIC="-gFSIZE=$$f -gNBNEU=$$n -gLOCKED=false -gEN_DELAY_IN=true -gDELAY_OUT_DATE=20 -gFRAMES=10 -gOUTPUT_FILE=\"$@\"" $(TARGET_WAVE) ; \
		test $$(grep "ERROR" $@ | wc -l) -eq 0 || echo "1" >> errors_found ; \
		grep -q "End of simulation" $@ || echo "2" >> errors_found ; \
	done; \
	done; \
	test "$$(cat errors_found | wc -l)" -eq 1



# TODO With unsigned and signed data
# TODO With binary weights / act, ternary weights / act, ternary compression



# Launch all tests

simus-all :
	# Simple tests
	$(MAKE) simu-f1n1
	$(MAKE) simu-f1n1-lock
	$(MAKE) simu-f2n2
	$(MAKE) simu-f2n2-lock
	$(MAKE) simu-f4n8
	$(MAKE) simu-f8n4
	$(MAKE) simu-f8n8
	$(MAKE) simu-f8n8-lock
	# With parallelism
	$(MAKE) simu-f1n1-p2i2
	$(MAKE) simu-f1n1-p2i4
	$(MAKE) simu-f1n1-p4i2
	$(MAKE) simu-f2n2-p4i2
	$(MAKE) simu-f4n4-p4i4
	# With storage types BRAM, LUTRAM, REGS
	$(MAKE) simu-f2n2-p2i2-lut
	$(MAKE) simu-f1n2-p2i2-reg
	# With time multiplexing
	$(MAKE) simu-f4n2-p2i2-tmux2
	$(MAKE) simu-f1n1-p2i2-tmux4
	# With wait delays on input and output sides
	$(MAKE) simu-delay-in
	$(MAKE) simu-delay-out
	$(MAKE) simu-delays

simus-clean :
	rm -f simu*output.txt *.ghw

