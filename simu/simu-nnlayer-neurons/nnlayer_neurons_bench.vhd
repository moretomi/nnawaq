
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library std;
use std.textio.all;

library work;
use work.all;

entity nnlayer_neurons_bench is
	generic (

		-- Parameters for the neurons
		WDATA   : natural := 16;    -- The data bit width
		SDATA   : boolean := true;  -- The data signedness
		WWEIGHT : natural := 16;    -- The weight bit width
		SWEIGHT : boolean := true;  -- The weight signedness
		WOUT    : natural := 32;    -- The accumulator bit width
		-- Parameters for BRAM usage
		NPERBLK : natural := 18;
		WRNB    : natural := 2;
		WWRITE  : natural := 64;
		USE_LUTRAM : boolean := false;
		USE_REGS   : boolean := false;
		PACKED    : boolean := true;
		-- Identifier of neuron layer, mostly to be passed to custom internal components
		LAYER_ID : natural := 0;
		-- For compression of weights in memory
		COMP_STYLE : natural := 0;  -- Compression style, 0 means no decoder on datapath
		COMP_WRAW  : natural := 0;  -- Size of raw data (must be a multiple of WWEIGHT)
		COMP_WENC  : natural := 0;  -- Size of an encoded word
		COMP_ENWR  : boolean := false;  -- Compression is implemented on Write side to be transparent to the controlling SW
		-- Parameters for frame and number of neurons
		FSIZE  : natural := 16;
		NBNEU  : natural := 16;
		-- Level of time multiplexing of the neuron accumulators
		TIME_MUX : natural := 1;
		-- Parameters for input and output parallelism
		PAR_IN  : natural := 1;
		PAR_OUT : natural := 1;
		-- Options for neuron output
		SHREG_MUX       : boolean := false;
		SHREG_MUX_RADIX : natural := 18;
		-- Identifier of multiplication operation
		CUSTOM_MUL_ID : natural := 0;
		CUSTOM_WMUL   : natural := 8;      -- Bit width of the multiplication result
		CUSTOM_SMUL   : boolean := false;  -- Signedness of the multiplication result
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false;

		-- Enable insertion of delay from input/output interfaces
		EN_DELAY_IN : boolean := false;
		DELAY_OUT_DATE : integer := 0;
		-- Number of images to simulate
		FRAMES : natural := 10;
		-- Output text file for simulation results
		OUTPUT_FILE : string := "simu_output.txt"
	);
end nnlayer_neurons_bench;

architecture simu of nnlayer_neurons_bench is

	-- Return 1 if true, otherwise return 0
	function bool_to_nat(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	signal clk      : std_logic := '0';
	signal clk_next : std_logic := '0';
	signal clear    : std_logic := '1';
	signal clk_want_stop : boolean := false;

	constant TOTAL_FSIZE : natural := FSIZE * PAR_IN;
	constant TOTAL_NBNEU : natural := NBNEU * PAR_OUT * TIME_MUX;

	constant FSIZE_PHY   : natural := FSIZE + FSIZE * bool_to_nat(not LOCKED);
	constant NBNEU_PHY   : natural := NBNEU + NBNEU * bool_to_nat(not LOCKED);

	-- The number of weights in one input write vector
	constant WR_WEIGHTS_NB  : natural := WWRITE / WWEIGHT;

	-- Ports for Write Enable
	signal write_mode1    : std_logic := '0';  --Warning : This is renamed from write_mode to avoid conflict with files
	signal write_data     : std_logic_vector(WWRITE-1 downto 0) := (others => '0');
	signal write_enable   : std_logic := '0';
	signal write_end      : std_logic := '0';

	-- Run-time window step on each dimension
	signal user_fsize     : std_logic_vector(15 downto 0) := (others => '0');
	signal user_nbneu     : std_logic_vector(15 downto 0) := (others => '0');

	-- Data input
	signal data_in         : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal data_in_valid   : std_logic := '0';
	signal data_in_ready   : std_logic := '0';

	-- Data output
	signal data_out        : std_logic_vector(PAR_OUT*WOUT-1 downto 0) := (others => '0');
	signal data_out_valid  : std_logic := '0';

	signal end_of_frame    : std_logic := '0';

	signal out_fifo_room   : std_logic_vector(15 downto 0) := (others => '0');

	-- Testbench only, to debug erroneous output vectors in waveforms
	signal tb_out_error         : boolean := false;
	signal tb_out_count_all     : integer := 0;
	signal tb_out_count_inframe : integer := 0;

	-- The pre-generated weights

	type arrweight_type is array (0 to TIME_MUX*NBNEU_PHY*PAR_OUT-1, 0 to FSIZE*PAR_IN-1) of std_logic_vector(WWEIGHT-1 downto 0);

	function init_arrweights(phony : boolean) return arrweight_type is
		variable arr : arrweight_type;
	begin
		-- Weight values are : n*FSIZE+f, where n is the neuron index and f the position in the frame
		for n in 0 to TIME_MUX*NBNEU_PHY*PAR_OUT-1 loop
			for f in 0 to FSIZE*PAR_IN-1 loop
				arr(n, f) := std_logic_vector(to_unsigned(n*TOTAL_FSIZE + f, WWEIGHT));
			end loop;
		end loop;
		return arr;
	end function;

	constant arrweight : arrweight_type := init_arrweights(true);

	-- The pre-generated activations

	type arract_type is array (0 to FRAMES-1, 0 to FSIZE*PAR_IN-1) of std_logic_vector(WDATA-1 downto 0);

	function init_arracts(phony : boolean) return arract_type is
		variable arr : arract_type;
	begin
		-- Input values are : fr*FSIZE+f, where fr is the frame index and f the position in the frame
		for fr in 0 to FRAMES-1 loop
			for f in 0 to FSIZE*PAR_IN-1 loop
				arr(fr, f) := std_logic_vector(to_unsigned(fr*TOTAL_FSIZE + f, WDATA));
			end loop;
		end loop;
		return arr;
	end function;

	constant arract : arract_type := init_arracts(true);

	-- Component declaration

	component nnlayer_neurons is
		generic (
			-- Parameters for the neurons
			WDATA   : natural := 2;     -- The data bit width
			SDATA   : boolean := true;  -- The data signedness
			WWEIGHT : natural := 2;     -- The weight bit width
			SWEIGHT : boolean := true;  -- The weight signedness
			WOUT    : natural := 12;    -- The accumulator bit width
			-- Parameters for BRAM usage
			NPERBLK : natural := 18;
			WRNB    : natural := 2;
			WWRITE  : natural := 64;
			USE_LUTRAM : boolean := false;
			USE_REGS   : boolean := false;
			PACKED     : boolean := true;
			-- Identifier of neuron layer, mostly to be passed to custom internal components
			LAYER_ID   : natural := 0;
			-- For compression of weights in memory
			COMP_STYLE : natural := 0;  -- Compression style, 0 means no decoder on datapath
			COMP_WRAW  : natural := 0;  -- Size of raw data (must be a multiple of WWEIGHT)
			COMP_WENC  : natural := 0;  -- Size of an encoded word
			COMP_ENWR  : boolean := false;  -- Compression is implemented on Write side to be transparent to the controlling SW
			-- Parameters for frame and number of neurons
			FSIZE  : natural := 16;
			NBNEU  : natural := 16;
			-- Level of time multiplexing of the neuron accumulators
			TIME_MUX : natural := 1;
			-- Parameters for input and output parallelism
			PAR_IN  : natural := 1;
			PAR_OUT : natural := 1;
			-- Options for neuron output
			SHREG_MUX       : boolean := false;
			SHREG_MUX_RADIX : natural := 18;
			-- Identifier of multiplication operation
			CUSTOM_MUL_ID : natural := 0;
			CUSTOM_WMUL   : natural := 8;      -- Bit width of the multiplication result
			CUSTOM_SMUL   : boolean := false;  -- Signedness of the multiplication result
			-- Take extra margin on the FIFO level, in case there is something outside
			FIFOMARGIN : natural := 0;
			-- Lock the layer parameters to the generic parameter value
			LOCKED : boolean := false
		);
		port (
			clk            : in  std_logic;
			clear          : in  std_logic;
			-- Ports for Write Enable
			write_mode     : in  std_logic;
			write_data     : in  std_logic_vector(WWRITE-1 downto 0);
			write_enable   : in  std_logic;
			write_end      : out std_logic;
			-- The user-specified frame size and number of neurons
			user_fsize     : in  std_logic_vector(15 downto 0);
			user_nbneu     : in  std_logic_vector(15 downto 0);
			-- Data input, 2 bits
			data_in        : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
			data_in_valid  : in  std_logic;
			data_in_ready  : out std_logic;
			-- Scan chain to extract values
			data_out       : out std_logic_vector(PAR_OUT*WOUT-1 downto 0);
			data_out_valid : out std_logic;
			-- Indicate to the parent component that we are reaching the end of the current frame
			end_of_frame   : out std_logic;
			-- The output data enters a FIFO. This indicates the available room.
			out_fifo_room  : in  std_logic_vector(15 downto 0)
		);
	end component;

begin

	comp_i: nnlayer_neurons
		generic map (
			-- Parameters for the neurons
			WDATA   => WDATA,
			SDATA   => SDATA,
			WWEIGHT => WWEIGHT,
			SWEIGHT => SWEIGHT,
			WOUT    => WOUT,
			-- Parameters for BRAM usage
			NPERBLK    => NPERBLK,
			WRNB       => WRNB,
			WWRITE     => WWRITE,
			USE_LUTRAM => USE_LUTRAM,
			USE_REGS   => USE_REGS,
			PACKED     => PACKED,
			-- Identifier of neuron layer, mostly to be passed to custom internal components
			LAYER_ID   => LAYER_ID,
			-- For compression of weights in memory
			COMP_STYLE => COMP_STYLE, -- Compression style, 0 means no decoder on datapath
			COMP_WRAW  => COMP_WRAW,  -- Size of raw data (must be a multiple of WWEIGHT)
			COMP_WENC  => COMP_WENC,  -- Size of an encoded word
			COMP_ENWR  => COMP_ENWR,  -- Compression is implemented on Write side to be transparent to the controlling SW
			-- Parameters for frame and number of neurons
			FSIZE  => FSIZE_PHY,
			NBNEU  => NBNEU_PHY,
			-- Level of time multiplexing of the neuron accumulators
			TIME_MUX => TIME_MUX,
			-- Parameters for input and output parallelism
			PAR_IN  => PAR_IN,
			PAR_OUT => PAR_OUT,
			-- Options for neuron output
			SHREG_MUX       => false,
			SHREG_MUX_RADIX => 18,
			-- Identifier of multiplication operation
			CUSTOM_MUL_ID => 0,
			CUSTOM_WMUL   => 8,      -- Bit width of the multiplication result
			CUSTOM_SMUL   => false,  -- Signedness of the multiplication result
			-- The output data enters a FIFO. This indicates the available room.
			FIFOMARGIN => 0,
			-- Lock the layer parameters to the generic parameter value
			LOCKED => LOCKED
		)
		port map (
			clk            => clk,
			clear          => clear,
			-- Ports for Write Enable
			write_mode     => write_mode1,
			write_data     => write_data,
			write_enable   => write_enable,
			write_end      => write_end,
			-- The user-specified frame size and number of neurons
			user_fsize     => user_fsize,
			user_nbneu     => user_nbneu,
			-- Data input, 2 bits
			data_in        => data_in,
			data_in_valid  => data_in_valid,
			data_in_ready  => data_in_ready,
			-- Scan chain to extract values
			data_out       => data_out,
			data_out_valid => data_out_valid,
			-- Indicate to the parent component that we are reaching the end of the current frame
			end_of_frame   => end_of_frame,
			-- The output data enters a FIFO. This indicates the available room.
			out_fifo_room  => out_fifo_room
		);

	clk_next <= not clk when clk_want_stop = false else clk;
	clk <= clk_next after 5 ns;

	-- The user-specified frame size and number of neurons
	user_fsize <= std_logic_vector(to_unsigned(FSIZE, user_fsize'length));
	user_nbneu <= std_logic_vector(to_unsigned(NBNEU, user_nbneu'length));

	-- Process that generates stimuli
	process
		variable var_vec   : std_logic_vector(31 downto 0);
		variable var_wrnb  : integer;
		variable var_total : integer;
	begin

		-- Ports for Write Enable
		write_mode1  <= '0';
		write_data   <= (others => '0');
		write_enable <= '0';

		-- Clear
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		clear <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Write configuration weights

		write_mode1 <= '1';

		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- FIXME This test strengthens the simulation setup, but should not be mandatory
		assert WWRITE mod WWEIGHT = 0 report "ERROR : Generic parameter WWRITE (" & natural'image(WWRITE) & ") must be divisible by WWIDTH (" & natural'image(WWEIGHT) & ") " severity failure;

		-- Write weight values
		-- For each block (there are TMUX blocks), write config data for PAR_OUT*NBNEU*PAR_IN weight values
		-- FIXME Only PACKED mode is supported, this is the most interesting anyway

		var_total := 0;
		for t in 0 to TIME_MUX-1 loop
			for f in 0 to FSIZE-1 loop

				write_data <= (others => '0');
				var_wrnb := 0;

				-- This will become one large word in the memory
				-- Weights are send by groups of size WR_WEIGHTS_NB
				-- IMPORTANT : Need to write the full number of physical neurons, not just the neurons that are used (when LOCKED=false)
				for po in 0 to PAR_OUT-1 loop
					for n in 0 to NBNEU_PHY-1 loop
						for pi in 0 to PAR_IN-1 loop

							-- Enqueue the weight value
							write_data((var_wrnb+1)*WWEIGHT-1 downto var_wrnb*WWEIGHT) <= arrweight(t*PAR_OUT*NBNEU+n*PAR_OUT+po, f*PAR_IN+pi);

							-- Send the write vector
							var_wrnb := var_wrnb + 1;
							if var_wrnb = WR_WEIGHTS_NB then
								write_enable <= '1';
								wait until rising_edge(clk);
								write_enable <= '0';
								write_data <= (others => '0');
								var_wrnb := 0;
								var_total := var_total + 1;
							end if;

						end loop;
					end loop;
				end loop;

				-- Send any remaining incomplete write word
				if var_wrnb > 0 then
					write_enable <= '1';
					wait until rising_edge(clk);
					write_enable <= '0';
					write_data <= (others => '0');
					var_wrnb := 0;
					var_total := var_total + 1;
				end if;

			end loop;  -- fsize
		end loop;  -- tmux

		report "Writing config took " & natural'image(var_total) & " clock cycles";

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Need to keep the write mode set for a bit more time, to the component has enough time to process data words
		write_mode1 <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Write frame data
		-- Input values are : fr*FSIZE+f, where fr is the frame index and f the position in the frame

		for fr in 0 to FRAMES-1 loop
			report "# Sending frame " & to_string(fr);

			for t in 0 to TIME_MUX-1 loop
				for f in 0 to FSIZE-1 loop

					-- Prepare the input vector
					data_in <= (others => '0');
					for pi in 0 to PAR_IN-1 loop
						data_in((pi+1)*WDATA-1 downto pi*WDATA) <= arract(fr, f*PAR_IN + pi);
					end loop;

					-- Add extra wait delay on input side, arbitrary
					var_total := fr / 3;
					if EN_DELAY_IN = true and var_total >= 1 then
						for i in 0 to var_total-1 loop
							wait until rising_edge(clk);
						end loop;
					end if;

					-- Send the acknowledge
					data_in_valid <= '1';
					wait until rising_edge(clk) and (data_in_ready = '1');
					data_in_valid <= '0';

				end loop;  -- fsize
			end loop;  -- tmux
		end loop;  -- frames

		data_in_valid <= '0';

		wait until rising_edge(clk);
		wait until rising_edge(clk);

		-- Wait for end of simulation
		wait;

	end process;

	-- Process that generates the out_fifo_room signal
	process
		variable clk_cnt : integer;
	begin

		-- Enable data output
		out_fifo_room <= std_logic_vector(to_unsigned(64, out_fifo_room'length));

		-- Wait indefinitely if no interruption is scheduled
		if DELAY_OUT_DATE = 0 then
			wait;
		end if;

		-- Wait until end of reset
		wait until rising_edge(clk);
		wait until clear = '0';
		wait until rising_edge(clk);

		clk_cnt := 0;

		-- Wait until the scheduled beginning of the output interruption
		while clk_cnt < DELAY_OUT_DATE loop
			clk_cnt := clk_cnt + 1;
			wait until rising_edge(clk);
		end loop;

		-- Emulate an output fifo that is full
		out_fifo_room <= std_logic_vector(to_unsigned(0, out_fifo_room'length));
		for j in 0 to 30 loop
			wait until rising_edge(clk);
		end loop;

		-- Back to no limit
		out_fifo_room <= std_logic_vector(to_unsigned(64, out_fifo_room'length));

		-- Wait for end of simulation
		wait;

	end process;

	-- Process that reads outputs
	process
		file data_outfile : text open write_mode is OUTPUT_FILE;
		variable out_line : line;
		variable get_out_vec : std_logic_vector(WOUT-1 downto 0);
		variable want_out_vec : integer;
		variable want_out_var : integer;
		variable vec_n : integer;
		variable vec_f : integer;
	begin

		-- Wait until end of reset
		wait until rising_edge(clk);
		wait until clear = '0';
		wait until rising_edge(clk);

		tb_out_count_all <= 0;

		for fr in 0 to FRAMES-1 loop
			report "# Receiving frame " & to_string(fr);
			write(out_line, string'("# Beginning of frame ") & to_string(fr));
			writeline(data_outfile, out_line);

			tb_out_count_inframe <= 0;

			for t in 0 to TIME_MUX-1 loop
				if TIME_MUX > 1 then
					report "# Beginning tmux " & to_string(t);
					write(out_line, string'("# Beginning of time mux ") & to_string(t));
					writeline(data_outfile, out_line);
				end if;

				for n in 0 to NBNEU-1 loop

					-- Wait for a valid output vector
					wait until rising_edge(clk) and (data_out_valid = '1');

					for po in 0 to PAR_OUT-1 loop

						-- Generate the expected output vector
						want_out_vec := 0;
						for f in 0 to FSIZE-1 loop
							for pi in 0 to PAR_IN-1 loop

								-- Reminder :
								-- Input values are  : fr*FSIZE+f, where fr is the frame index and f the position in the frame
								-- Weight values are : n*FSIZE+f, where n is the neuron index and f the position in the frame
								vec_n := t*PAR_OUT*NBNEU + n*PAR_OUT + po;
								vec_f := f*PAR_IN + pi;
								want_out_vec := want_out_vec +
									to_integer(unsigned(arrweight(vec_n, vec_f))) *
									to_integer(unsigned(arract(fr, vec_f)));

							end loop;
						end loop;

						-- Select the right part of the output vector
						get_out_vec := data_out((po+1)*WOUT-1 downto po*WOUT);

						-- Emit an error message
						tb_out_error <= false;
						if to_integer(signed(get_out_vec)) /= want_out_vec then
							tb_out_error <= true;
							report "ERROR Got " & to_string(to_integer(signed(get_out_vec))) & " expected " & to_string(want_out_vec) &
								" for frame " & to_string(fr) & " tmux " & to_string(t) & " n " & to_string(n) & " po " & to_string(po);
						end if;

						-- Write output data to file
						hwrite(out_line, get_out_vec);
						if to_integer(signed(get_out_vec)) /= want_out_vec then
							write(out_line, string'("   ERROR expected "));
							hwrite(out_line, std_logic_vector(to_signed(want_out_vec, WOUT)));
						end if;
						writeline(data_outfile, out_line);

						-- Update counter signals to visualize in waveforms
						tb_out_count_all     <= tb_out_count_all + 1;
						tb_out_count_inframe <= tb_out_count_inframe + 1;

					end loop;  -- par_out

				end loop;  -- nbneu
			end loop;  -- tmux

		end loop;  -- frame

		-- Write end message to the output file
		write(out_line, string'("# End of simulation"));
		writeline(data_outfile, out_line);

		-- End of simulation
		report "All output data is received";
		report "Stopping simulation";
		clk_want_stop <= true;
		wait;

	end process;

end architecture;

