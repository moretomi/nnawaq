
submodules : hwopt-xilinx-7series datasets

hwopt-xilinx-7series :
	git submodule update --init $@
	ln -sf $@ hwopt

datasets :
	git submodule update --init $@

