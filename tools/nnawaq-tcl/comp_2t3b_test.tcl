#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It runs a neural network accelerator already configured in the PCIe FPGA board
# This is an experiment with 2t3b compression of ternary weights

# Create network
source build_vgg.tcl

# Dataset configuration
source config_vgg.tcl

# Launch test
nn_comp_2t3b_test
