#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It runs a MobileNet-v2 neural network in different conditions :
# - software execution, as CPU reference and for golden results
# - hardware accelerator already configured in the PCIe FPGA board

# The neural network is designed to classify images for ImageNet dataset

# Input images : 224x224x3 8b
# Output : 1000 classes

# Settings inherited from environment variables

global env

set vhdl 0
set swexec 0
set hwpcie 0
set hwforce 0
set hwtimeout 1000ms
set frames 10

if {[info exists env(VHDL)]} {
	set vhdl 1
}
if {[info exists env(SW)]} {
	set swexec 1
}
if {[info exists env(PCIE)]} {
	set hwpcie 1
}
if {[info exists env(PCIEFORCE)]} {
	set hwforce 1
}
if {[info exists env(PCIETIMEOUT)]} {
	set hwtimeout $env(PCIETIMEOUT)
}
if {[info exists env(FRAMES)]} {
	set frames $env(FRAMES)
}

# Build the network

nn_set hw_timeout=$hwtimeout

if {$hwpcie == 1} {

	# Read network architecture from FPGA
	nn_pcie_build

	# Print the network, for visual confirmation
	nn_print

} else {

	# General parameters
	nn_set selout=1
	nn_set fifomon=1
	nn_set lockregs=1
	nn_set noregs=0
	nn_set ifw=64

	puts "Generating VHDL"

	#nn_set no_fifo_win_neu_th=8
	#nn_set no_fifo_win_pool=true
	#nn_set no_fifo_neu_relu=true

	# Create network
	source build_mobilenetv2.tcl

}

# Generate VHDL and exit

if {$vhdl == 1} {

	# Generate the VHDL implementation
	set fi ../../boards/vc709/hdl-template/cnn_wrapper.template.vhd
	set fo ../../boards/vc709/hdl/cnn_wrapper.vhd
	nn_genvhdl $fi $fo

	exit 0

}

# Associate and load configuration files

source config_mobilenetv2.tcl

nn_load_config

# Actual run

nn_set fn=$frames ol=last onl=0

# Note: Other useful options with raw output to ease debug (from obsolete command-line) :
# -osep ' ' -omask -ofmt '%02x' -ofmt-norm '%7.3g' -swexec-gen-in -swexec-mod 0

# Note: to get full speed, use option -noout
# Note: to only count output data, use option -freerun and observe reg 6

if {$hwforce == 1} {

	# Force execution in hardware, even though we don't know what network is in the hardware
	nn_set o=out-mb2hw-forced.txt
	nn_pcie_run -force

} elseif {$hwpcie == 1} {

	# Execution on the FPGA
	nn_set o=out-mb2hw.txt
	nn_pcie_run

} elseif {$swexec == 1} {

	nn_set o=out-mb2sw.txt
	nn_swexec

}

