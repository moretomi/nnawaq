#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It configures weights and thresholds in an already-loaded neural network

set dataset "random"
if {[info exists env(DATASET)]} {
	set dataset $env(DATASET)
}

set vgg_neu 64
if {[info exists env(NEU)]} {
	set vgg_neu $env(NEU)
}

set vggcfg_dir "../../datasets/vgg${vgg_neu}-${dataset}-in3x8"

set cfg_neu [list \
	"$vggcfg_dir/weights1.csv" \
	"$vggcfg_dir/weights2.csv" \
	"$vggcfg_dir/weights4.csv" \
	"$vggcfg_dir/weights5.csv" \
	"$vggcfg_dir/weights7.csv" \
	"$vggcfg_dir/weights8.csv" \
	"$vggcfg_dir/weights11.csv" \
	"$vggcfg_dir/weights12.csv" \
	"$vggcfg_dir/weights13.csv" \
]

set cfg_ter [list \
	"$vggcfg_dir/thresholds1.csv" \
	"$vggcfg_dir/thresholds2.csv" \
	"$vggcfg_dir/thresholds4.csv" \
	"$vggcfg_dir/thresholds5.csv" \
	"$vggcfg_dir/thresholds7.csv" \
	"$vggcfg_dir/thresholds8.csv" \
	"$vggcfg_dir/thresholds11.csv" \
	"$vggcfg_dir/thresholds12.csv" \
]

set vgg_frames "$vggcfg_dir/input1.csv"
nn_set frames=$vgg_frames
nn_set floop=1 ml=1

# Set config file for neuron layers
for {set i 0} {$i < 9} {incr i} {
	nn_layer_set neu$i cfg=[lindex $cfg_neu $i]
}

# Set config file for ternarization layers
for {set i 0} {$i < 8} {incr i} {
	nn_layer_set ter$i cfg=[lindex $cfg_ter $i]
}

# Set number of output neurons for common datasets
if {$dataset == "random"} {
	nn_set no=10
}
if {$dataset == "cifar10"} {
	nn_set no=10
}
if {$dataset == "cifar100"} {
	nn_set no=100
}
if {$dataset == "gtsrb"} {
	nn_set no=43
}
if {$dataset == "svhn"} {
	nn_set no=10
}
