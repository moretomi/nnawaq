#!./nnawaq -tcl

# This TCL script is intended to be executed by the tool nnawaq
# It runs the Lenet-v5 neural network designed, in different conditions :
# - software execution, as CPU reference and for golden results
# - hardware accelerator already configured in the PCIe FPGA board

# Settings inherited from environment variables

global env

set vhdl 0
set swexec 0
set hwpcie 0
set hwforce 0
set hwtimeout 1000ms
set frames 10

if {[info exists env(VHDL)]} {
	set vhdl 1
}
if {[info exists env(SW)]} {
	set swexec 1
}
if {[info exists env(PCIE)]} {
	set hwpcie 1
}
if {[info exists env(PCIEFORCE)]} {
	set hwforce 1
}
if {[info exists env(PCIETIMEOUT)]} {
	set hwtimeout $env(PCIETIMEOUT)
}
if {[info exists env(FRAMES)]} {
	set frames $env(FRAMES)
}

# Build the network

nn_set hw_timeout=$hwtimeout

if {$hwpcie == 1} {

	# Read network architecture from FPGA
	nn_pcie_build

	# Print the network, for visual confirmation
	nn_print

} else {

	# General parameters
	nn_set selout=0
	nn_set fifomon=0
	nn_set lockregs=1
	nn_set noregs=0
	nn_set ifw=32

	puts "Generating VHDL"

	# Create network
	source build_lenet5.tcl

}

# Generate VHDL and exit

if {$vhdl == 1} {

	# Generate the VHDL implementation
	set fi ../../boards/zybo/cnn_axi_master_1.0/hdl-template/cnn_axi_master_v1_0_S00_AXI.template.vhd
	set fo ../../boards/zybo/cnn_axi_master_1.0/hdl/cnn_axi_master_v1_0_S00_AXI.vhd
	nn_genvhdl $fi $fo

	exit 0

}

# Associate and load configuration files

source config_lenet5.tcl

nn_load_config

# Actual run

#nn_set fn=1 ol=neu0 onl=6
nn_set fn=$frames ol=last onl=0

# Note: Other useful options with raw output to ease debug (from obsolete command-line) :
# -osep ' ' -omask -ofmt '%02x' -ofmt-norm '%7.3g' -swexec-gen-in -swexec-mod 0

# Note: to get full speed, use option -noout
# Note: to only count output data, use option -freerun and observe reg 6

if {$hwforce == 1} {

	# Force execution in hardware, even though we don't know what network is in the hardware
	nn_set o=out-lenethw-forced.txt
	nn_pcie_run -force

} elseif {$hwpcie == 1} {

	# Execution on the FPGA
	nn_set o=out-lenethw.txt
	nn_pcie_run

} elseif {$swexec == 1} {

	nn_set o=out-lenetsw.txt
	nn_swexec

}

