
extern "C" {

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <limits.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <time.h>

#include "nnawaq_utils.h"

}

#include "nn_layers_utils.h"
#include "nn_hw_config.h"
#include "swexec.h"

// ASIC estimations
#ifndef LIMITED
#include "estimasic.h"
#include "genvhdl.h"
#include "compress.h"
#endif

#ifndef NOTCL
#include "tcl_parser.h"
#endif

#include "hwacc_common.h"

// For declaration of recursive lambda functions
#include <functional>

using namespace std;


//============================================
// Layer class
//============================================

Layer::Layer(void) {
	// Note : All other fields are supposed to be initialized to zero, false, or something empty
	type = LAYER_NONE;
	id = -1;
}

Layer::~Layer(void) {
	if(vhdl_prefixl != NULL) free(vhdl_prefixl);
	if(vhdl_prefixu != NULL) free(vhdl_prefixu);
	if(cfg_filename != NULL) free(cfg_filename);
	if(cfg_data != NULL)   { free(cfg_data[0]); free(cfg_data); }
	if(swexec_output != NULL) free(swexec_output);
}

// Global list of layer types
map<string, unsigned> Layer::map_layer_name2id;
vector<Layer*> Layer::vec_layer_id2type;
map<string, unsigned> Layer::map_layer_entity2id;

int Layer::register_type(Layer* layer, const char* name) {
	unsigned errors_nb = 0;

	// Check availability of slots
	if((unsigned)layer->type >= vec_layer_id2type.size()) vec_layer_id2type.resize(layer->type + 1, nullptr);
	if(vec_layer_id2type[layer->type] != nullptr && vec_layer_id2type[layer->type] != layer) {
		printf("Error : Layer type %u is already registered\n", layer->type);
		errors_nb++;
	}

	string str = name;
	auto iter = map_layer_name2id.find(str);
	if(iter != map_layer_name2id.end() && iter->second != (unsigned)layer->type) {
		printf("Error : Layer type name '%s' is already registered by layer type %u\n", name, iter->second);
		errors_nb++;
	}

	// Ensure the entity name is unique
	if(layer->custom_entity != nullptr) {
		std::string str_entity = layer->custom_entity;
		if(map_layer_entity2id.insert(make_pair(str_entity, layer->type)).second == false) {
			printf("Error : Entity name '%s' is already registered\n", layer->custom_entity);
			errors_nb++;
		}
	}

	// Do nothing if slots are not available
	if(errors_nb > 0) return errors_nb;

	// Insert the layer type
	map_layer_name2id[str] = layer->type;
	vec_layer_id2type[layer->type] = layer;

	return 0;
}

int Layer::get_type_name2id(const char* type_name) {
	int type_id = LAYER_NONE;

	string str = type_name;
	auto iter = map_layer_name2id.find(str);
	if(iter != map_layer_name2id.end()) {
		type_id = iter->second;
	}

	return type_id;
}
int Layer::get_type_name2id_verbose(const char* type_name) {
	int type_id = get_type_name2id(type_name);
	if(type_id == LAYER_NONE) {
		printf("Error: Unknown layer type '%s'\n", type_name);
	}
	return type_id;
}

char const * Layer::get_type_id2namel(int type_id) {
	layer_t* ref_layer = nullptr;
	if((unsigned)type_id < Layer::vec_layer_id2type.size()) {
		ref_layer = Layer::vec_layer_id2type[type_id];
	}
	if(ref_layer == nullptr) return "unknown";
	return ref_layer->typenamel;
}
char const * Layer::get_type_id2nameu(int type_id) {
	layer_t* ref_layer = nullptr;
	if((unsigned)type_id < Layer::vec_layer_id2type.size()) {
		ref_layer = Layer::vec_layer_id2type[type_id];
	}
	if(ref_layer == nullptr) return "UNKNOWN";
	return ref_layer->typenameu;
}

Layer* Layer::create_new_from_id(unsigned type_id, const char* type_name) {
	layer_t* ref_layer = nullptr;
	if((unsigned)type_id < Layer::vec_layer_id2type.size()) {
		ref_layer = Layer::vec_layer_id2type[type_id];
	}
	if(ref_layer == nullptr) return nullptr;
	// Use a clone method to inherit all custom fields
	layer_t* layer = ref_layer->clone();
	int z = layer->params_from_type_name(type_name);
	if(z != 0) {
		delete layer;
		layer = nullptr;
	}
	return layer;
}
Layer* Layer::create_new_from_id_verbose(unsigned type_id, const char* type_name) {
	layer_t* layer = create_new_from_id(type_id, type_name);
	if(layer == nullptr) {
		printf("Error: Layer type ID %i is not a valid layer type\n", type_id);
	}
	return layer;
}


//============================================
// Classes for the NN layer types
//============================================

LayerWin::LayerWin(void) {

	type = LAYER_WIN;

	// FIXME The layer type name should not be set here
	typenamel = "win";
	typenameu = "WIN";

	mem_implem = MEM_IMPLEM_NONE;

	// Default parameters
	winx      = 1;
	winy      = 1;
	stepx     = 1;
	stepy     = 1;
	begpadx   = 1;
	begpady   = 1;

}
LayerNeu::LayerNeu(void) {

	type = LAYER_NEU;

	// FIXME The layer type name should not be set here
	typenamel = "neu";
	typenameu = "NEU";

	mem_implem = MEM_IMPLEM_NONE;

	neu_style = 0;

}
LayerPool::LayerPool(void) {

	type = LAYER_POOL;

	// FIXME The layer type name should not be set here
	typenamel = "pool";
	typenameu = "POOL";

	// Default parameters
	winx = 2;
	winy = 2;

	// This is intentionally initialized to an invalid value
	// Instantiation of the layer will set this correctly
	pool_type = POOL_TYPE_NONE;

}

LayerNorm::LayerNorm(void) {

	type = LAYER_NORM;

	// FIXME The layer type name should not be set here
	typenamel = "norm";
	typenameu = "NORM";

}
LayerTernarize::LayerTernarize(void) {

	type = LAYER_TER;

	// FIXME The layer type name should not be set here
	typenamel = "ter";
	typenameu = "TER";

}
LayerRelu::LayerRelu(void) {

	type = LAYER_RELU;

	// FIXME The layer type name should not be set here
	typenamel = "relu";
	typenameu = "RELU";

}
LayerAdd::LayerAdd(void) {

	type = LAYER_ADD;

	// FIXME The layer type name should not be set here
	typenamel = "add";
	typenameu = "ADD";

}
LayerCustom::LayerCustom(void) {

	// Note : The layer type and type name will be overridden at custom layer creation

	type = LAYER_CUSTOM;

	// FIXME The layer type name should not be set here
	typenamel = "cust";
	typenameu = "CUST";

}

LayerFork::LayerFork(void) {

	type = LAYER_FORK;

	// FIXME The layer type name should not be set here
	typenamel = "fork";
	typenameu = "FORK";

	// Successor layers are listed in array
	next_is_arr = true;

}
LayerCat::LayerCat(void) {

	type = LAYER_CAT;

	// FIXME The layer type name should not be set here
	typenamel = "cat";
	typenameu = "CAT";

	// Predecessor layers are listed in array
	prev_is_arr = true;

}
LayerScatter::LayerScatter(void) {

	type = LAYER_SCATTER;

	// FIXME The layer type name should not be set here
	typenamel = "sca";
	typenameu = "SCA";

	// Successor layers are listed in array
	next_is_arr = true;

}
LayerGather::LayerGather(void) {

	type = LAYER_GATHER;

	// FIXME The layer type name should not be set here
	typenamel = "gat";
	typenameu = "GAT";

	// Predecessor layers are listed in array
	prev_is_arr = true;

}

LayerFlatten::LayerFlatten(void) {

	type = LAYER_FLATTEN;

	// FIXME The layer type name should not be set here
	typenamel = "flat";
	typenameu = "FLAT";

}
LayerSoftMax::LayerSoftMax(void) {

	type = LAYER_SOFTMAX;

	// FIXME The layer type name should not be set here
	typenamel = "softmax";
	typenameu = "SOFTMAX";

}
LayerFifo::LayerFifo(void) {

	type = LAYER_FIFO;

	// FIXME The layer type name should not be set here
	typenamel = "fifo";
	typenameu = "FIFO";

}

void Layer::apply_network_defaults(Network* network) {
	// Nothing to do
}

void LayerWin::apply_network_defaults(Network* network) {
	if(network->default_mem_implem_win != MEM_IMPLEM_AUTO) {
		mem_implem = network->default_mem_implem_win;
	}
}

void LayerNeu::apply_network_defaults(Network* network) {
	neu_wweight = network->default_neu_ww;
	neu_sgnd = NEUSGN_SIGNED | NEUSGN_LOCKED;
	neu_sgnw = (network->default_neu_sw == true) ? NEUSGN_SIGNED : 0;
	neu_sgnw |= NEUSGN_LOCKED;
	if(network->default_mem_implem_neu != MEM_IMPLEM_AUTO) {
		mem_implem = network->default_mem_implem_neu;
	}
}

void LayerPool::apply_network_defaults(Network* network) {
	round_nearest = network->default_round_nearest;
}

void LayerNorm::apply_network_defaults(Network* network) {
	round_nearest = network->default_round_nearest;
	norm_mul_cst = network->default_norm_mul_cst;
	norm_shr_cst = network->default_norm_shr_cst;
	norm_wbias   = network->default_norm_wbias;
	norm_wmul    = network->default_norm_wmul;
	norm_wshr    = network->default_norm_wshr;
}

void LayerRelu::apply_network_defaults(Network* network) {
	relu_min = network->default_relu_min;
	relu_max = network->default_relu_max;
}

// Methods to create a new instance of a Layer object

Layer* Layer::create_new(void) {
	// Creating an instance of the base Layer class is not allowed
	abort();
}
Layer* Layer::create_new(Network* network) {
	Layer* layer = create_new();
	layer->apply_network_defaults(network);
	return layer;
}

Layer* LayerWin::create_new(void) {
	return new LayerWin();
}
Layer* LayerNeu::create_new(void) {
	return new LayerNeu();
}
Layer* LayerPool::create_new(void) {
	return new LayerPool();
}

Layer* LayerNorm::create_new(void) {
	return new LayerNorm();
}
Layer* LayerTernarize::create_new(void) {
	return new LayerTernarize();
}
Layer* LayerRelu::create_new(void) {
	return new LayerRelu();
}
Layer* LayerAdd::create_new(void) {
	return new LayerAdd();
}
Layer* LayerCustom::create_new(void) {
	return new LayerCustom();
}

Layer* LayerFork::create_new(void) {
	return new LayerFork();
}
Layer* LayerCat::create_new(void) {
	return new LayerCat();
}
Layer* LayerScatter::create_new(void) {
	return new LayerScatter();
}
Layer* LayerGather::create_new(void) {
	return new LayerGather();
}

Layer* LayerFlatten::create_new(void) {
	return new LayerFlatten();
}
Layer* LayerSoftMax::create_new(void) {
	return new LayerSoftMax();
}
Layer* LayerFifo::create_new(void) {
	return new LayerFifo();
}

// Methods to create a new clone of a Layer object

Layer* Layer::clone(void) {
	// Creating an instance of the base Layer class is not allowed
	abort();
}

Layer* LayerWin::clone(void) {
	return new LayerWin(*this);
}
Layer* LayerNeu::clone(void) {
	return new LayerNeu(*this);
}
Layer* LayerPool::clone(void) {
	return new LayerPool(*this);
}

Layer* LayerNorm::clone(void) {
	return new LayerNorm(*this);
}
Layer* LayerTernarize::clone(void) {
	return new LayerTernarize(*this);
}
Layer* LayerRelu::clone(void) {
	return new LayerRelu(*this);
}
Layer* LayerAdd::clone(void) {
	return new LayerAdd(*this);
}
Layer* LayerCustom::clone(void) {
	return new LayerCustom(*this);
}

Layer* LayerFork::clone(void) {
	return new LayerFork(*this);
}
Layer* LayerCat::clone(void) {
	return new LayerCat(*this);
}
Layer* LayerScatter::clone(void) {
	return new LayerScatter(*this);
}
Layer* LayerGather::clone(void) {
	return new LayerGather(*this);
}

Layer* LayerFlatten::clone(void) {
	return new LayerFlatten(*this);
}
Layer* LayerSoftMax::clone(void) {
	return new LayerSoftMax(*this);
}
Layer* LayerFifo::clone(void) {
	return new LayerFifo(*this);
}

// Methods to re-parameterize a new layer (created with create_new or clone) with the user-specified layer name

int Layer::params_from_type_name(char const * type_name) {
	// Nothing to do by default
	return 0;
}

int LayerPool::params_from_type_name(char const * type_name) {
	unsigned type_wanted = POOL_TYPE_NONE;
	if(type_name != nullptr) {
		if     (strcasecmp(type_name, "maxpool") == 0) type_wanted = POOL_TYPE_MAX;
		else if(strcasecmp(type_name, "minpool") == 0) type_wanted = POOL_TYPE_MIN;
	  else if(strcasecmp(type_name, "avgpool") == 0) type_wanted = POOL_TYPE_AVG;
	  else if(strcasecmp(type_name, "add") == 0)     type_wanted = POOL_TYPE_ADD;
	  else if(strcasecmp(type_name, "addpool") == 0) type_wanted = POOL_TYPE_ADD;
	}
	if(type_wanted != POOL_TYPE_NONE) pool_type = type_wanted;
	if(pool_type == POOL_TYPE_NONE) return 1;
	return 0;
}


//============================================
// Network class
//============================================

Network* Network::singleton = nullptr;

// The only way of obtaining an HwAcc object
Network* Network::GetSingleton(void) {
	if(singleton == nullptr) {
		singleton = new Network();
	}
	return singleton;
}
void Network::DeleteSingleton(void) {
	if(singleton != nullptr) {
		delete singleton;
	}
	singleton = nullptr;
}

void Network::clear(void) {

	// Clear the contents of the layers
	for(auto layer : layers) {
		delete layer;
	}

	// Empty the vector of layers

	layers.clear();
	layer_first = nullptr;
	layer_last = nullptr;

	// Reset other fields

	// FIXMEEEE This variable is out of the scope of this class
	// Also need to null the global pointer to layers
	param_out_layer = nullptr;

	param_cnn_origin = CNN_ORIGIN_SCRIPT;

	// Reset the counters per layer type

	layers_idx.clear();

	layers_idxhw = 0;
	layers_idxcfg = 0;

}

layer_t* Network::getlayer_from_string_id(const char* strid) {
	if(strid==NULL || strid[0]==0) return NULL;
	if(layers.size()==0) return NULL;

	// Check if the string is all digits
	// FIXME Missing spec about what the ID means, and this way of getting a layer may not be useful
	#if 0
	bool alldigit = true;
	for(const char* ptrid=strid; (*ptrid)!=0; ptrid++) {
		if(isdigit(*ptrid)) continue;
		alldigit = false;
		break;
	}
	// Get the layer from integer ID, as index in the stack of layers
	if(alldigit==true) {
		int id = atoi(strid);
		if(id < 0 || id >= layers_nb) return NULL;
		return layers + id;
	}
	#endif

	// Handle special string identifiers
	if(strcasecmp(strid, "first")==0) {
		return layer_first;
	}
	if(strcasecmp(strid, "last")==0) {
		return layer_last;
	}

	// Search layer from <type><idx>
	char buf[100];
	for(auto layer : layers) {
		sprintf(buf, "%s%u", layer->typenamel, layer->typeidx);
		if(strcasecmp(buf, strid)==0) return layer;
	}

	return NULL;
}

layer_t* Network::getlayer_from_nameidx(const char* type_name, unsigned typeidx) {
	for(auto layer : layers) {
		if(strcasecmp(layer->typenamel, type_name)!=0) continue;
		if(layer->typeidx!=typeidx) continue;
		return layer;
	}
	return NULL;
}


//============================================
// Build NN
//============================================

int declare_builtin_layers(void) {
	int errors_nb = 0;
	Layer* layer = nullptr;

	layer = new LayerWin();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "window");

	layer = new LayerNeu();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "neuron");
	errors_nb += Layer::register_type(layer, "neurons");

	layer = new LayerPool();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "maxpool");
	errors_nb += Layer::register_type(layer, "minpool");
	errors_nb += Layer::register_type(layer, "avgpool");
	errors_nb += Layer::register_type(layer, "addpool");

	layer = new LayerNorm();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "norm");
	errors_nb += Layer::register_type(layer, "batchnorm");

	layer = new LayerTernarize();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "ternarize");
	errors_nb += Layer::register_type(layer, "recode_to_ter");

	layer = new LayerRelu();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "relu");

	layer = new LayerAdd();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "add");

	layer = new LayerFork();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "fork");

	layer = new LayerCat();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "concat");

	layer = new LayerScatter();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "scatter");

	layer = new LayerGather();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "gather");

	layer = new LayerFlatten();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "flatten");

	layer = new LayerSoftMax();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "softmax");

	layer = new LayerFifo();
	errors_nb += Layer::register_type(layer, layer->typenamel);
	errors_nb += Layer::register_type(layer, "fifo");

	return errors_nb;
}

layer_t* Network::layer_new_fromtype(int type_id, char const * type_name) {

	// Get the reference layer type pointer
	layer_t* layer = Layer::create_new_from_id_verbose(type_id, type_name);
	if(layer == nullptr) {
		return nullptr;
	}
	layer->apply_network_defaults(this);

	// Assign IDs according to indexes of layer types
	layer->type = type_id;
	if((unsigned)type_id >= layers_idx.size()) layers_idx.resize(type_id + 1, 0);
	layer->typeidx = layers_idx[type_id]++;
	if(layer->requires_idxhw()) layer->id = layers_idxhw++;
	if(layer->requires_idxcfg()) layer->cfg_id = layers_idxcfg++;

	// Append in the array of layers
	layer->index = layers.size();
	layers.push_back(layer);
	layer->network = this;

	return layer;
}

Layer* Network::layer_new_fromtypename(char const * type_name) {
	int type_id = Layer::get_type_name2id_verbose(type_name);
	if(type_id == LAYER_NONE) return nullptr;
	return layer_new_fromtype(type_id, type_name);
}

static void layer_add_link(layer_t* layer, layer_t* layer_other) {
	layer->arr_layers.push_back(layer_other);
}

void layer_link(layer_t* layer_prev, layer_t* layer_next) {
	// Link in prev layer
	if(layer_prev->next_is_arr == true) layer_add_link(layer_prev, layer_next);
	else layer_prev->next = layer_next;
	// Link in next layer
	if(layer_next->prev_is_arr == true) layer_add_link(layer_next, layer_prev);
	else layer_next->prev = layer_prev;
}

static void layer_link_replace_prev(layer_t* layer, layer_t* layer_prev_old, layer_t* layer_prev_new) {
	if(layer->prev == layer_prev_old) layer->prev = layer_prev_new;
	if(layer->prev_is_arr == true) {
		for(unsigned i=0; i<layer->arr_layers.size(); i++) {
			if(layer->arr_layers[i] == layer_prev_old) layer->arr_layers[i] = layer_prev_new;
		}
	}
}
static void layer_link_replace_next(layer_t* layer, layer_t* layer_next_old, layer_t* layer_next_new) {
	if(layer->next == layer_next_old) layer->next = layer_next_new;
	if(layer->next_is_arr == true) {
		for(unsigned i=0; i<layer->arr_layers.size(); i++) {
			if(layer->arr_layers[i] == layer_next_old) layer->arr_layers[i] = layer_next_new;
		}
	}
}

// Assumptions :
// - prev/next are not connected to each other, or they are neither FORK nor CAT
// - the layer to insert has no connection
void layer_insert(layer_t* layer, layer_t* layer_prev, layer_t* layer_next) {
	if(layer_prev != nullptr) layer_link(layer_prev, layer);
	if(layer_next != nullptr) layer_link(layer, layer_next);
}

// Assumptions :
// - prev/next are connected to each other
// - the layer to insert has no connection, and is neither FORK nor CAT
void layer_insert_replace(layer_t* layer, layer_t* layer_prev, layer_t* layer_next) {
	// Link prev <-> layer
	layer_link_replace_next(layer_prev, layer_next, layer);
	layer->prev = layer_prev;
	// Link layer <-> next
	layer->next = layer_next;
	layer_link_replace_prev(layer_next, layer_prev, layer);
}

// Assume that the input layer is the last created one, not yet linked with its previous one in the vector
void Network::layer_enqueue(layer_t* layer) {
	layer_insert(layer, layer_last, nullptr);
	if(layer_first == nullptr) layer_first = layer;
	layer_last = layer;
}

layer_t* Network::layer_new_enqueue_fromtype(int type_id, char const * type_name) {
	layer_t* layer = layer_new_fromtype(type_id, type_name);
	if(layer != nullptr) layer_enqueue(layer);
	return layer;
}

