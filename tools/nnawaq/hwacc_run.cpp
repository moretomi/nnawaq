// Actual usage of the FPGA accelerator with RIFFA PCIe interface
// Write configuration data, send frames, receive classification results

extern "C" {

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <ctype.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>  // For usleep()

#include "nnawaq_utils.h"
#include "load_config.h"

}

#include "nn_layers_utils.h"
#include "nn_hwacc_config.h"
#include "nn_hw_config.h"
#include "nn_load_config.h"

#include "hwacc_common.h"

using namespace std;


//============================================
// Write config for NN layers
//============================================

int HwAcc_Common::write_layer_config(layer_t* layer) {
	// Initial number of loop bounds
	unsigned num_parts = 1;

	vector<uint32_t> arr;

	for(unsigned idx_part=0; idx_part < num_parts; idx_part++) {
		unsigned code_part = 0;

		// Get the configuration stream
		int z = layer->hwacc_genconfig(this, arr, code_part, num_parts, idx_part);
		if(z != 0) return z;

		if(num_parts == 0 || arr.size() == 0) break;

		// Round the number of 32b words to the size of the width of the communication interface
		while(arr.size() % accreg_ifw32 != 0) arr.push_back(0);

		// FIXME Reset the entire HW accelerator
		#if 1
		accreg_clear();
		//accreg_clear();
		accreg_rd(3);
		// Send the NN config to the FPGA so the frame size is known by the neurons
		//if(param_hw_force==false) {
		//	build_network();
		//	accreg_rd(3);
		//}
		#endif

		// Set primary write mode
		accreg_set_wmode1(layer->cfg_id);
		// Set the secondary write mode
		accreg_set_wmode2(code_part);

		// Tiny read to make sure the mode is correctly taken into account
		accreg_rd(4);

		if(param_debug==true) {
			printf("Info Layer %s%u : Sending data buffer with %lu words\n", layer->typenameu, layer->typeidx, arr.size());
		}

		#if 0
		for(unsigned i=0; i<arr.size(); i+=accreg_ifw32) {
			printf("DEBUG %s%u addr %u:", layer->typenameu, layer->typeidx, i/accreg_ifw32);
			for(unsigned v=0; v<accreg_ifw32; v++) printf(" 0x%08x", arr[i+v]);
			printf("\n");
		}
		#endif

		// Send the big buffer to the FPGA
		fpga_send32_wait(arr.data(), arr.size());

		if(param_debug==true) {
			unsigned hwr = accreg_get_pcierecv32();
			printf("  The HW says it got %u words (%+i)\n", hwr, hwr - (unsigned)arr.size());
		}

		// Clean
		arr.resize(0);

	}  // Loop on parts

	return 0;
}

int HwAcc_Common::write_config(Network* network) {
	int64_t oldtime, newtime;

	// Only to know the execution time
	oldtime = Time64_GetReal();

	auto& layers = network->layers;
	for(auto layer : layers) {
		// Read the config files
		layer->load_config_files();
		// Convert to raw configuration data and send to the accelerator
		write_layer_config(layer);
	}  // Scan all layers

	newtime = Time64_GetReal();
	int64_t totime_config = newtime - oldtime;
	printf("Config time .. %g s\n", TimeDouble_From64(totime_config));

	return 0;
}



//============================================
// Write frames
//============================================

typedef struct frame_thread_data_t {
	HwAcc_Common* hwacc;
	layer_t* layer;
	unsigned frames_nb;
} frame_thread_data_t;

// Wrapper thread routine to call pthread_create on a non-member function
static void* getoutputs_thread_wrapper(void* arg) {
	frame_thread_data_t* thdata = (frame_thread_data_t*)arg;
	HwAcc_Common* hwacc = thdata->hwacc;
	return hwacc->getoutputs_thread(thdata->layer, thdata->frames_nb);
}

// Thread routine to receive NN results while the frames are being sent
void* HwAcc_Common::getoutputs_thread(layer_t* layer, unsigned frames_nb) {
	unsigned frame_size = layer->out_nbframes * ((layer->out_fsize + layer->split_out - 1) / layer->split_out);
	unsigned frame_size_user = frame_size;
	Network* network = layer->network;

	if(layer->type == LAYER_NEU) {
		if(accreg_locked==true || param_hw_force==true) {
			unsigned neurons = layer->neurons;
			if(network->cnn_outneu > 0 && network->cnn_outneu <= layer->neurons_max) neurons = network->cnn_outneu;
			frame_size      = layer->out_nbframes * ((layer->neurons_max + layer->split_out - 1) / layer->split_out);
			frame_size_user = layer->out_nbframes * ((neurons            + layer->split_out - 1) / layer->split_out);
		}
	}
	unsigned frame_extra = frame_size - frame_size_user;

	unsigned nb32 = frames_nb * frame_size;
	unsigned nb32_rnd4 = uint_next_multiple(nb32, 4);
	int32_t* buf = (int32_t*)malloc(nb32_rnd4 * sizeof(*buf));
	// In case of timeout, clear the buffer for debug
	if(param_out_timeout > 0) memset(buf, 0, nb32_rnd4 * sizeof(*buf));

	if(param_debug==true) {
		printf("DEBUG Asking for %u 32-bit words (buffer of %u words)\n", nb32, nb32_rnd4);
	}
	else {
		printf("Info: Expecting to receive %u 32-bit words from the accelerator\n", nb32);
	}
	accreg_set_nboutputs(nb32);

	int z = fpga_recv32((uint32_t*)buf, nb32_rnd4);

	if(z != (int)nb32_rnd4) {
		printf("ERROR: recv %i words, wanted %u\n", z, nb32_rnd4);
		if(param_out_timeout > 0) printf("  This may be due to timeout: the HW says it sent %u words\n", accreg_get_nboutputs());
		// Note: Don't clear the end of the buffer because 0 is returned in case of timeout...
		//if(z < nb32_rnd4) memset(buf+z, 0, (nb32_rnd4-z)*sizeof(*buf));
	}

	// Fix alignment of results
	unsigned extravalues = nb32 % 4;
	if(extravalues > 0) {
		// Go at the address of the last 128-bit chunk
		int32_t* ptr = buf + nb32_rnd4 - 4;
		// Move the extra values
		unsigned m = 4 - extravalues;
		for(unsigned i=0; i<extravalues; i++) ptr[i] = buf[i + m];
	}

	if(param_noout==false) {

		unsigned mask = (unsigned)~0;
		if(param_out_mask==true) mask = uint_genmask(layer->out_wdata);

		// Display
		unsigned bufidx = 0;
		for(unsigned f=0; f<frames_nb; f++) {
			if(Fo==stdout) printf("RESULT: Frame %u: ", f);
			for(unsigned r=0; r<frame_size_user; r++) {
				if(param_out_nl > 0 && r > 0 && r % param_out_nl == 0) fprintf(Fo, "\n");
				else if(r > 0 && param_out_sep != NULL) fprintf(Fo, "%s", param_out_sep);
				fprintf(Fo, param_out_format, buf[bufidx++] & mask);
			}
			bufidx += frame_extra;
			fprintf(Fo, "\n");
			if(param_out_nl > 0) fprintf(Fo, "\n");
		}  // Loop on frames

	}  // param_noout == false

	// Clean
	free(buf);

	return NULL;
}

int HwAcc_Common::write_frames_inout(const char* filename, layer_t* inlayer, layer_t* outlayer) {
	int64_t totime_file = 0;
	int64_t totime_nn = 0;

	int64_t oldtime, newtime;
	double diff;

	FILE* F = fopen(filename, "rb");
	if(F==NULL) {
		printf("ERROR: Can't open file '%s'\n", filename);
		return -1;
	}

	// Set primary write mode
	accreg_set_wmode1(ACCREG4_WMODE1_FRAME);
	// Make sure the mode is correctly taken into account
	accreg_rd(4);

	Network* network = inlayer->network;
	unsigned fsize = inlayer->fsize;
	unsigned wdata = inlayer->wdata;

	// Array to store data for a certain number of frames
	// Check maximum size for Riffa: 2G 32-bit words = 8k MB
	if(param_bufsz_mb >= 8192) param_bufsz_mb = 8192 - 1;

	uint64_t maxframes_nb_tmp64 = (uint64_t)param_bufsz_mb * 1024*1024 * 8 / fsize / wdata;
	unsigned maxframes_nb = maxframes_nb_tmp64;
	if(maxframes_nb==0) maxframes_nb = 1;
	if(param_fn==0 || param_fn > maxframes_nb) {
		printf("Info: Using frame batches of up to %u frames\n", maxframes_nb);
	}
	if(param_fn > 0 && maxframes_nb > param_fn) maxframes_nb = param_fn;

	// Compute the number of bits per frame
	unsigned bits_per_frame = fsize * wdata;
	unsigned total_nb32 = ((uint64_t)maxframes_nb * bits_per_frame + 31) / 32;

	// Allocate some extra space just to make sure PCIe 128-bit buffers fit
	uint32_t* databuf = (uint32_t*)malloc(total_nb32 * sizeof(*databuf) + 128);
	int* framebuf = (int*)malloc(fsize * sizeof(*framebuf));

	unsigned databuf_nb32 = 0;

	// Merge items inside one 32-bit buffer
	uint32_t buf32 = 0;
	unsigned buf32_nb = 0;  // Number of items in the current 32-bit word
	uint32_t buf32_mask = 0;
	for(unsigned i=0; i<wdata; i++) buf32_mask = (buf32_mask << 1) | 0x01;

	unsigned curframes_nb = 0;
	unsigned totalframes_nb = 0;

	load_warnings_clear();

	// Force set free run mode each time, to reset the output counter
	accreg_freerun_clear();
	if(param_freerun==true) {
		accreg_freerun_set();
	}

	// Only to know the execution time
	oldtime = Time64_GetReal();

	// Infinite loop that gets frames
	do {

		// Get one frame
		int r = loadfile_oneframe(F, framebuf, fsize, param_multiline);
		if(r < 0 && param_floop==true && param_fn > 0) {
			// Sanity check to avoid infinite loop
			if(totalframes_nb==0) break;
			// Rewind the file
			rewind(F);
			continue;
		}

		// Add the frame to the big buffer
		if(r >= 0) {

			// If needed, reorder image data
			if(inlayer->fx > 1 || inlayer->fy > 1) {
				unsigned fx = inlayer->fx;
				unsigned fy = inlayer->fy;
				unsigned fz = inlayer->fz;
				// Reorder
				reorder_to_zfirst_dim2(&framebuf, 1, fsize, fx, fy, fz, 0);
			}

			// For debug: a VHDL simulation can read this dumped data as input
			#if 0
			for(unsigned i=0; i<fsize; i++) {
				int val = framebuf[i];
				for(int sh=wdata-1; sh>=0; sh--) {
					printf("%c", '0' + ((val >> sh) & 0x01));
				}
				printf("\n");
			}
			#endif

			// Enqueue the frame data to the big buffer of frames
			for(unsigned i=0; i<fsize; i++) {
				buf32 |= (framebuf[i] & buf32_mask) << (buf32_nb*wdata);
				buf32_nb ++;
				if(buf32_nb * wdata >= 32) {
					databuf[databuf_nb32++] = buf32;
					buf32 = 0;
					buf32_nb = 0;
				}
			}

			// Increment frame counters
			curframes_nb ++;
			totalframes_nb ++;
		}

		// If the big buffer contains enough frames, send that
		if(
			(curframes_nb >= maxframes_nb) ||
			(param_fn > 0 && totalframes_nb >= param_fn) ||
			(curframes_nb > 0 && r<0)
		) {

			// If some items remain, add them to the big buffer
			if(buf32_nb > 0) {
				databuf[databuf_nb32++] = buf32;
				buf32 = 0;
				buf32_nb = 0;
			}

			// Only to know the execution time
			newtime = Time64_GetReal();
			totime_file += newtime - oldtime;

			if(param_debug==true) {
				printf("Info: Sending data buffer with %u frames, %u words\n", curframes_nb, databuf_nb32);
			}
			else {
				printf("Info: Sending data buffer with %u frames, size %u bytes\n", curframes_nb, databuf_nb32*4);
			}

			#if 0
			for(unsigned i=0; i<nb32; i+=4) {
				printf("DEBUG FRAME addr %u: 0x%08x 0x%08x 0x%08x 0x%08x\n", i/4, databuf[i], databuf[i+1], databuf[i+2], databuf[i+3]);
			}
			#endif

			// ID for the listening thread
			pthread_t th_get;

			frame_thread_data_t thdata;
			thdata.hwacc = this;
			thdata.layer = outlayer;
			thdata.frames_nb = curframes_nb;

			// FIXME Reset the entire HW accelerator
			#if 1
			accreg_clear();
			//accreg_clear();
			accreg_rd(3);
			//if(param_hw_force==false) {
			//	write_config_regs();
			//	accreg_rd(3);
			//}
			// Force set free run mode each time, to reset the output counter
			accreg_freerun_clear();
			if(param_freerun==true) {
				accreg_freerun_set();
			}
			#endif

			// Set configuration
			if(accreg_selout==true && network->param_selout==true) {
				accreg_set_recv1(outlayer->id);
			}
			else {
				accreg_set_recv1(ACCREG4_WMODE1_FRAME);
			}
			accreg_set_recv2(0);
			accreg_rd(4);

			// Launch receive thread
			if(param_freerun==false) {
				pthread_create(&th_get, NULL, getoutputs_thread_wrapper, &thdata);
			}

			// Only to know the execution time
			oldtime = Time64_GetReal();

			// Send the big buffer to the FPGA, through the Riffa data channel
			fpga_send32(databuf, databuf_nb32);
			printf("Info: Data sent\n");

			// Note: No need to have an additional wait loop because we do get results this time

			if(param_freerun==false) {
				printf("Info: Waiting for results...\n");
				// Ensure the listening thread has finished
				pthread_join(th_get, NULL);
			}

			// Only to know the execution time
			newtime = Time64_GetReal();
			totime_nn += newtime - oldtime;
			oldtime = newtime;

			if(param_debug==true) {
				if(param_freerun==true) {
					// Wait for the last frame to be fully processed
					usleep(100*1000);
				}
				unsigned hwr = accreg_get_pcierecv32();
				printf("  The HW says it got %u words (%+i)\n", hwr, hwr - databuf_nb32);
			}

			// Reset counters for next big buffer of frames
			curframes_nb = 0;
			databuf_nb32 = 0;
		}

		// Exit when the end of the file is reached
		if(r < 0) break;
		// Exit when enough frames have been sent
		if(param_fn > 0 && totalframes_nb >= param_fn) break;

	} while(1);  // Read the lines of the file

	// Clean
	free(databuf);
	free(framebuf);
	fclose(F);

	if(totalframes_nb==0) {
		printf("ERROR: No frames were found in file '%s'\n", filename);
		return -1;
	}

	// Print stats
	printf("Stats:\n");
	printf("  Frames ...... %u\n", totalframes_nb);
	diff = TimeDouble_From64(totime_file);
	printf("  Time, file .. %g s, %g frames/s\n", diff, totalframes_nb / diff);
	diff = TimeDouble_From64(totime_nn);
	printf("  Time, FPGA .. %g s, %g frames/s\n", diff, totalframes_nb / diff);

	return 0;
}

int HwAcc_Common::write_frames(Network* network, const char* filename) {
	layer_t* inlayer = NULL;
	layer_t* outlayer = NULL;

	// Get first and last layers of the network
	for(layer_t* layer = network->layer_first; layer != NULL; layer = layer->next) {
		if(layer->type != LAYER_FIFO) { inlayer = layer; break; }
	}
	for(layer_t* layer = network->layer_last; layer != NULL; layer = layer->prev) {
		if(layer->type != LAYER_FIFO) { outlayer = layer; break; }
	}

	// If an output layer was selected, ensure it is handled by the hardware
	if(param_out_layer!=NULL) {
		if(accreg_selout==false && param_out_layer!=outlayer) {
			printf("ERROR: The hardware supports only output at last layer\n");
			return -1;
		}
		if(network->param_selout==false && param_out_layer!=outlayer) {
			printf("Warning: Using default output layer\n");
		}
		else {
			outlayer = param_out_layer;
		}
	}

	unsigned errors_nb = 0;
	if(inlayer==NULL) {
		printf("ERROR: Could not find first layer\n");
		errors_nb ++;
	}
	if(outlayer==NULL) {
		printf("ERROR: Could not find last layer\n");
		errors_nb ++;
	}
	if(errors_nb != 0) return -1;

	int z = write_frames_inout(filename, inlayer, outlayer);

	return z;
}



//============================================
// Global usage
//============================================

void HwAcc_Common::run(Network* network) {

	// Ensure PCIe is initialized
	accreg_config_get();

	if(param_hw_force == false) {
		// If no network is built, try to do it from PCIe
		if(network->layers.size() == 0) {
			build_network(network);
		}
	}

	// Ensure the network is built
	check_have_hwacc(network);

	if(param_hw_force == false) {
		// Update the config registers from the layer structures + send them to the FPGA
		write_config_regs(network);
	}
	else {
		if(network->param_cnn_origin != Network::CNN_ORIGIN_HARDWARE) {
			printf("Error: The NN must be based on PCIe design\n");
			exit(EXIT_FAILURE);
		}
	}

	// Print accelerator details
	accreg_config_print();

	// Send configuration data
	write_config(network);

	// Finally, send the frames
	if(filename_frames!=NULL) {
		write_frames(network, filename_frames);
	}
}

