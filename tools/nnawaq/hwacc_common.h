
#pragma once

extern "C" {

#include <stdint.h>
#include <stdbool.h>

}

#include <vector>


// Object that represents one HW target
// This is a virtual class, has to be inherited to provide the implementation of methods

class HwAcc_Common {

	public:

	//============================================
	// Definition of config bits of HW accelerator
	//============================================

	static const unsigned ACCREG3_NBREGS_SHIFT      = 24;
	static const uint32_t ACCREG3_NBREGS_MASK       = 0xFF000000;

	static const uint32_t ACCREG3_FLAG_CLEAR        = 0x00000001;
	static const uint32_t ACCREG3_FLAG_FREERUN      = 0x00000002;
	static const uint32_t ACCREG3_FLAG_SELOUT       = 0x00000004;
	static const uint32_t ACCREG3_FLAG_FIFOMON      = 0x00000008;
	static const uint32_t ACCREG3_FLAG_CFGSHEN      = 0x00000100;
	static const uint32_t ACCREG3_FLAG_CFGGET       = 0x00000200;
	static const uint32_t ACCREG3_FLAG_CFGSET       = 0x00000400;
	static const uint32_t ACCREG3_FLAG_LOCKED       = 0x00000800;

	static const uint32_t ACCREG4_WMODE1_FRAME      = 0x000000FF;

	// For version starting from v1.0
	static const unsigned ACCREG4_WMODE1_SHIFT      = 0;
	static const uint32_t ACCREG4_WMODE1_MASK       = 0x0000003F;
	static const unsigned ACCREG4_WMODE2_SHIFT      = 6;
	static const uint32_t ACCREG4_WMODE2_MASK       = 0x0000FFC0;

	// For version up to v0.4
	static const unsigned ACCREG4_V0_WMODE1_SHIFT   = 0;
	static const uint32_t ACCREG4_V0_WMODE1_MASK    = 0x000000FF;
	static const uint32_t ACCREG4_V0_WMODE1_FRAME   = 0x000000FF;
	static const unsigned ACCREG4_V0_WMODE2_SHIFT   = 8;
	static const uint32_t ACCREG4_V0_WMODE2_MASK    = 0x0000FF00;

	static const unsigned ACCREG4_RECV1_SHIFT       = 16;
	static const uint32_t ACCREG4_RECV1_MASK        = 0x00FF0000;
	static const unsigned ACCREG4_RECV2_SHIFT       = 24;
	static const uint32_t ACCREG4_RECV2_MASK        = 0xFF000000;

	static const unsigned ACCREG15_FIFOSEL_SHIFT    = 24;
	static const uint32_t ACCREG15_FIFOSEL_MASK     = 0xFF000000;

	//============================================
	// Fields common to all HW targets
	//============================================

	unsigned  accreg_ifw      = 0;
	unsigned  accreg_ifw32    = 0;  // Number of 32b words for one interface data sample
	unsigned  accreg_wdata    = 0;
	bool      accreg_selout   = false;
	bool      accreg_fifomon  = false;
	bool      accreg_locked   = false;
	unsigned  accreg_id       = 0;
	unsigned  accreg_id_maj   = 0;
	unsigned  accreg_id_min   = 0;
	unsigned  accreg_cfgnn_nb = 0;

	// The vector of config registers
	std::vector<uint32_t> accreg_cfgnn;

	//============================================
	// Constructor / destructor
	//============================================

	HwAcc_Common(void);

	// The destructor is virtual because this is a base class to be inherited
	virtual ~HwAcc_Common(void);

	//============================================
	// Virtual methods
	//============================================

	// Access configuration registers
	virtual uint32_t accreg_rd(unsigned idx) { return 0; }
	virtual void     accreg_wr(unsigned idx, uint32_t val) {}

	// Streams of data
	virtual int fpga_send32(uint32_t* buf, unsigned buf_nb) { return 0; }
	virtual int fpga_send32_wait(uint32_t* buf, unsigned buf_nb) { return 0; }  // There may be an additional wait to ensure data was processed indeed
	virtual int fpga_recv32(uint32_t* buf, unsigned buf_nb) { return 0; }

	//============================================
	// Utility macros
	//============================================

	inline int accreg_ver_cmp(unsigned maj, unsigned min) const {
		return accreg_id_maj != (maj) ? (int)accreg_id_maj - (int)(maj) : (int)accreg_id_min - (int)(min);
	}

	inline void accreg_set_wmode1(unsigned i) {
		if(accreg_id_maj==0)
			accreg_wr(4, (accreg_rd(4) & ~ACCREG4_V0_WMODE1_MASK) | (((i) << ACCREG4_V0_WMODE1_SHIFT) & ACCREG4_V0_WMODE1_MASK));
		else
			accreg_wr(4, (accreg_rd(4) & ~ACCREG4_WMODE1_MASK) | (((i) << ACCREG4_WMODE1_SHIFT) & ACCREG4_WMODE1_MASK));
	}
	inline void accreg_set_wmode2(unsigned i) {
		if(accreg_id_maj==0)
			accreg_wr(4, (accreg_rd(4) & ~ACCREG4_V0_WMODE2_MASK) | (((i) << ACCREG4_V0_WMODE2_SHIFT) & ACCREG4_V0_WMODE2_MASK));
		else
			accreg_wr(4, (accreg_rd(4) & ~ACCREG4_WMODE2_MASK) | (((i) << ACCREG4_WMODE2_SHIFT) & ACCREG4_WMODE2_MASK));
	}

	inline void accreg_set_recv1(unsigned i) { accreg_wr(4, (accreg_rd(4) & ~ACCREG4_RECV1_MASK) | (((i) << ACCREG4_RECV1_SHIFT) & ACCREG4_RECV1_MASK)); }
	inline void accreg_set_recv2(unsigned i) { accreg_wr(4, (accreg_rd(4) & ~ACCREG4_RECV2_MASK) | (((i) << ACCREG4_RECV2_SHIFT) & ACCREG4_RECV2_MASK)); }

	inline void accreg_clear(void)         { accreg_wr(3, accreg_rd(3) | ACCREG3_FLAG_CLEAR); }
	inline void accreg_freerun_set(void)   { accreg_wr(3, accreg_rd(3) | ACCREG3_FLAG_FREERUN); }
	inline void accreg_freerun_clear(void) { accreg_wr(3, accreg_rd(3) & ~ACCREG3_FLAG_FREERUN); }

	inline void     accreg_cfgnn_sh_en(void)      { accreg_wr(3, accreg_rd(3) | ACCREG3_FLAG_CFGSHEN); }
	inline void     accreg_cfgnn_sh_dis(void)     { accreg_wr(3, accreg_rd(3) & ~ACCREG3_FLAG_CFGSHEN); }
	inline void     accreg_cfgnn_set(void)        { accreg_wr(3, accreg_rd(3) | ACCREG3_FLAG_CFGSET); }
	inline uint32_t accreg_cfgnn_pop(void)        { return accreg_rd(1); }
	inline void     accreg_cfgnn_push(uint32_t v) { accreg_wr(1, v); }

	inline unsigned accreg_cfgnn_get_nb(void)     { return ((accreg_rd(3) & ACCREG3_NBREGS_MASK) >> ACCREG3_NBREGS_SHIFT); }

	inline void     accreg_set_nboutputs(unsigned nb) { accreg_wr(6, nb); }
	inline unsigned accreg_get_nboutputs(void)        { return accreg_rd(6); }
	// FIXME Give another name to this method
	inline unsigned accreg_get_pcierecv32(void)       { return accreg_rd(7); }

	inline unsigned accreg_get_fifosel(void)       { return ((accreg_rd(15) & ACCREG15_FIFOSEL_MASK) >> ACCREG15_FIFOSEL_SHIFT); }
	inline void     accreg_set_fifosel(unsigned i) { accreg_wr(15, (accreg_rd(15) & ~ACCREG15_FIFOSEL_MASK) | (((i) << ACCREG15_FIFOSEL_SHIFT) & ACCREG15_FIFOSEL_MASK)); }

	//============================================
	// Methods
	//============================================

	void accreg_print_regs(void);

	void accreg_cfgnn_read(void);
	void accreg_cfgnn_write(void);
	void accreg_cfgnn_print(void);

	void accreg_config_get(void);
	void accreg_config_print(void);

	void check_have_hwacc(Network* network);

	private :
	int  build_network_internal(Network* network);

	public :
	void build_network(Network* network);
	int  write_config_regs(Network* network);
	void print_fifos(Network* network);

	//============================================
	// Methods for using the HW accelerator
	//============================================

	// FIXME It may be convenient to have one Network instance owned by the HwAcc object
	// Or create the HwAcc instance with a reference to an external Network object

	int write_layer_config(layer_t* layer);
	int write_config(Network* network);

	// These methods should be private, but for now need to be public to be called from extrernal thread function
	void* getoutputs_thread(layer_t* layer, unsigned frames_nb);
	int write_frames_inout(const char* filename, layer_t* inlayer, layer_t* outlayer);

	int write_frames(Network* network, const char* filename);

	void run(Network* network);

};

