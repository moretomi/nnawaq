
// This file contains utility functions to talk to a hardware accelerator
// Implementation is not specific to a particular HW target

extern "C" {

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>  // For usleep()

#include "nnawaq_utils.h"

}

#include "nn_layers_utils.h"
#include "hwacc_common.h"

using namespace std;


//============================================
// Constructor / destructor
//============================================

HwAcc_Common::HwAcc_Common(void) {
	// All initialization is the init values of fields, already specified
}

HwAcc_Common::~HwAcc_Common(void) {
	// Nothing specific to destruct : all object fields have their own destructor
}


//============================================
// Methods
//============================================

// Utility function to print the state of relevant IP registers
void HwAcc_Common::accreg_print_regs(void) {
	printf("Registers of the hardware accelerator:\n");
	printf(" ");
	for(unsigned i=0; i<16; i++) {
		if(i > 0) printf(" ");
		printf("%08x", accreg_rd(i));
	}
	printf("\n");
}

void HwAcc_Common::accreg_cfgnn_read(void) {
	if(accreg_cfgnn_nb == 0) return;
	// Get registers in the scan chain
	accreg_cfgnn_set();
	// Enable register shift
	accreg_cfgnn_sh_en();
	// Read registers
	accreg_cfgnn.reserve(accreg_cfgnn_nb);
	for(unsigned i=0; i<accreg_cfgnn_nb; i++) accreg_cfgnn[i] = accreg_cfgnn_pop();
	// Disable register shift
	accreg_cfgnn_sh_dis();
}
void HwAcc_Common::accreg_cfgnn_write(void) {
	if(accreg_cfgnn_nb==0) return;
	// Enable register shift
	accreg_cfgnn_sh_en();
	// Read registers
	for(unsigned i=0; i<accreg_cfgnn_nb; i++) accreg_cfgnn_push(accreg_cfgnn[i]);
	// Disable register shift
	accreg_cfgnn_sh_dis();
	// Set registers from the scan chain
	accreg_cfgnn_set();
}
void HwAcc_Common::accreg_cfgnn_print(void) {
	printf("Registers of the network pipeline (number %u):\n", accreg_cfgnn_nb);
	for(unsigned i=0; i<accreg_cfgnn_nb; i++) {
		printf("  0x%08x\n", accreg_cfgnn[i]);
	}
}

// Get IP configuration
void HwAcc_Common::accreg_config_get(void) {
	if(accreg_cfgnn_nb > 0) return;

	unsigned r = 0;

	// Get the accelerator ID
	accreg_id = accreg_rd(0);
	// Parse the ID
	char id_n1 = (accreg_id >> 0) & 0xFF;
	char id_n2 = (accreg_id >> 8) & 0xFF;
	accreg_id_maj = (accreg_id >> 16) & 0xFF;
	accreg_id_min = (accreg_id >> 24) & 0xFF;

	if(id_n1!='N' || id_n2!='N') {
		printf("RIFFA Error: Unknown accelerator ID 0x%08x\n", accreg_id);
		exit(EXIT_FAILURE);
	}

	// Check
	if(accreg_ver_cmp(2, 0) < 0) {
		printf("RIFFA Error: Accelerator version %u.%u not handled, too low\n", accreg_id_maj, accreg_id_min);
		exit(EXIT_FAILURE);
	}
	if(accreg_id_maj > 2) {
		printf("RIFFA Error: Accelerator version %u.%u not handled, too high\n", accreg_id_maj, accreg_id_min);
		exit(EXIT_FAILURE);
	}

	// Read the number of config registers
	accreg_cfgnn_nb = accreg_cfgnn_get_nb();
	// Read the flags
	r = accreg_rd(3);
	accreg_wdata = (r >> 4) & 0x0F;
	accreg_wdata = 1 << accreg_wdata;
	accreg_selout  = (r & ACCREG3_FLAG_SELOUT) != 0 ? true : false;
	accreg_fifomon = (r & ACCREG3_FLAG_FIFOMON) != 0 ? true : false;
	accreg_locked  = (r & ACCREG3_FLAG_LOCKED) != 0 ? true : false;
	if(accreg_ver_cmp(1, 2) >= 0) {
		accreg_ifw = (r >> 12) & 0x3F;
		accreg_ifw *= 8;
	}
	else {
		accreg_ifw = 128;
	}

	if(accreg_ifw == 0) {
		printf("Error: The accelerator says its interface width is zero\n");
		exit(EXIT_FAILURE);
	}

	accreg_ifw32 = (accreg_ifw + 31) / 32;

	// Read the NN config registers
	accreg_cfgnn_read();
}

// Print IP configuration
void HwAcc_Common::accreg_config_print(void) {
	printf("RIFFA : Summary of IP configuration:\n");
	printf("  Accelerator ID ........... 0x%08x\n", accreg_id);
	printf("  Accelerator version ...... %u.%u\n", accreg_id_maj, accreg_id_min);
	printf("  Data width (PCIe layer) .. %u\n", accreg_wdata);
	printf("  Can select output layer .. %s\n", accreg_selout==true ? "yes" : "no");
	printf("  Can monitor FIFOs ........ %s\n", accreg_fifomon==true ? "yes" : "no");
	printf("  Registers are locked ..... %s\n", accreg_locked==true ? "yes" : "no");
	printf("  NN Config registers ...... %u\n", (unsigned)accreg_cfgnn.size());
}

// Ensure the current NN was built from PCIe
// FIXME The variable cnn_origin should also be checked if forced mode is not active
void HwAcc_Common::check_have_hwacc(Network* network) {
	if(accreg_cfgnn_nb == 0 || network->layers.size() == 0) {
		printf("Error: The NN must be based on Hardware Accelerator design\n");
		exit(EXIT_FAILURE);
	}
}


//============================================
// Building network from PCIe accelerator
//============================================

void Layer::config_from_regs(const std::vector<uint32_t>& accreg_cfgnn) {
	// Nothing to configure
}

void LayerWin::config_from_regs(const std::vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned i = layer->regs_idx;

	// Parse registers
	layer->regs_nb = 1 + 6;

	// Parse registers
	r = accreg_cfgnn[++i];
	layer->fx      = (r >>  0) & 0x0000FFFF;
	layer->fx_max  = (r >> 16) & 0x0000FFFF;
	r = accreg_cfgnn[++i];
	layer->fy      = (r >>  0) & 0x0000FFFF;
	layer->fy_max  = (r >> 16) & 0x0000FFFF;
	r = accreg_cfgnn[++i];
	layer->fz      = (r >>  0) & 0x0000FFFF;
	layer->fz_max  = (r >> 16) & 0x0000FFFF;
	r = accreg_cfgnn[++i];
	layer->stepx   = (r >>  0) & 0x0000003F;
	layer->stepy   = (r >>  6) & 0x0000003F;
	layer->winx    = (r >> 12) & 0x0000003F;
	layer->winy    = (r >> 18) & 0x0000003F;
	layer->begpadx = (r >> 24) & 0x0000000F;
	layer->begpady = (r >> 28) & 0x0000000F;
	r = accreg_cfgnn[++i];
	layer->nwinx   = (r >>  0) & 0x0000FFFF;
	layer->nwiny   = (r >> 16) & 0x0000FFFF;
	r = accreg_cfgnn[++i];
	layer->nwinz = layer->fz;
	layer->win_par_oz = (r >> 16) & 0x0000FFFF;

	// Apply parallelism multiplier
	layer->fz     *= layer->win_par_oz;
	layer->fz_max *= layer->win_par_oz;
	layer->nwinz  *= layer->win_par_oz;

}

void LayerNeu::config_from_regs(const std::vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned i = layer->regs_idx;

	layer->regs_nb = 1 + 3;

	r = accreg_cfgnn[++i];
	layer->fsize       = (r >>  0) & 0x0000FFFF;
	layer->fsize_max   = (r >> 16) & 0x0000FFFF;
	r = accreg_cfgnn[++i];
	layer->neurons     = (r >>  0) & 0x0000FFFF;
	layer->neurons_max = (r >> 16) & 0x0000FFFF;

	r = accreg_cfgnn[++i];
	layer->neu_per_bram = (r >>  0) & 0x3F;
	layer->neu_wrnb     = (r >>  6) & 0x3F;
	layer->neu_wweight  = (r >> 12) & 0x3F;
	layer->neu_sgnd = 0;
	if(((r >> 18) & 0x01) != 0) layer->neu_sgnd |= NEUSGN_LOCKED;
	if(((r >> 19) & 0x01) != 0) layer->neu_sgnd |= NEUSGN_SIGNED;
	else layer->neu_sgnd |= NEUSGN_UNSIGNED;
	layer->neu_sgnd |= NEUSGN_VALID;
	layer->neu_style = (r >> 22) & 0x03;
	// Fields for custom multiplication operations
	layer->neu_custom_mul    = (r >> 24) & 0x01;
	layer->neu_custom_mul_id = (r >> 25) & 0x3F;
	// Weight signedness
	layer->neu_sgnd = 0;
	if(((r >> 31) & 0x01) != 0) layer->neu_sgnw |= NEUSGN_SIGNED;
	layer->neu_sgnw |= NEUSGN_LOCKED;
	layer->neu_sgnw |= NEUSGN_VALID;

	// Checks
	if(layer->neu_per_bram == 0) {
		printf("Warning layer %u, neuron: Field neu_per_bram is zero, setting to 64\n", (unsigned)network->layers.size());
		layer->neu_per_bram = 64;
	}

	// Apply parallelism
	layer->fsize       *= layer->split_in;
	layer->fsize_max   *= layer->split_in;
	layer->neurons     *= layer->split_out;
	layer->neurons_max *= layer->split_out;
	// Protection against uninitialized registers
	if(layer->fsize==0 || layer->fsize > layer->fsize_max) layer->fsize = layer->fsize_max;
	if(layer->neurons==0 || layer->neurons > layer->neurons_max) layer->neurons = layer->neurons_max;

}

void LayerPool::config_from_regs(const std::vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned i = layer->regs_idx;

	layer->regs_nb = 1 + 1;
	r = accreg_cfgnn[++i];
	layer->fsize = (r >> 0) & 0x0000FFFF;

}

void LayerNorm::config_from_regs(const std::vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned i = layer->regs_idx;

	layer->regs_nb = 1 + 3;

	r = accreg_cfgnn[++i];
	layer->fsize     = (r >>  0) & 0x0000FFFF;
	layer->fsize_max = (r >> 16) & 0x0000FFFF;
	// Apply parallelism
	layer->fsize     *= layer->split_in;
	layer->fsize_max *= layer->split_in;
	// Protection against uninitialized registers
	if(layer->fsize==0 || layer->fsize > layer->fsize_max) layer->fsize = layer->fsize_max;
	// Miscellaneous flags
	r = accreg_cfgnn[++i];
	layer->sdata      = (r >>  0) & 0x01;
	layer->out_sdata  = (r >>  1) & 0x01;
	int bias_en       = (r >>  2) & 0x01;
	int mul_en        = (r >>  3) & 0x01;
	// Width of run-time shift
	layer->norm_wshr  = (r >>  5) & 0x07;
	// Output data width
	layer->out_wdata  = ((r >> 16) & 0x1F) + 1;
	// Width of run-time bias
	layer->norm_wbias = (bias_en == 0) ? 0 : ((r >> 21) & 0x1F) + 1;
	// Constant shift
	layer->norm_shr_cst = (r >> 26) & 0x1F;
	// Constant multiplier
	r = accreg_cfgnn[++i];
	layer->norm_mul_cst = (r >> 0) & 0xFFFFFF;
	// Width of run-time multiplier
	layer->norm_wmul = (mul_en == 0) ? 0 : ((r >> 24) & 0x1F) + 1;

}

void LayerTernarize::config_from_regs(const std::vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned i = layer->regs_idx;

	layer->regs_nb = 1 + 1;
	r = accreg_cfgnn[++i];
	layer->fsize     = (r >>  0) & 0x0000FFFF;
	layer->fsize_max = (r >> 16) & 0x0000FFFF;
	// Apply parallelism
	layer->fsize     *= layer->split_in;
	layer->fsize_max *= layer->split_in;
	// Protection against uninitialized registers
	if(layer->fsize==0 || layer->fsize > layer->fsize_max) layer->fsize = layer->fsize_max;

}

void LayerRelu::config_from_regs(const std::vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned i = layer->regs_idx;

	layer->regs_nb = 1 + 3;

	r = accreg_cfgnn[++i];
	layer->fsize     = (r >>  0) & 0x0000FFFF;
	// Apply parallelism
	layer->fsize     *= layer->split_in;
	layer->fsize_max *= layer->fsize;
	// Protection against uninitialized registers
	if(layer->fsize==0 || layer->fsize > layer->fsize_max) layer->fsize = layer->fsize_max;
	// Output data width
	layer->out_wdata  = ((r >> 16) & 0x1F) + 1;
	// Miscellaneous flags
	layer->sdata      = (r >>  21) & 0x01;
	layer->out_sdata  = (r >>  22) & 0x01;
	// Min/max parameters
	r = accreg_cfgnn[++i];
	layer->relu_min = r;
	r = accreg_cfgnn[++i];
	layer->relu_max = r;

}

void LayerCustom::config_from_regs(const std::vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned i = layer->regs_idx;

	layer->regs_nb = 1 + 1;

	r = accreg_cfgnn[++i];
	layer->fsize     = (r >>  0) & 0x0000FFFF;
	// Apply parallelism
	layer->fsize     *= layer->split_in;
	layer->fsize_max *= layer->fsize;
	// Protection against uninitialized registers
	if(layer->fsize==0 || layer->fsize > layer->fsize_max) layer->fsize = layer->fsize_max;
	// Output data width
	layer->out_wdata  = ((r >> 16) & 0x1F) + 1;
	// Miscellaneous flags
	layer->sdata      = (r >>  21) & 0x01;
	layer->out_sdata  = (r >>  22) & 0x01;
	// Custom activation function ID
	layer->custom_user_id = (r >> 24) & 0xFF;

}

void LayerSoftMax::config_from_regs(const std::vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned i = layer->regs_idx;

	layer->regs_nb = 1 + 1;

	r = accreg_cfgnn[++i];
	layer->fsize     = (r >>  0) & 0x0000FFFF;
	layer->fsize_max = layer->fsize;  // No max is really necessary for this component
	// Apply parallelism
	layer->fsize     *= layer->split_in;
	layer->fsize_max *= layer->split_in;
	// Output data width
	layer->out_wdata  = ((r >> 16) & 0x1F) + 1;
	// Miscellaneous flags
	layer->sdata      = (r >>  21) & 0x01;
	layer->out_sdata  = 0;
}

// Build from config registers read from PCIe
static layer_t* build_layer_getcommon(Network* network, uint32_t r) {
	unsigned type = r & 0x0000000F;
	if(type==LAYER_NONE) return NULL;

	layer_t* layer = network->layer_new_enqueue_fromtype(type);
	if(layer==NULL) return NULL;

	// FIXME This can't handle PAR > 127
	layer->split_in  = (r >>  4) & 0x0000007F;
	layer->split_out = (r >> 11) & 0x0000007F;

	layer->wdata     = (r >> 18) & 0x0000007F;
	layer->out_wdata = (r >> 25) & 0x0000007F;

	return layer;
}

int HwAcc_Common::build_network_internal(Network* network) {
	// Clear any existing NN
	network->clear();

	if(accreg_cfgnn_nb==0) {
		printf("Error PCIE: No config registers\n");
		exit(EXIT_FAILURE);
	}

	layer_t* layer = NULL;

	// Scan all config registers
	for(unsigned i=0; i<accreg_cfgnn_nb; i++) {
		uint32_t r = accreg_cfgnn[i];
		if(i == 0) continue;  // First word only ?

		layer = build_layer_getcommon(network, r);
		if(layer==NULL) {
			printf("Error PCIE: Could not create a NN layer from config register %u value 0x%08x\n", i, r);
			exit(EXIT_FAILURE);
		}

		layer->regs_idx = i;
		layer->config_from_regs(accreg_cfgnn);
		i += layer->regs_nb;
		if(layer->regs_nb == 0) i++;  // Workaround for layer types with no config method

		// Propagate parameters
		propag_params_layer(network, layer);

	}  // Scan all config registers

	// Get last parameters
	network->param_win = network->layer_first->wdata;
	network->param_sin = network->layer_first->sdata;

	// Set the number of neurons of the output layer
	if(network->cnn_outneu > 0 && accreg_locked == false) {
		apply_outneu(network, network->cnn_outneu);
	}

	return 0;
}

void HwAcc_Common::build_network(Network* network) {

	// Ensure PCIe is initialized
	accreg_config_get();

	// Ensure there is no network
	if(network->layers.size() != 0) {
		printf("Error: A network is already built\n");
		exit(EXIT_FAILURE);
	}

	build_network_internal(network);
	network->param_cnn_origin = Network::CNN_ORIGIN_HARDWARE;
	network->param_locked = accreg_locked;
}


//============================================
// Write config registers to HW accelerator
//============================================

#define SETREG(r, v, m, sh) do { \
	r = ((r) & ~((m) << (sh))) | (((v) & (m)) << (sh)); \
} while(0)

void Layer::write_config_regs(vector<uint32_t>& accreg_cfgnn) {
	// Nothing to write
}

void LayerWin::write_config_regs(vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	// Fx
	r = accreg_cfgnn[layer->regs_idx + 1];
	SETREG(r, layer->fx, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 1] = r;
	// Fy
	r = accreg_cfgnn[layer->regs_idx + 2];
	SETREG(r, layer->fy, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 2] = r;
	// Fz
	r = accreg_cfgnn[layer->regs_idx + 3];
	SETREG(r, layer->fz / layer->win_par_oz, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 3] = r;
	// Step x y, begpad x y
	r = accreg_cfgnn[layer->regs_idx + 4];
	SETREG(r, layer->stepx, 0x003F, 0);
	SETREG(r, layer->stepy, 0x003F, 6);
	SETREG(r, layer->begpadx, 0x000F, 24);
	SETREG(r, layer->begpady, 0x000F, 28);
	accreg_cfgnn[layer->regs_idx + 4] = r;
	// Nwin x y
	r = accreg_cfgnn[layer->regs_idx + 5];
	SETREG(r, layer->nwinx, 0x0000FFFF, 0);
	SETREG(r, layer->nwiny, 0x0000FFFF, 16);
	accreg_cfgnn[layer->regs_idx + 5] = r;
	// Rcycles z
	// PAR_OZ
	r = accreg_cfgnn[layer->regs_idx + 6];
	SETREG(r, layer->win_par_oz, 0x0000FFFF, 16);
	accreg_cfgnn[layer->regs_idx + 6] = r;
}

void LayerNeu::write_config_regs(vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned fsize = (layer->fsize + layer->split_in - 1) / layer->split_in;
	unsigned nbneu = (layer->neurons + layer->split_out - 1) / layer->split_out;
	// Fsize
	r = accreg_cfgnn[layer->regs_idx + 1];
	SETREG(r, fsize, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 1] = r;
	// Neurons
	r = accreg_cfgnn[layer->regs_idx + 2];
	SETREG(r, nbneu, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 2] = r;
	// Flags
	if((layer->neu_sgnd & NEUSGN_LOCKED) == 0) {
		r = accreg_cfgnn[layer->regs_idx + 3];
		SETREG(r, layer->neu_sgnd & NEUSGN_SIGNED ? 1 : 0, 0x01, 19);
		accreg_cfgnn[layer->regs_idx + 3] = r;
	}
}

void LayerPool::write_config_regs(vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	// Fsize
	r = accreg_cfgnn[layer->regs_idx + 1];
	SETREG(r, layer->fsize, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 1] = r;
}

void LayerNorm::write_config_regs(vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned fsize = (layer->fsize + layer->split_in - 1) / layer->split_in;
	r = accreg_cfgnn[layer->regs_idx + 1];
	// Frame size
	SETREG(r, fsize, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 1] = r;
}

void LayerTernarize::write_config_regs(vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned fsize = (layer->fsize + layer->split_in - 1) / layer->split_in;
	r = accreg_cfgnn[layer->regs_idx + 1];
	SETREG(r, fsize, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 1] = r;
}

void LayerRelu::write_config_regs(vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned fsize = (layer->fsize + layer->split_in - 1) / layer->split_in;
	r = accreg_cfgnn[layer->regs_idx + 1];
	// Frame size
	SETREG(r, fsize, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 1] = r;
	// Min/max ReLU parameters
	accreg_cfgnn[layer->regs_idx + 2] = layer->relu_min;
	accreg_cfgnn[layer->regs_idx + 3] = layer->relu_max;
}

void LayerSoftMax::write_config_regs(vector<uint32_t>& accreg_cfgnn) {
	Layer* layer = this;
	uint32_t r = 0;
	unsigned fsize = (layer->fsize + layer->split_in - 1) / layer->split_in;
	r = accreg_cfgnn[layer->regs_idx + 1];
	// Frame size
	SETREG(r, fsize, 0x0000FFFF, 0);
	accreg_cfgnn[layer->regs_idx + 1] = r;
}

#undef SETREG

int HwAcc_Common::write_config_regs(Network* network) {
	auto& layers = network->layers;

	// Ensure the design is based on Harwdare Accelerator design
	check_have_hwacc(network);

	// Scan all layers
	for(auto layer : layers) {
		layer->write_config_regs(accreg_cfgnn);
	}

	// Commit these changes into the accelerator
	accreg_cfgnn_write();

	return 0;
}


//============================================
// Print the status of all FIFOs
//============================================

void HwAcc_Common::print_fifos(Network* network) {
	auto& layers = network->layers;

	// Ensure PCIe is initialized
	accreg_config_get();

	// Check that FIFO monitoring is enabled
	if(accreg_fifomon==false) {
		printf("Error: The accelerator does not have the FIFO monitoring option\n");
		exit(EXIT_FAILURE);
	}

	// No need to build the network in order to know the number of FIFOs
	// FIXME the number of fifos may not be equal to layers+1 in the near future
	if(param_hw_force == false) {
		// If no network is built, try to do it from PCIe
		if(layers.size() == 0) {
			build_network(network);
		}
	}
	// Ensure the network is built
	check_have_hwacc(network);

	// Scan all FIFOs
	// FIXME the number of fifos may not be equal to layers+1 in the near future, need a separate reg for the number of FIFOs
	for(unsigned i=0; i<=layers.size(); i++) {
		accreg_set_fifosel(i);
		usleep(15);
		unsigned data = accreg_rd(15);
		printf("Fifo %02u in rdy/ack %u/%u out rdy/ack %u/%u cnt %u\n", i,
			(data >> 8) & 1, (data >> 9) & 1, (data >> 10) & 1, (data >> 11) & 1, data & 0xFF
		);
	}
}

