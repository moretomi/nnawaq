
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include <time.h>
#include <signal.h>
#include <unistd.h>

// for the serial port config
#include <fcntl.h>
#include <termios.h>


int TTYS;

char* filename_log = NULL;
FILE* file_log = NULL;

unsigned log_persec = 1;

bool do_vc709 = false;
bool do_zc706 = false;
bool do_zcu102 = false;

bool do_scan = false;
bool do_read = false;
bool do_stop = false;
bool do_disp = true;

// If true, lots of messages are printed on standard output
bool debug = false;


//============================================
// Time
//============================================

// Declarations

int64_t Time64_GetReal();
double TimeDouble_From64(int64_t v64);
double TimeDouble_DiffCurrReal_From64(int64_t oldtime);

// Implementation

#define TIME64PERSEC 1000000000

double TimeDouble_FromTS(struct timespec *ts) {
	return (double)ts->tv_sec + ((double)ts->tv_nsec)/1.0e9;
}
double TimeDouble_From64(int64_t v64) {
	return ((double)v64) / 1.0e9;
}
int64_t Time64_FromTS(struct timespec *ts) {
	return (int64_t)ts->tv_sec * TIME64PERSEC + ts->tv_nsec;
}
void TimeSpec_From64(struct timespec *ts, int64_t v64) {
	ts->tv_sec = v64 / TIME64PERSEC;
	ts->tv_nsec = v64 % TIME64PERSEC;
}

// Important: This returns the system absolute time
void TimeSpec_GetReal(struct timespec *ts) {
	clock_gettime(CLOCK_REALTIME, ts);
}

int64_t Time64_GetReal() {
	struct timespec ts;
	TimeSpec_GetReal(&ts);
	return Time64_FromTS(&ts);
}

double TimeDouble_GetDiff_FromTS(struct timespec *oldtime, struct timespec *newtime) {
	return TimeDouble_From64(Time64_FromTS(newtime) -  Time64_FromTS(oldtime));
}

double TimeDouble_DiffCurrReal_From64(int64_t oldtime) {
	return TimeDouble_From64(Time64_GetReal() - oldtime);
}


//========================================================
// Get Ctr-C signal
//========================================================

volatile int sig_do_stop = 0;

void intHandler(int dummy) {
	sig_do_stop = 1;
}


//========================================================
// UART-to-I2C basic bridge definitions
//========================================================

#define CST_ACK    0x01
#define CST_NACK   0x02
#define CST_ERROR  0x04

#define CMD_START    0x00
#define CMD_RESTART  0x01
#define CMD_STOP     0x02
#define CMD_ACK      0x03
#define CMD_NACK     0x04
#define CMD_WRITE    0x05
#define CMD_READ     0x06

static const char* cmd_name[] = {
	"START",
	"RESTART",
	"STOP",
	"ACK",
	"NACK",
	"WRITE",
	"READ"
};
static char* string_unknown = "unknown";

const char* cmd_get_name(uint8_t cmd) {
	if(cmd <= 6) return cmd_name[cmd];
	return string_unknown;
}

uint8_t uart_read_byte() {
	int z, c = 0;
	z = read(TTYS, &c, 1);
	if(z==EOF) {
		printf("read: EOF received\n");
		exit(EXIT_FAILURE);
	}
	if(z==0) {
		printf("read: timeout\n");
		exit(EXIT_FAILURE);
	}
	if(debug==true) {
		printf("read %02x\n", c & 0xFF);
	}
	return c;
}

uint8_t uart_read_ack() {
	int z, c = 0;
	z = read(TTYS, &c, 1);
	if(z==EOF) {
		printf("read: EOF received\n");
		exit(EXIT_FAILURE);
	}
	if(z==0) {
		printf("read: timeout\n");
		exit(EXIT_FAILURE);
	}
	if(debug==true) {
		c = c & 0xff;
		char* name = string_unknown;
		if(c==CST_ACK)   name = "ACK";
		if(c==CST_NACK)  name = "NACK";
		if(c==CST_ERROR) name = "ERROR";
		printf("read %02x %s\n", c & 0xFF, name);
	}
	return c;
}

void uart_send_cmd(int c) {
	int z;
	if(debug==true) {
		printf("send %02x %s\n", c & 0xFF, cmd_get_name(c));
	}
	z = write(TTYS, &c, 1);
	if(z==EOF) {
		printf("send: EOF received\n");
		exit(EXIT_FAILURE);
	}
}

void uart_send_addr(int c, int read_en) {
	int z;
	c = (c << 1) | read_en;
	if(debug==true) {
		printf("send %02x addr %u read %u\n", c & 0xFF, c >> 1, read_en);
	}
	z = write(TTYS, &c, 1);
	if(z==EOF) {
		printf("send: EOF received\n");
		exit(EXIT_FAILURE);
	}
}
void uart_send_addr_write(int c) {
	uart_send_addr(c, 0);
}
void uart_send_addr_read(int c) {
	uart_send_addr(c, 1);
}

void uart_send_byte(int c) {
	int z;
	if(debug==true) {
		printf("send %02x\n", c & 0xFF);
	}
	z = write(TTYS, &c, 1);
	if(z==EOF) {
		printf("send: EOF received\n");
		exit(EXIT_FAILURE);
	}
}


//========================================================
// Functions that implement read/write of slave registers
//========================================================

void slave_read_reg(uint8_t addr, uint8_t reg, uint8_t* buf, unsigned nb) {
	unsigned ack;
	// Start
	uart_send_cmd(CMD_START);
	// Send slave address
	uart_send_cmd(CMD_WRITE);
	uart_send_addr_write(addr);
	ack = uart_read_ack();
	if(ack!=CST_ACK) exit(EXIT_FAILURE);
	// Send register
	uart_send_cmd(CMD_WRITE);
	uart_send_byte(reg);
	ack = uart_read_ack();
	if(ack!=CST_ACK) exit(EXIT_FAILURE);
	// Restart
	uart_send_cmd(CMD_RESTART);
	// Send slave address, read mode
	uart_send_cmd(CMD_WRITE);
	uart_send_addr_read(addr);
	ack = uart_read_ack();
	if(ack!=CST_ACK) exit(EXIT_FAILURE);
	// Read data
	for(unsigned i=0; i<nb; i++) {
		uart_send_cmd(CMD_READ);
		buf[i] = uart_read_byte();
		if(i==nb-1) uart_send_cmd(CMD_NACK);
		else uart_send_cmd(CMD_ACK);
	}
	// End
	uart_send_cmd(CMD_STOP);
}

uint8_t slave_read_byte(uint8_t addr, uint8_t reg) {
	uint8_t byte;
	slave_read_reg(addr, reg, &byte, 1);
	return byte;
}

void slave_write_reg(uint8_t addr, uint8_t reg, uint8_t data) {
	unsigned ack;
	// Start
	uart_send_cmd(CMD_START);
	// Send slave address
	uart_send_cmd(CMD_WRITE);
	uart_send_addr_write(addr);
	ack = uart_read_ack();
	if(ack!=CST_ACK) {
		printf("Error: No ACK after sending write address %u (device unreachable)\n", addr);
		exit(EXIT_FAILURE);
	}
	// Send register
	uart_send_cmd(CMD_WRITE);
	uart_send_byte(reg);
	ack = uart_read_ack();
	if(ack!=CST_ACK) {
		printf("Error: No ACK after sending write register address\n");
		exit(EXIT_FAILURE);
	}
	// Send data
	uart_send_cmd(CMD_WRITE);
	uart_send_byte(data);
	uart_read_ack();
	if(ack!=CST_ACK) {
		printf("Error: No ACK after sending write data\n");
		exit(EXIT_FAILURE);
	}
	// End
	uart_send_cmd(CMD_STOP);
}

int32_t slave_read_id(uint8_t addr) {
	unsigned ack;
	int32_t id = 0;
	// Start
	uart_send_cmd(CMD_START);
	// Send reserved address for device ID
	uart_send_cmd(CMD_WRITE);
	uart_send_addr_write(0x7C);
	ack = uart_read_ack();
	if(ack!=CST_ACK) {
		//printf("Error line %u: 0x%02x\n", __LINE__, ack);
		uart_send_cmd(CMD_STOP);
		return -1;
	}
	// Send slave address
	uart_send_cmd(CMD_WRITE);
	uart_send_addr_write(addr);
	ack = uart_read_ack();
	if(ack!=CST_ACK) {
		//printf("Error line %u: 0x%02x\n", __LINE__, ack);
		uart_send_cmd(CMD_STOP);
		return -1;
	}
	// Restart
	uart_send_cmd(CMD_RESTART);
	// Send reserved address for device ID, read mode
	uart_send_cmd(CMD_WRITE);
	uart_send_addr_read(0x7C);
	ack = uart_read_ack();
	if(ack!=CST_ACK) {
		printf("Error line %u: 0x%02x\n", __LINE__, ack);
		uart_send_cmd(CMD_STOP);
		return -1;
	}
	// Read data
	uint8_t data;
	// Read first byte
	uart_send_cmd(CMD_READ);
	data = uart_read_byte();
	id = (id << 8) | data;
	uart_send_cmd(CMD_ACK);
	// Read second byte
	uart_send_cmd(CMD_READ);
	data = uart_read_byte();
	id = (id << 8) | data;
	uart_send_cmd(CMD_ACK);
	// Read third and last byte
	uart_send_cmd(CMD_READ);
	data = uart_read_byte();
	id = (id << 8) | data;
	uart_send_cmd(CMD_NACK);
	// End
	uart_send_cmd(CMD_STOP);
	return id;
}


//========================================================
// PMBus
//========================================================

#define PMBUS_CMD_PAGE         0x00  // To select the rail
#define PMBUS_CMD_VOUT_MODE    0x20  // Voltage mode and exponent
#define PMBUS_CMD_READ_VOUT    0x8B
#define PMBUS_CMD_READ_IOUT    0x8C
#define PMBUS_CMD_READ_TEMP1   0x8D
#define PMBUS_CMD_PMBUS_REV    0x98
#define PMBUS_CMD_MFR_ID       0x99
#define PMBUS_CMD_MFR_MODEL    0x9A
#define PMBUS_CMD_MFR_REV      0x9B

uint8_t pmbus_buf[16];

int pmbus_read_stuff(uint8_t addr, uint8_t cmd, unsigned size) {
	unsigned ack;
	pmbus_buf[0] = 0;
	// Start
	uart_send_cmd(CMD_START);
	// Send slave address
	uart_send_cmd(CMD_WRITE);
	uart_send_addr_write(addr);
	ack = uart_read_ack();
	if(ack!=CST_ACK) {
		printf("Error line %u: 0x%02x\n", __LINE__, ack);
		uart_send_cmd(CMD_STOP);
		return __LINE__;
	}
	// Send PMBus command
	uart_send_cmd(CMD_WRITE);
	uart_send_byte(cmd);
	ack = uart_read_ack();
	if(ack!=CST_ACK) {
		printf("Error line %u: 0x%02x\n", __LINE__, ack);
		uart_send_cmd(CMD_STOP);
		return __LINE__;
	}
	// Restart
	uart_send_cmd(CMD_RESTART);
	// Send slave address, read mode
	uart_send_cmd(CMD_WRITE);
	uart_send_addr_read(addr);
	ack = uart_read_ack();
	if(ack!=CST_ACK) {
		printf("Error line %u: 0x%02x\n", __LINE__, ack);
		uart_send_cmd(CMD_STOP);
		return __LINE__;
	}
	// Read data
	for(unsigned i=0; i<size; i++) {
		uart_send_cmd(CMD_READ);
		pmbus_buf[i] = uart_read_byte();
		if(i==size-1) uart_send_cmd(CMD_NACK);
		else uart_send_cmd(CMD_ACK);
	}
	pmbus_buf[size] = 0;
	// End
	uart_send_cmd(CMD_STOP);
	return 0;
}

int pmbus_read_mfr_id(uint8_t addr) {
	return pmbus_read_stuff(addr, PMBUS_CMD_MFR_ID, 8);
}
int pmbus_read_mfr_model(uint8_t addr) {
	return pmbus_read_stuff(addr, PMBUS_CMD_MFR_MODEL, 5);
}
int pmbus_read_mfr_rev(uint8_t addr) {
	return pmbus_read_stuff(addr, PMBUS_CMD_MFR_REV, 2);
}

int pmbus_read_pmbus_rev(uint8_t addr) {
	return pmbus_read_stuff(addr, PMBUS_CMD_PMBUS_REV, 1);
}

double lin11_to_double(int exp5, int man11) {
	// Sign extension of exponent
	exp5 = (exp5 << (32-5)) >> (32-5);
	// Sign extension of mantissa
	man11 = (man11 << (32-11)) >> (32-11);
	// Convert to double
	return man11 * pow(2.0, exp5);
}
double lin16_to_double(int exp5, int man16) {
	// Sign extension of exponent
	exp5 = (exp5 << (32-5)) >> (32-5);
	// Sign extension of mantissa
	man16 = (man16 << (32-16)) >> (32-16);
	// Convert to double
	return man16 * pow(2.0, exp5);
}

double pmbus_read_voltage(uint8_t addr, uint8_t rail) {
	// Select the rail
	slave_write_reg(addr, PMBUS_CMD_PAGE, rail);
	// Read the data
	uint8_t vout_mode = slave_read_byte(addr, PMBUS_CMD_VOUT_MODE);
	uint8_t read_vout[2];
	slave_read_reg(addr, PMBUS_CMD_READ_VOUT, read_vout, 2);
	// Convert to double
	double d = 0;
	if((vout_mode >> 5) == 0x00) {
		// Linear mode: 5-bit signed exponent, 16-bit signed mantissa
		int exp = 0;
		int man = 0;
		exp = vout_mode & 0x1F;
		man = read_vout[1];
		man = (man << 8) | read_vout[0];
		d = lin16_to_double(exp, man);
	}
	else {
		// FIXME Unhandled format => should handle Direct mode
		d = -1;
	}
	return d;
}

double pmbus_read_current(uint8_t addr, uint8_t rail) {
	// Select the rail
	slave_write_reg(addr, PMBUS_CMD_PAGE, rail);
	// Read the data
	uint8_t read_data[2];
	slave_read_reg(addr, PMBUS_CMD_READ_IOUT, read_data, 2);
	// Linear mode: 5-bit signed exponent, 11-bit signed mantissa
	int exp = 0;
	int man = 0;
	exp = read_data[1] >> 3;
	man = read_data[1];
	man = (man << 8) | read_data[0];
	return lin11_to_double(exp, man);
}

typedef struct rail_t {
	char*    name;
	uint8_t  addr;
	uint8_t  rail;
	unsigned measure_nb;
	double   cur_volt;
	double   cur_curr;
	double   cur_pow;
	double   min_volt;
	double   min_curr;
	double   min_pow;
	double   max_volt;
	double   max_curr;
	double   max_pow;
	double   acc_volt;
	double   acc_curr;
	double   acc_pow;
} rail_t;


//========================================================
// Do polling on PMBus
//========================================================

void print_line() {
	printf("+--------------+-------------------------");
	printf("+-----------------+-----------------+-----------------+");
	printf("\n");
}
void print_rail(rail_t* rail) {
	printf("| %-12s | %7.3f %7.3f %7.3f ", rail->name, rail->cur_volt, rail->cur_curr, rail->cur_pow);
	printf("| %7.3f %7.3f | %7.3f %7.3f | %7.3f %7.3f ", rail->min_volt, rail->max_volt, rail->min_curr, rail->max_curr, rail->min_pow, rail->max_pow);
	printf("|\n");
}

char* filename_vcd = NULL;
FILE* Fvcd = NULL;

// Warning : no spaces in the signal names
void VCD_WriteInit(rail_t* rails, unsigned rails_nb) {
	if(Fvcd==NULL) return;
	fprintf(Fvcd, "$date %d $end\n", (int)time(NULL));
	fprintf(Fvcd, "$version %s version %s $end\n", "uart2pmbus", "1.0");
	fprintf(Fvcd, "$timescale 1ms $end\n");
	fprintf(Fvcd, "$scope PMBus $end\n");
	for(unsigned i=0; i<rails_nb; i++) {
		rail_t* rail = rails + i;
		fprintf(Fvcd, "$var real 32 &%u %s $end\n", i, rail->name);
	}
	fprintf(Fvcd, "$var real 32 &%u total $end\n", rails_nb);
	fprintf(Fvcd, "$upscope $end\n");
	fprintf(Fvcd, "$enddefinitions $end\n");
	fprintf(Fvcd, "$dumpvars\n");
}

void VCD_WriteValues(rail_t* rails, unsigned rails_nb, double t) {
	if(Fvcd==NULL) return;
	fprintf(Fvcd, "#%u\n", (unsigned)(t*1000));
	double total = 0;
	for(unsigned i=0; i<rails_nb; i++) {
		rail_t* rail = rails + i;
		fprintf(Fvcd, "r%f &%u\n", rail->cur_pow, i);
		total += rail->cur_pow;
	}
	fprintf(Fvcd, "r%f &%u\n", total, rails_nb);
}

#define VC709_RAILS_NB 11
rail_t vc709_rails[VC709_RAILS_NB] = {
	// PMBus Controller at Address 52: Core voltage controller and regulators
	{ "vccint_fpga", 52, 1 },
	{ "vccaux"     , 52, 2 },
	{ "vcc3v3"     , 52, 3 },
	// PMBus Controller at Address 53: Auxiliary voltage controllers and regulators
	{ "vcc2v5_fpga", 53, 1 },
	{ "vcc1v5"     , 53, 2 },
	{ "mgtavcc"    , 53, 3 },
	{ "mgtavtt"    , 53, 4 },
	// PMBus Controller at Address 53: Auxiliary voltage controllers and regulators
	{ "vccaux_io"  , 54, 1 },
	{ "unused"     , 54, 2 },
	{ "mgtvccaux"  , 54, 3 },
	{ "vcc1v8_fpga", 54, 4 },
};

#define ZC706_RAILS_NB 5
rail_t zc706_rails[ZC706_RAILS_NB] = {
	{ "vccint"     , 101, 1 },
	{ "vccaux"     , 101, 2 },
	{ "vcc1v5_pl"  , 101, 3 },
	{ "vadj_fpga"  , 101, 4 },
	{ "vcc3v3_fpga", 101, 5 },
};

#define ZCU102_RAILS_NB 11
rail_t zcu102_rails[ZCU102_RAILS_NB] = {
	// PL rails
	{ "vccint"     , 19, 1 },
	{ "vccbram"    , 20, 1 },
	{ "vccaux"     , 21, 1 },
	{ "vcc1v2"     , 22, 1 },
	{ "vcc3v3"     , 23, 1 },
	{ "vadj_fmc"   , 24, 1 },
	{ "mgtavcc"    , 114, 1 },
	{ "mgtavtt"    , 115, 1 },
	// PS rails
	{ "vccpsintfp" , 10, 1 },
	{ "vccpsintlp" , 11, 1 },
	{ "vccops"     , 16, 1 },
};

/* Notes about ANSII escape codes
 * https://en.wikipedia.org/wiki/ANSI_escape_code
 *
 * \033[H    Move cursor to top left of screen
 * \033[2J   Clear screen
 */

#define SetMin(r, v) do { if(v < r) r = v; } while(0)
#define SetMax(r, v) do { if(v > r) r = v; } while(0)

void rails_poll(rail_t* rails, unsigned rails_nb) {
	double min_curr = 0;
	double min_pow = 0;
	double max_curr = 0;
	double max_pow = 0;

	int64_t delay = 0;
	if(log_persec > 0) delay = 1000 * 1000 * 1000 / log_persec;

	int64_t time_beg = 0;
	int64_t time_cur = 0;
	int64_t time_want = 0;

	VCD_WriteInit(rails, rails_nb);
	VCD_WriteValues(rails, rails_nb, 0);

	// Get Ctrl-C
	signal(SIGINT, intHandler);

	// Initialize time
	time_cur = Time64_GetReal();
	time_beg = time_cur;
	time_want = time_cur;

	for(unsigned m=0; sig_do_stop==0; m++) {

		// Read all rails
		for(unsigned r=0; r<rails_nb; r++) {
			rail_t* rail = rails + r;

			rail->cur_volt = pmbus_read_voltage(rail->addr, rail->rail-1);
			rail->cur_curr = pmbus_read_current(rail->addr, rail->rail-1);
			rail->cur_pow  = rail->cur_volt * rail->cur_curr;

			rail->acc_volt += rail->cur_volt;
			rail->acc_curr += rail->cur_curr;
			rail->acc_pow  += rail->cur_pow;

			if(rail->measure_nb == 0) {
				rail->min_volt = rail->cur_volt;
				rail->min_curr = rail->cur_curr;
				rail->min_pow  = rail->cur_pow;
				rail->max_volt = rail->cur_volt;
				rail->max_curr = rail->cur_curr;
				rail->max_pow  = rail->cur_pow;
			}
			else {
				SetMin(rail->min_volt , rail->cur_volt);
				SetMin(rail->min_curr , rail->cur_curr);
				SetMin(rail->min_pow  , rail->cur_pow );
				SetMax(rail->max_volt , rail->cur_volt);
				SetMax(rail->max_curr , rail->cur_curr);
				SetMax(rail->max_pow  , rail->cur_pow );
			}

			rail->measure_nb ++;
		}  // Read all rails

		if(do_disp==true) {

			if(m==0) {
				// Clear screen
				printf("\033[2J");
			}

			// Move the cursor top left
			printf("\033[H");

			print_line();
			printf("| Rail         |   Volts    Amps   Watts ");
			printf("|  Volts, min-max |   Amps, min-max |  Watts, min-max |");
			printf("\n");
			print_line();

			double total_curr = 0;
			double total_pow = 0;
			for(unsigned r=0; r<rails_nb; r++) {
				rail_t* rail = rails + r;
				if(rails==vc709_rails && (r==3 || r==7)) print_line();  // FIXME add flags in rail structure?
				print_rail(rail);
				total_curr += rail->cur_curr;
				total_pow += rail->cur_pow;
			}

			print_line();

			if(m==0) {
				min_curr = total_curr;
				min_pow  = total_pow;
				max_curr = total_curr;
				max_pow  = total_pow;
			}
			else {
				SetMin(min_curr, total_curr);
				SetMin(min_pow, total_pow);
				SetMax(max_curr, total_curr);
				SetMax(max_pow, total_pow);
			}

			printf("| Total        |         %7.3f %7.3f ", total_curr, total_pow);
			printf("|                 | %7.3f %7.3f | %7.3f %7.3f ", min_curr, max_curr, min_pow, max_pow);
			printf("|\n");
			print_line();

		}  // Pretty-print on stdout

		if(file_log!=NULL) {
			fprintf(file_log, "%f", TimeDouble_From64(time_cur-time_beg));

			double total_pow = 0;
			for(unsigned r=0; r<rails_nb; r++) {
				rail_t* rail = rails + r;
				total_pow += rail->cur_pow;
				fprintf(file_log, ", %f", rail->cur_pow);
			}

			fprintf(file_log, ", %f\n", total_pow);
			fflush(file_log);
		}  // Log data

		VCD_WriteValues(rails, rails_nb, TimeDouble_From64(time_cur-time_beg) + 0.001);

		// Wait time, if applicable
		time_want += delay;
		time_cur = Time64_GetReal();
		if(time_cur < time_want) {
			usleep((time_want - time_cur) / 1000);
			time_cur = time_want;
		}

	}  // Infinite loop
}


//========================================================
// Main function
//========================================================

static void print_usage(int argc, char** argv) {
	printf(
		"Usage: %s [options]\n", argv[0]
	);
	printf(
		"Options:\n"
		"  -u, -h, --help    Display this help and exit\n"
		"  --about           Display Copyright notice and disclaimer, and exit\n"
		"  -p <path>         The port to open [/dev/ttyS0]\n"
		"  -b <value>        Baudrate [115200]\n"
		"  -log <file>       Log all data to the specified file ('-' for stdout)\n"
		"  -vcd <file>       Log all data in VCD format ('-' for stdout)\n"
		"  -logf <nb>        Log all data data <nb> times per second (default 1)\n"
		"  -rail <addr> <id> Measure power rail <id> (starting from 1) at address <addr>\n"
		"  -vc709            Measure power rails of board VC709\n"
		"  -zc706            Measure power rails of board ZC706\n"
		"  -zcu102           Measure power rails of board ZCU102\n"
		"  -stop             Send STOP in I2C bus to somewhat force reset\n"
		"  -read             Continuously read the UART to flush FIFOs\n"
		"  -nodisp           Don't pretty-print on standard output\n"
		"  -debug            Prints lots of messages on standard output\n"
	);
}

static void print_about(int argc, char** argv) {
	printf(
		"\n"
		"Uart2PMBus: Application for reading PMBus through UART\n"
		"Copyright TIMA Laboratory (Grenoble, France)\n"
		"Contact: Adrien Prost-Boucle <adrien.prost-boucle@imag.fr>\n"
		"Permanent contact: Frédéric Pétrot <frderic.petrot@imag.fr>\n"
		"\n"
		"Disclaimer:\n"
		"This program is distributed in the hope that it will be useful.\n"
		"Despite a lot of testing, this program may not be exempt of bugs.\n"
		"In case unexpected behaviour happens, your data may be lost.\n"
		"\n"
	);
}

int main(int argc, char** argv) {
	int z;
  struct termios termios_p;
	char* filename_serial = "/dev/ttyS0";
	speed_t baudrate = B115200;
	bool parity = false;
	int one_rail_addr = -1;
	int one_rail = -1;

	if(argc==1) {
		print_about(argc, argv);
		exit(EXIT_SUCCESS);
	}

	// Parse the command-line parameters
	unsigned argi = 1;
	do {
		if(argi >= argc) break;
		char* arg = argv[argi];
		if(arg[0]==0) break;

		// Local utility functions

		// Get parameters
		char* getparam_str() {
			argi++;
			if(argi >= argc) {
				printf("Error: Missing parameters after '%s'\n", arg);
				exit(EXIT_FAILURE);
			}
			return argv[argi];
		}

		// Parse parameters

		if(strcmp(arg, "-h")==0 || strcmp(arg, "-u")==0 || strcmp(arg, "--help")==0) {
			print_usage(argc, argv);
			exit(EXIT_SUCCESS);
		}
		if(strcmp(arg, "--about")==0) {
			print_about(argc, argv);
			exit(EXIT_SUCCESS);
		}

		if(strcmp(arg, "-p")==0) {
			filename_serial = getparam_str();
		}
		else if(strcmp(argv[argi], "-b")==0) {
			char* param = getparam_str();

			#define Q(x) #x
			#define GEN_TESTDEF(x) else if(strcmp(param, Q(x))==0) baudrate = B##x;

			if     (strcmp(param, "50")==0) baudrate = B50;
			else if(strcmp(param, "75")==0) baudrate = B75;
			else if(strcmp(param, "110")==0) baudrate = B110;
			else if(strcmp(param, "134")==0) baudrate = B134;
			else if(strcmp(param, "150")==0) baudrate = B150;
			else if(strcmp(param, "200")==0) baudrate = B200;
			else if(strcmp(param, "300")==0) baudrate = B300;
			else if(strcmp(param, "600")==0) baudrate = B600;
			else if(strcmp(param, "1200")==0) baudrate = B1200;
			else if(strcmp(param, "1800")==0) baudrate = B1800;
			else if(strcmp(param, "2400")==0) baudrate = B2400;
			else if(strcmp(param, "4800")==0) baudrate = B4800;
			else if(strcmp(param, "9600")==0) baudrate = B9600;

			GEN_TESTDEF(19200)
			GEN_TESTDEF(38400)
			GEN_TESTDEF(57600)
			GEN_TESTDEF(115200)
			GEN_TESTDEF(230400)
			GEN_TESTDEF(460800)
			GEN_TESTDEF(500000)
			GEN_TESTDEF(576000)
			GEN_TESTDEF(921600)
			GEN_TESTDEF(1000000)
			GEN_TESTDEF(1152000)
			GEN_TESTDEF(1500000)
			GEN_TESTDEF(2000000)
			GEN_TESTDEF(2500000)
			GEN_TESTDEF(3000000)
			GEN_TESTDEF(3500000)
			GEN_TESTDEF(4000000)

			else {
				printf("Error: Invalid argument for '%s'.\n", param);
				exit(EXIT_FAILURE);
			}
		}
		else if(strcmp(arg, "-par")==0) {
			parity = true;
		}

		else if(strcmp(arg, "-log")==0) {
			filename_log = getparam_str();
		}
		else if(strcmp(arg, "-logf")==0) {
			log_persec = atoi(getparam_str());
		}
		else if(strcmp(arg, "-vcd")==0) {
			filename_vcd = getparam_str();
		}

		else if(strcmp(arg, "-rail")==0) {
			one_rail_addr = atoi(getparam_str());
			one_rail      = atoi(getparam_str());
		}

		else if(strcmp(arg, "-vc709")==0) {
			do_vc709 = true;
		}
		else if(strcmp(arg, "-zc706")==0) {
			do_zc706 = true;
		}
		else if(strcmp(arg, "-zcu102")==0) {
			do_zcu102 = true;
		}

		else if(strcmp(arg, "-scan")==0) {
			do_scan = true;
		}
		else if(strcmp(arg, "-read")==0) {
			do_read = true;
		}
		else if(strcmp(arg, "-stop")==0) {
			do_stop = true;
		}
		else if(strcmp(arg, "-nodisp")==0) {
			do_disp = false;
		}
		else if(strcmp(arg, "-debug")==0) {
			debug = true;
		}

		else {
			printf("Error: Unknown parameter '%s'.\n", arg);
			exit(EXIT_FAILURE);
		}

		// Here the parameter was known. Increment index.
		argi++;

	} while(1);  // Parse all command-line parameters

	// Open in/out files

	if(filename_log!=NULL) {
		if(strcmp(filename_log, "-")==0) {
			file_log = stdout;
		}
		else {
			file_log = fopen(filename_log, "wb");
			if(file_log==NULL) {
				printf("Error: Can't open file '%s'.\n", filename_log);
				exit(EXIT_FAILURE);
			}
		}
	}

	if(filename_vcd!=NULL) {
		if(strcmp(filename_vcd, "-")==0) {
			Fvcd = stdout;
		}
		else {
			Fvcd = fopen(filename_vcd, "wb");
			if(Fvcd==NULL) {
				printf("Error: Can't open file '%s'.\n", filename_vcd);
				exit(EXIT_FAILURE);
			}
		}
	}

	// Open the port
	TTYS = open(filename_serial, O_RDWR | O_NOCTTY);
	if(TTYS<0) {
		printf("Error: Can't open serial port '%s'.\n", filename_serial);
		exit(EXIT_FAILURE);
	}

	// Read current parameters
	tcgetattr(TTYS, &termios_p);
	cfsetspeed(&termios_p, baudrate);

	// Ignore BREAK and characters with parity errors
	termios_p.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
	termios_p.c_cflag &= ~(CSIZE | CSTOPB);
	termios_p.c_cflag |= CS8;
	termios_p.c_oflag = 0;
	// Set parity
	if(parity==false) {
		termios_p.c_cflag &= ~PARENB;
	}
	else {
		termios_p.c_cflag |= PARENB;
	}

	// Canonical raw input
	termios_p.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG | ECHONL | IEXTEN);
	termios_p.c_oflag &= ~OPOST;
	termios_p.c_cflag |= (CS8);  // use 8bit data

	// Read with timeout, 1 second
	termios_p.c_cc[VMIN] = 0;
	termios_p.c_cc[VTIME] = 10;

	// Set the new options for the port
	z = tcsetattr(TTYS, TCSAFLUSH, &termios_p);
	if(z!=0) {
		printf("ERROR : port configuration failed (return = %d)\n", z);
		return 1;
	}

	// Here the UART is configured
	// Begin doing something useful

	if(do_vc709==true) {
		rails_poll(vc709_rails, VC709_RAILS_NB);
		exit(EXIT_SUCCESS);
	}
	if(do_zc706==true) {
		rails_poll(zc706_rails, ZC706_RAILS_NB);
		exit(EXIT_SUCCESS);
	}
	if(do_zcu102==true) {
		rails_poll(zcu102_rails, ZCU102_RAILS_NB);
		exit(EXIT_SUCCESS);
	}

	if(one_rail_addr >= 0 && one_rail >= 0) {
		rail_t rails_local = { "rail"     , one_rail_addr, one_rail };
		rails_poll(&rails_local, 1);
		exit(EXIT_SUCCESS);
	}

	if(do_scan==true) {
		for(unsigned addr=0; addr<128; addr++) {
			// Skip forbidden I2C slave addresses: 0000xxx and 1111xxx
			if((addr >> 3) == 0x00) continue;
			if((addr >> 3) == 0x0F) continue;
			// Send address and listen for ACK
			uart_send_cmd(CMD_START);
			uart_send_cmd(CMD_WRITE);
			uart_send_addr_write(addr);
			unsigned ack = uart_read_ack();
			uart_send_cmd(CMD_STOP);
			// Print message
			if(ack==CST_ERROR) {
				printf("Error received for address %u\n", addr);
				exit(EXIT_FAILURE);
			}
			if(ack==CST_ACK) {
				printf("Address %u\n", addr);

				#if 0
				uint32_t id = slave_read_id(addr);
				printf("  id 0x%08x\n", id);
				#endif

				#if 1
				int z;
				z = pmbus_read_pmbus_rev(addr);
				if(z!=0) pmbus_buf[0] = 0;
				printf("  pmbus rev 0x%02x\n", pmbus_buf[0]);
				z = pmbus_read_mfr_id(addr);
				if(z!=0) pmbus_buf[0] = 0;
				printf("  mfr id    '%s'\n", (char*)pmbus_buf);
				z = pmbus_read_mfr_model(addr);
				if(z!=0) pmbus_buf[0] = 0;
				printf("  mfr model '%s'\n", (char*)pmbus_buf);
				pmbus_read_mfr_rev(addr);
				printf("  mfr rev   0x%02x 0x%02x\n", pmbus_buf[0], pmbus_buf[1]);
				#endif

			}
		}
		exit(EXIT_SUCCESS);
	}

	if(do_read==true) {
		do {
			uart_read_byte();
		} while(1);
		exit(EXIT_SUCCESS);
	}

	if(do_stop==true) {
		uart_send_cmd(CMD_STOP);
		exit(EXIT_SUCCESS);
	}

	// In case there is something else in the pipe, read all
	printf("Nothing specified, exiting\n");

	// Close UART channel
	close(TTYS);

	return 0;
}

