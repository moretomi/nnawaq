#!/usr/bin/env python
#coding=utf-8
"""
plotting power consumption from logfile
"""
#for command line arguments
import sys
import os
#plotting
import numpy as np
import matplotlib.pyplot as plt

#argument: csv argfile
if len(sys.argv) != 2:
	print('E: '+sys.argv[0]+' needs exactly one argument (csv argfile)')
	sys.exit()

#column extraction
argfile=sys.argv[1]
print('plotting data from '+argfile)
#l'extraction des données
data = np.genfromtxt(argfile, skip_header=1, invalid_raise=False, usecols=(0,1,12), dtype=('f4,f4,f4'), comments="#", delimiter=",")

#colors
colo1='#008000'

#import pdb; pdb.set_trace()

#creating figure
#fig, ax = plt.subplots() # manière d'écrire ce qu'ilya en dessous mais figsize marche pas ...
fig = plt.figure(figsize=(10,3))
ax = fig.add_subplot(1,1,1)

#small config
plot = ax.plot(
	data['f0'], data['f1'],
	alpha=0.9,
	linewidth=1,
	#color=colo1,
	#antialiased=True,
	#solid_capstyle='round',
	#solid_joinstyle='round',
	label='vccint'
)

plot = ax.plot(
	data['f0'], data['f2'],
	alpha=0.9,
	linewidth=1,
	#color=colo1,
	#antialiased=True,
	#solid_capstyle='round',
	#solid_joinstyle='round',
	label='Total'
)

plt.xlabel('Time (s)')
plt.ylabel('Power (W)')
#plt.title('Post-synthesis frequency (ISE 14.7)')
#plt.legend(loc=0,prop={'size':9})

#log x
#ax.set_xscale('log')
#ax.set_xlim([min(data['f0'])-10,max(data['f0'])+10])
ax.set_ylim([0,max(data['f2'] + 1)])

# Hide the right and top spines
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
# Only show ticks on the left and bottom spines
ax.yaxis.set_ticks_position('left')
ax.xaxis.set_ticks_position('bottom')
#enabling grid
ax.grid(True,linestyle='-.')
# disable x grid
ax.xaxis.grid(False)
ax.set_axisbelow(True)

plt.tight_layout()

scriptname=os.path.splitext(os.path.basename(sys.argv[0]))[0]
print("Output argfile: "+argfile+"."+scriptname+'.svg')
fig.savefig(argfile+"."+scriptname+'.svg')
#plt.show()
