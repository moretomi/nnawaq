
# Programs and parameters
CC      = gcc
CFLAGS  = -Wall -std=gnu99

LD      = gcc
LDFLAGS =

ifdef DEBUG
	CFLAGS  += -g -O0
	LDFLAGS +=
else
	CFLAGS  += -march=native -Ofast -DNDEBUG
	LDFLAGS += -s
endif

RM      = rm
RMFLAGS = -f

PROG = uart2pmbus

# Compilation tasks

.PHONY: all clean

all: $(PROG)

$(PROG): $(PROG).c
	$(CC) $(CFLAGS) $(LDFLAGS) -lm -lrt -o $@ $<


# Launch for general information

.PHONY: \
	about help \
	vc709 vc709-core \
	zc706 zc706-core \
	zcu102 zcu102-core \
	plot plot-view gtkwave follow

about: $(PROG)
	./$(PROG) --about

help: $(PROG)
	./$(PROG) --help

# Launch for board VC709

vc709: $(PROG)
	./$(PROG) -b 115200 -p /dev/ttyUSB0 -vc709 -log vc709.log -vcd vc709.vcd -logf 5

vc709-core: $(PROG)
	./$(PROG) -b 115200 -p /dev/ttyUSB0 -rail 52 1 -log vc709.log -vcd vc709.vcd -logf 20

# Warning: Needed packages: python-matplotlib python-numpy
plot:
	plot/draw_power.py vc709.log

plot-view:
	inkview vc709.log.draw_power.svg &

gtkwave:
	gtkwave vc709.vcd &

follow: $(PROG)
	./$(PROG) -b 115200 -p /dev/ttyUSB0 -vc709 -nodisp -log vc709.log -vcd - -logf 5 | shmidcat | gtkwave -v -I vc709.gtkw &

# Launch for other boards
# Warning: their PMBus bus is normally accessibleonly from PMBus connector or on-board processor subsystem
# To use these recipes, a hack is needed, such as be connecting PMBus buses with another board VC709
# This works because PMBus addresses don't conflict between boards

zc706: $(PROG)
	./$(PROG) -b 115200 -p /dev/ttyUSB0 -zc706 -log zc706.log -vcd zc706.vcd -logf 5

zc706-core: $(PROG)
	./$(PROG) -b 115200 -p /dev/ttyUSB0 -rail 101 1 -log zc706.log -vcd zc706.vcd -logf 5

zcu102: $(PROG)
	./$(PROG) -b 115200 -p /dev/ttyUSB0 -zcu102 -log zcu102.log -vcd zcu102.vcd -logf 5

zcu102-core: $(PROG)
	./$(PROG) -b 115200 -p /dev/ttyUSB0 -rail 19 1 -log zcu102.log -vcd zcu102.vcd -logf 5

# Delete object files
clean:
	$(RM) $(RMFLAGS) *.o $(PROG)
