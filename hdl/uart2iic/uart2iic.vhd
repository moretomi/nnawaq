
-- UART-to-I2C bridge component
-- The UART implements a special protocol
-- Only for one-master I2C designs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart2iic is
	generic (
		FREQ_IN   : natural := 100000000;
		FREQ_I2C  : natural := 100000;
		BAUD_UART : natural := 115200
	);
	port (
		reset : in  std_logic;
		clk   : in  std_logic;
		-- The UART ports
		rx    : in  std_logic;
		tx    : out std_logic;
		-- The I2C ports
		scl   : inout std_logic;
		sda   : inout std_logic
	);
end uart2iic;

architecture augh of uart2iic is

	signal uart2fifo_data : std_logic_vector(7 downto 0);
	signal uart2fifo_rdy  : std_logic;
	signal uart2fifo_ack  : std_logic;

	signal fifo2iic_data  : std_logic_vector(7 downto 0);
	signal fifo2iic_rdy   : std_logic;
	signal fifo2iic_ack   : std_logic;

	signal iic2fifo_data  : std_logic_vector(7 downto 0);
	signal iic2fifo_rdy   : std_logic;
	signal iic2fifo_ack   : std_logic;

	signal fifo2uart_data : std_logic_vector(7 downto 0);
	signal fifo2uart_rdy  : std_logic;
	signal fifo2uart_ack  : std_logic;

	component uart_rx is
		generic (
			FREQ_IN : natural := 100000000;
			BAUD    : natural := 115200
		);
		port (
			reset : in  std_logic;
			clk   : in  std_logic;
			-- The FIFO
			fifo_ack  : in  std_logic;
			fifo_rdy  : out std_logic;
			fifo_data : out std_logic_vector(7 downto 0);
			-- The RX port
			rx : in  std_logic
		);
	end component;

	component uart_tx is
		generic (
			FREQ_IN : natural := 100000000;
			BAUD    : natural := 115200
		);
		port (
			reset : in  std_logic;
			clk   : in  std_logic;
			-- The FIFO
			fifo_ack  : in  std_logic;
			fifo_rdy  : out std_logic;
			fifo_data : in  std_logic_vector(7 downto 0);
			-- The TX port
			tx : out std_logic
		);
	end component;

	component i2c_master is
		generic (
			FREQ_IN  : natural := 100000000;
			FREQ_OUT : natural := 100000
		);
		port (
			reset         : in  std_logic;
			clk           : in  std_logic;
			-- The input FIFO
			fifo_in_ack   : in  std_logic;
			fifo_in_rdy   : out std_logic;
			fifo_in_data  : in  std_logic_vector(7 downto 0);
			-- The output FIFO
			fifo_out_ack  : in  std_logic;
			fifo_out_rdy  : out std_logic;
			fifo_out_data : out std_logic_vector(7 downto 0);
			-- The I2C ports
			scl           : inout std_logic;
			sda           : inout std_logic
		);
	end component;

	component fifo_with_counters is
		generic (
			DATAW : natural := 32;
			DEPTH : natural := 8;
			CNTW  : natural := 16
		);
		port (
			reset         : in  std_logic;
			clk           : in  std_logic;
			fifo_in_data  : in  std_logic_vector(DATAW-1 downto 0);
			fifo_in_rdy   : out std_logic;
			fifo_in_ack   : in  std_logic;
			fifo_in_cnt   : out std_logic_vector(CNTW-1 downto 0);
			fifo_out_data : out std_logic_vector(DATAW-1 downto 0);
			fifo_out_rdy  : out std_logic;
			fifo_out_ack  : in  std_logic;
			fifo_out_cnt  : out std_logic_vector(CNTW-1 downto 0)
		);
	end component;

begin

	uart_rx_i : uart_rx
		generic map (
			FREQ_IN => FREQ_IN,
			BAUD    => BAUD_UART
		)
		port map (
			reset     => reset,
			clk       => clk,
			-- The FIFO
			fifo_ack  => uart2fifo_ack,
			fifo_rdy  => uart2fifo_rdy,
			fifo_data => uart2fifo_data,
			-- The RX port
			rx        => rx
		);

	fifo_uart2iic : fifo_with_counters
		generic map (
			DATAW => 8,
			DEPTH => 8,
			CNTW  => 8
		)
		port map (
			reset         => reset,
			clk           => clk,
			fifo_in_data  => uart2fifo_data,
			fifo_in_rdy   => uart2fifo_ack,
			fifo_in_ack   => uart2fifo_rdy,
			fifo_in_cnt   => open,
			fifo_out_data => fifo2iic_data,
			fifo_out_rdy  => fifo2iic_rdy,
			fifo_out_ack  => fifo2iic_ack,
			fifo_out_cnt  => open
		);

	i2c_i : i2c_master
		generic map (
			FREQ_IN  => FREQ_IN,
			FREQ_OUT => FREQ_I2C
		)
		port map (
			reset         => reset,
			clk           => clk,
			-- The input FIFO
			fifo_in_ack   => fifo2iic_rdy,
			fifo_in_rdy   => fifo2iic_ack,
			fifo_in_data  => fifo2iic_data,
			-- The output FIFO
			fifo_out_ack  => iic2fifo_ack,
			fifo_out_rdy  => iic2fifo_rdy,
			fifo_out_data => iic2fifo_data,
			-- The I2C ports
			scl           => scl,
			sda           => sda
		);

	fifo_iic2uart : fifo_with_counters
		generic map (
			DATAW => 8,
			DEPTH => 8,
			CNTW  => 8
		)
		port map (
			reset         => reset,
			clk           => clk,
			fifo_in_data  => iic2fifo_data,
			fifo_in_rdy   => iic2fifo_ack,
			fifo_in_ack   => iic2fifo_rdy,
			fifo_in_cnt   => open,
			fifo_out_data => fifo2uart_data,
			fifo_out_rdy  => fifo2uart_rdy,
			fifo_out_ack  => fifo2uart_ack,
			fifo_out_cnt  => open
		);

	uart_tx_i : uart_tx
		generic map (
			FREQ_IN => FREQ_IN,
			BAUD    => BAUD_UART
		)
		port map (
			reset     => reset,
			clk       => clk,
			-- The FIFO
			fifo_ack  => fifo2uart_rdy,
			fifo_rdy  => fifo2uart_ack,
			fifo_data => fifo2uart_data,
			-- The TX port
			tx        => tx
		);

end architecture;

