
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity uart_rx is
	generic (
		FREQ_IN : natural := 100000000;
		BAUD    : natural := 115200
	);
	port (
		reset : in  std_logic;
		clk   : in  std_logic;
		-- The FIFO
		fifo_ack  : in  std_logic;
		fifo_rdy  : out std_logic;
		fifo_data : out std_logic_vector(7 downto 0);
		-- The RX port
		rx : in  std_logic
	);
end uart_rx;

architecture augh of uart_rx is

	-- Note: Data bits are received as little endian

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	constant data_bits      : natural := 8;
	constant frames_nb      : natural := 1;
	constant clock_divider  : natural := FREQ_IN / BAUD;
	-- Bit width of a counter that counts from 0 to clock_divider-1
	constant clockdiv_width : natural := storebitsnb(clock_divider-1);
	-- Bit width of a counter that counts from 0 to data_bits-1
	constant bitcount_width : natural := 3;
	-- Bit width of a counter that counts from 0 to frames_nb
	constant framecnt_width : natural := 1;

	-- Types for little FSM
	type status_type is (fsm_start, fsm_bits, fsm_stop);

	signal rx_status          : status_type := fsm_start;
	signal rx_status_next     : status_type := fsm_start;
	signal rx_bits_idx        : unsigned(bitcount_width-1 downto 0) := (others => '0');
	signal rx_bits_idx_next   : unsigned(bitcount_width-1 downto 0) := (others => '0');
	signal rx_clkdiv_cnt      : unsigned(clockdiv_width-1 downto 0) := (others => '0');
	signal rx_clkdiv_cnt_next : unsigned(clockdiv_width-1 downto 0) := (others => '0');
	-- An intermediate signal to cover cases where the fifo width is not a multiple of data_bits
	signal rx_fifo_tmp_data   : std_logic_vector(frames_nb*data_bits-1 downto 0);
	-- Frames of data bits. When there are several frames to receive, the number 0 is the one being filled from Rx.
	signal rx_bits0           : std_logic_vector(data_bits-1 downto 0);
	signal rx_bits0_next      : std_logic_vector(data_bits-1 downto 0);
	-- Number of frames received. Reset to 0 on FIFO transfer.
	-- It is incremented each time a frame is received.
	signal rx_frames          : unsigned(framecnt_width-1 downto 0) := (others => '0');
	signal rx_frames_next     : unsigned(framecnt_width-1 downto 0) := (others => '0');

begin

	-- Sequential process
	process (clk, reset)
	begin
		if rising_edge(clk) then

			rx_status        <= rx_status_next;
			rx_bits_idx      <= rx_bits_idx_next;
			rx_frames        <= rx_frames_next;
			rx_clkdiv_cnt    <= rx_clkdiv_cnt_next;
			rx_bits0         <= rx_bits0_next;

			if reset = '1' then
				rx_status        <= fsm_start;
				rx_bits_idx      <= (others => '0');
				rx_frames        <= (others => '0');
				rx_clkdiv_cnt    <= (others => '0');
			end if;

		end if;
	end process;

	-- Combinatorial process
	process (rx_status, rx_bits_idx, rx_frames, rx_clkdiv_cnt, rx_bits0, fifo_ack, rx)
	begin
		-- Default values for ports
		fifo_rdy <= '0';
		-- Default values for internal signals
		rx_status_next     <= rx_status;
		rx_bits_idx_next   <= rx_bits_idx;
		rx_frames_next     <= rx_frames;
		rx_bits0_next      <= rx_bits0;

		-- Clock divider
		if rx_clkdiv_cnt = clock_divider-1 then
			rx_clkdiv_cnt_next <= (others => '0');
		else
			rx_clkdiv_cnt_next <= rx_clkdiv_cnt + 1;
		end if;

		-- Handle data output
		if rx_frames = frames_nb then
			fifo_rdy <= '1';
			if fifo_ack = '1' then
				rx_frames_next <= (others => '0');
			end if;
		end if;

		-- Compute next state and read Rx
		if rx_status = fsm_start then
			rx_bits_idx_next <= (others => '0');
			if rx = '1' and rx_clkdiv_cnt < clock_divider/2 then
				-- We are in a stop bit
				rx_clkdiv_cnt_next <= (others => '0');
			else
				-- We are in the start bit
				if rx_clkdiv_cnt = clock_divider-1 then
					rx_status_next <= fsm_bits;
					-- Drop the previously received data... FIXME Overrun is not handled
					if rx_frames = frames_nb then
						rx_frames_next <= (others => '0');
					end if;
				end if;
			end if;
		elsif rx_status = fsm_bits then
			-- Receive the data bits
			-- Read the data bit at the middle of the bit duration
			if rx_clkdiv_cnt = clock_divider/2 then
				rx_bits0_next <= rx & rx_bits0(data_bits-1 downto 1);
			end if;
			-- Increment the bit counter and change state
			if rx_clkdiv_cnt = clock_divider-1 then
				if rx_bits_idx = data_bits-1 then
					rx_frames_next <= rx_frames + 1;
					rx_status_next <= fsm_stop;
				else
					rx_bits_idx_next <= rx_bits_idx + 1;
				end if;
			end if;
		elsif rx_status = fsm_stop then
			-- Read the stop bit (discarded) for just half a symbol to let some slack
			if rx_clkdiv_cnt = clock_divider/2 then
				-- Go waiting for next falling edge
				rx_clkdiv_cnt_next <= (others => '0');
				rx_status_next <= fsm_start;
			end if;
		end if;

	end process;

	-- The intermediate signal for FIFO data output
	rx_fifo_tmp_data <= rx_bits0;

	-- The FIFO data output
	fifo_data <= rx_fifo_tmp_data;

end architecture;

