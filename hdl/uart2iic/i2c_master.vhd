
-- I2C Master component
-- Only for one-master designs

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity i2c_master is
	generic (
		FREQ_IN  : natural := 100000000;
		FREQ_OUT : natural := 100000
	);
	port (
		reset         : in  std_logic;
		clk           : in  std_logic;
		-- The input FIFO
		fifo_in_ack   : in  std_logic;
		fifo_in_rdy   : out std_logic;
		fifo_in_data  : in  std_logic_vector(7 downto 0);
		-- The output FIFO
		fifo_out_ack  : in  std_logic;
		fifo_out_rdy  : out std_logic;
		fifo_out_data : out std_logic_vector(7 downto 0);
		-- The I2C ports
		scl           : inout std_logic;
		sda           : inout std_logic
	);
end i2c_master;

architecture augh of i2c_master is

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	constant DATA_BITS : natural := 8;
	constant ADDR_BITS : natural := 7;
	constant CLKDIV    : natural := FREQ_IN / FREQ_OUT / 4;

	-- Bit width of a counter that counts from 0 to clock_divider-1
	constant CLKDIV_WIDTH : natural := storebitsnb(CLKDIV-1);
	-- Bit width of a counter that counts from 0 to DATA_BITS-1 or to ADDR_BITS-1
	constant CNT_WIDTH    : natural := 4;

	constant EVENT_ACK   : std_logic_vector(7 downto 0) := "00000001";
	constant EVENT_NACK  : std_logic_vector(7 downto 0) := "00000010";
	constant EVENT_ERROR : std_logic_vector(7 downto 0) := "00000100";

	constant CMD_START   : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(0, 8));
	constant CMD_RESTART : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(1, 8));
	constant CMD_STOP    : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(2, 8));
	constant CMD_ACK     : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(3, 8));
	constant CMD_NACK    : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(4, 8));
	constant CMD_WRITE   : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(5, 8));
	constant CMD_READ    : std_logic_vector(7 downto 0) := std_logic_vector(to_unsigned(6, 8));

	-- Types for little FSM
	type fsm_status_type is (
		fsm_wait,
		fsm_start, fsm_restart, fsm_stop,
		fsm_read, fsm_write,
		fsm_gen_ack, fsm_gen_nack,
		fsm_get_ack
	);
	signal status, status_n : fsm_status_type := fsm_wait;

	signal clkdiv_cnt, clkdiv_cnt_n : unsigned(CLKDIV_WIDTH-1 downto 0) := (others => '0');
	signal clkdiv_freeze   : std_logic := '0';
	signal clkdiv_end      : std_logic := '0';
	signal clkdiv_state    : std_logic_vector(3 downto 0) := "0001";
	signal clkdiv_cl1      : std_logic := '0';
	signal clkdiv_cl2      : std_logic := '0';
	signal clkdiv_ch1      : std_logic := '0';
	signal clkdiv_ch2      : std_logic := '0';
	signal clkdiv_endstate : std_logic := '0';

	signal bits_in,  bits_in_n  : std_logic_vector(DATA_BITS-1 downto 0);
	signal bits_out, bits_out_n : std_logic_vector(DATA_BITS-1 downto 0);

	signal bits_in_valid,  bits_in_valid_n  : std_logic := '0';
	signal bits_out_valid, bits_out_valid_n : std_logic := '0';

	signal bits_idx, bits_idx_n : unsigned(CNT_WIDTH-1 downto 0) := (others => '0');

	signal read_bit, read_bit_n : std_logic := '0';

	signal sda_drive0, sda_drive0_n : std_logic := '0';
	signal scl_drive0, scl_drive0_n : std_logic := '0';

	signal scl_buf1, scl_buf2 : std_logic := '0';

begin

	-- Alias signals

	clkdiv_end <= '1' when clkdiv_cnt = CLKDIV-1 else '0';

	clkdiv_cl2 <= clkdiv_state(0);
	clkdiv_ch1 <= clkdiv_state(1);
	clkdiv_ch2 <= clkdiv_state(2);
	clkdiv_cl1 <= clkdiv_state(3);

	clkdiv_endstate <= clkdiv_end and clkdiv_cl1;

	-- Sequential process

	process (clk)
	begin
		if rising_edge(clk) then

			if clkdiv_freeze = '0' then
				clkdiv_cnt <= clkdiv_cnt_n;
				if clkdiv_end = '1' then
					clkdiv_state <= clkdiv_state(2 downto 0) & clkdiv_state(3);
				end if;
			end if;

			status         <= status_n;

			bits_in        <= bits_in_n;
			bits_out       <= bits_out_n;
			bits_in_valid  <= bits_in_valid_n;
			bits_out_valid <= bits_out_valid_n;
			bits_idx       <= bits_idx_n;

			read_bit       <= read_bit_n;

			sda_drive0     <= sda_drive0_n;
			scl_drive0     <= scl_drive0_n;

			scl_buf2       <= scl_buf1;
			scl_buf1       <= scl;

			if reset = '1' then
				status         <= fsm_wait;
				clkdiv_cnt     <= (others => '0');
				clkdiv_state   <= "0001";
				bits_in_valid  <= '0';
				bits_out_valid <= '0';
				sda_drive0     <= '0';
				scl_drive0     <= '0';
			end if;

		end if;
	end process;

	-- Combinatorial process

	process (
		status,
		clkdiv_cnt, clkdiv_end, clkdiv_cl1, clkdiv_cl2, clkdiv_ch1, clkdiv_ch2, clkdiv_endstate,
		bits_in, bits_out, bits_in_valid, bits_out_valid, bits_idx, read_bit,
		fifo_in_data, fifo_in_ack, fifo_out_ack,
		sda_drive0, scl_drive0,
		sda, scl_buf2
	)
		variable var_change_state : boolean := false;
	begin

		var_change_state := false;

		-- The FIFO data input
		fifo_in_rdy   <= '0';

		-- The FIFO data output
		fifo_out_data <= bits_out;
		fifo_out_rdy  <= bits_out_valid;

		-- Default values for internal signals
		status_n <= status;

		bits_in_n        <= bits_in;
		bits_out_n       <= bits_out;
		bits_in_valid_n  <= bits_in_valid;
		bits_out_valid_n <= bits_out_valid;
		bits_idx_n       <= bits_idx;

		read_bit_n       <= read_bit;

		sda_drive0_n     <= '0';
		scl_drive0_n     <= '0';

		-- Clock divider
		clkdiv_cnt_n <= clkdiv_cnt + 1;
		if clkdiv_end = '1' then
			clkdiv_cnt_n <= (others => '0');
		end if;
		clkdiv_freeze <= '0';

		-- Handle data output
		if bits_out_valid = '1' then
			if fifo_out_ack = '1' then
				bits_out_valid_n <= '0';
			end if;
		end if;

		-- The states
		-- One bit starts at SCL low (end) and ends at SCL low (start of next period)

		if status = fsm_wait then

			sda_drive0_n <= sda_drive0;
			scl_drive0_n <= scl_drive0;

			var_change_state := true;

		elsif status = fsm_start then

			sda_drive0_n <= clkdiv_ch2 or clkdiv_cl1;
			scl_drive0_n <= clkdiv_cl1;

			var_change_state := true;

		elsif status = fsm_restart then

			sda_drive0_n <= clkdiv_ch2 or clkdiv_cl1;
			scl_drive0_n <= clkdiv_cl1;

			var_change_state := true;

		elsif status = fsm_stop then

			sda_drive0_n <= clkdiv_cl2 or clkdiv_ch1;
			scl_drive0_n <= clkdiv_cl2;

			var_change_state := true;

		elsif status = fsm_read then

			sda_drive0_n <= '0';
			scl_drive0_n <= clkdiv_cl1 or clkdiv_cl2;

			-- Get the slave value at middle of clock high
			if (clkdiv_end = '1') and (clkdiv_ch1 = '1') then
				read_bit_n <= sda;
			end if;

			if clkdiv_endstate = '1' then
				bits_idx_n <= bits_idx - 1;
				bits_out_n <= bits_out(DATA_BITS-2 downto 0) & read_bit;
				if bits_idx = 0 then
					bits_out_valid_n <= '1';
				end if;
			end if;

			if bits_idx = 0 then
				var_change_state := true;
			end if;

		elsif status = fsm_write then

			-- Wait for input data
			-- Note: This happens only at the beginning of the write state
			if bits_in_valid = '0' then
				clkdiv_freeze <= '1';
				-- The FIFO data input
				fifo_in_rdy <= '1';
				if fifo_in_ack = '1' then
					bits_in_n <= fifo_in_data;
					bits_in_valid_n <= '1';
				end if;
			end if;

			sda_drive0_n <= not bits_in(DATA_BITS-1);
			scl_drive0_n <= clkdiv_cl1 or clkdiv_cl2;

			if clkdiv_endstate = '1' then
				bits_idx_n <= bits_idx - 1;
				bits_in_n  <= bits_in(DATA_BITS-2 downto 0) & '0';
			end if;

			if bits_idx = 0 then
				if clkdiv_endstate = '1' then
					bits_in_valid_n <= '0';
					status_n <= fsm_get_ack;
				end if;
			end if;

		elsif status = fsm_gen_ack then

			sda_drive0_n <= '1';
			scl_drive0_n <= clkdiv_cl1 or clkdiv_cl2;

			var_change_state := true;

		elsif status = fsm_gen_nack then

			sda_drive0_n <= '0';
			scl_drive0_n <= clkdiv_cl1 or clkdiv_cl2;

			var_change_state := true;

		elsif status = fsm_get_ack then

			sda_drive0_n <= '0';
			scl_drive0_n <= clkdiv_cl1 or clkdiv_cl2;

			-- Get the slave ack at middle of clock high
			if (clkdiv_end = '1') and (clkdiv_ch1 = '1') then
				read_bit_n <= sda;
			end if;

			if clkdiv_endstate = '1' then
				if read_bit = '0' then
					bits_out_n <= EVENT_ACK;
				else
					bits_out_n <= EVENT_NACK;
				end if;
				bits_out_valid_n <= '1';
			end if;

			var_change_state := true;

		end if;

		-- Implement clock stretching, at beginning of first clock high
		if (clkdiv_cnt < 4) and (clkdiv_ch1 = '1') then
			if (scl_drive0 = '0') and (scl_buf2 = '0') then
				clkdiv_freeze <= '1';
			end if;
		end if;

		-- Check state change
		if var_change_state = true then
			if clkdiv_endstate = '1' then

				fifo_in_rdy <= '1';
				if fifo_in_ack = '1' then

					bits_in_valid_n <= '0';
					bits_idx_n <= to_unsigned(DATA_BITS-1, bits_idx_n'length);

					if fifo_in_data = CMD_START then
						status_n <= fsm_start;
					elsif fifo_in_data = CMD_RESTART then
						status_n <= fsm_restart;
					elsif fifo_in_data = CMD_STOP then
						status_n <= fsm_stop;
					elsif fifo_in_data = CMD_ACK then
						status_n <= fsm_gen_ack;
					elsif fifo_in_data = CMD_NACK then
						status_n <= fsm_gen_nack;
					elsif fifo_in_data = CMD_WRITE then
						status_n <= fsm_write;
					elsif fifo_in_data = CMD_READ then
						status_n <= fsm_read;
					else
						bits_out_n <= EVENT_ERROR;
						bits_out_valid_n <= '1';
						status_n <= fsm_wait;
					end if;

				else
					status_n <= fsm_wait;
				end if;

			end if;
		end if;

	end process;

	-- The I2C wires

	sda <= '0' when sda_drive0 = '1' else 'Z';
	scl <= '0' when scl_drive0 = '1' else 'Z';

end architecture;

