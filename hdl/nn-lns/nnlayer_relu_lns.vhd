
-- This custom activation layer applies ReLU + conversion to log representation
-- The activation function is common to all layers

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnlayer_relu_lns is
	generic(
		WDATA : natural := 9;
		SDATA : boolean := true;
		WOUT  : natural := 4;
		SOUT  : boolean := false;
		PAR   : natural := 1;
		-- Identifier of layer (optional)
		LAYER_ID : natural := 0;
		-- Identifier of user function (optional)
		USER_ID : natural := 0;
		-- Enable/disable input buffering and flow control
		INBUF : boolean := true;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false
	);
	port(
		clk            : in  std_logic;
		clear          : in  std_logic;
		-- Data input
		data_in        : in  std_logic_vector(PAR*WDATA-1 downto 0);
		data_in_valid  : in  std_logic;
		data_in_ready  : out std_logic;
		-- Data output
		data_out       : out std_logic_vector(PAR*WOUT-1 downto 0);
		data_out_valid : out std_logic;
		-- The output data enters a FIFO. This indicates the available room.
		out_fifo_room  : in  std_logic_vector(15 downto 0)
	);
end nnlayer_relu_lns;

architecture synth of nnlayer_relu_lns is

	-- First stage buffers
	signal clear1      : std_logic := '0';
	signal data_in1    : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid1 : std_logic := '0';

	-- One buffer to reduce routing pressure between input and output FIFOs
	-- This register indicates the input data is accepted
	signal buf_in_ready : std_logic := '0';

begin

	-------------------------------------------------------------------
	-- Input buffers
	-------------------------------------------------------------------

	gen_inbuf : if INBUF = true generate

		process(clk)
		begin
			if rising_edge(clk) then

				-- First stage buffers
				clear1      <= clear;
				data_in1    <= data_in;
				data_valid1 <= data_in_valid and buf_in_ready;

				-- Validate data entry in the pipeline
				buf_in_ready <= '0';
				if unsigned(out_fifo_room) >= 4 + FIFOMARGIN then
					buf_in_ready <= '1';
				end if;

				-- General clear
				if clear1 = '1' then
					data_valid1 <= '0';
				end if;

			end if;  -- Rising edge of clock
		end process;

	else generate

		-- Input is always enabled
		buf_in_ready <= '1';

		-- First stage buffers
		clear1      <= clear;
		data_in1    <= data_in;
		data_valid1 <= data_in_valid;

	end generate;

	-------------------------------------------------------------------
	-- Instantiate the parallel data processing paths
	-------------------------------------------------------------------

	gen_par: for p in 0 to PAR-1 generate

		signal loc_data  : std_logic_vector(WDATA-1 downto 0) := (others => '0');
		signal loc_next  : std_logic_vector(WOUT-1 downto 0) := (others => '0');
		signal loc_valid : std_logic := '0';

	begin

		loc_data <= data_in1((p+1)*WDATA-1 downto p*WDATA);

		-- Compound ReLU + conversion to log
		-- This is normally a 9-bit input ROM, which would use 8 LUTs per output bit, so 32 LUTs per ReLU instance
		-- Optimization : the MSB is a sign bit, when this is '1' then all output is "1111"
		-- Result : one 8-bit input ROM (16 LUTs) + logic OR operation (2 LUTs)

		with loc_data(7 downto 0) select loc_next <=
			"1111" when "00000000",
			"1100" when "00000001",
			"1010" when "00000010",
			"1001" when "00000011",
			"1000" when "00000100",
			"0111" when "00000101",
			"0111" when "00000110",
			"0110" when "00000111",
			"0110" when "00001000",
			"0110" when "00001001",
			"0101" when "00001010",
			"0101" when "00001011",
			"0101" when "00001100",
			"0101" when "00001101",
			"0100" when "00001110",
			"0100" when "00001111",
			"0100" when "00010000",
			"0100" when "00010001",
			"0100" when "00010010",
			"0100" when "00010011",
			"0011" when "00010100",
			"0011" when "00010101",
			"0011" when "00010110",
			"0011" when "00010111",
			"0011" when "00011000",
			"0011" when "00011001",
			"0011" when "00011010",
			"0010" when "00011011",
			"0010" when "00011100",
			"0010" when "00011101",
			"0010" when "00011110",
			"0010" when "00011111",
			"0010" when "00100000",
			"0010" when "00100001",
			"0010" when "00100010",
			"0010" when "00100011",
			"0010" when "00100100",
			"0010" when "00100101",
			"0010" when "00100110",
			"0001" when "00100111",
			"0001" when "00101000",
			"0001" when "00101001",
			"0001" when "00101010",
			"0001" when "00101011",
			"0001" when "00101100",
			"0001" when "00101101",
			"0001" when "00101110",
			"0001" when "00101111",
			"0001" when "00110000",
			"0001" when "00110001",
			"0001" when "00110010",
			"0001" when "00110011",
			"0001" when "00110100",
			"0001" when "00110101",
			"0000" when "00110110",
			"0000" when "00110111",
			"0000" when "00111000",
			"0000" when "00111001",
			"0000" when "00111010",
			"0000" when "00111011",
			"0000" when "00111100",
			"0000" when "00111101",
			"0000" when "00111110",
			"0000" when "00111111",
			"0000" when "01000000",
			"0000" when "01000001",
			"0000" when "01000010",
			"0000" when "01000011",
			"0000" when "01000100",
			"0000" when "01000101",
			"0000" when "01000110",
			"0000" when "01000111",
			"0000" when "01001000",
			"0000" when "01001001",
			"0000" when "01001010",
			"0000" when "01001011",
			"0000" when "01001100",
			"0000" when "01001101",
			"0000" when "01001110",
			"0000" when "01001111",
			"0000" when "01010000",
			"0000" when "01010001",
			"0000" when "01010010",
			"0000" when "01010011",
			"0000" when "01010100",
			"0000" when "01010101",
			"0000" when "01010110",
			"0000" when "01010111",
			"0000" when "01011000",
			"0000" when "01011001",
			"0000" when "01011010",
			"0000" when "01011011",
			"0000" when "01011100",
			"0000" when "01011101",
			"0000" when "01011110",
			"0000" when "01011111",
			"0000" when "01100000",
			"0000" when "01100001",
			"0000" when "01100010",
			"0000" when "01100011",
			"0000" when "01100100",
			"0000" when "01100101",
			"0000" when "01100110",
			"0000" when "01100111",
			"0000" when "01101000",
			"0000" when "01101001",
			"0000" when "01101010",
			"0000" when "01101011",
			"0000" when "01101100",
			"0000" when "01101101",
			"0000" when "01101110",
			"0000" when "01101111",
			"0000" when "01110000",
			"0000" when "01110001",
			"0000" when "01110010",
			"0000" when "01110011",
			"0000" when "01110100",
			"0000" when "01110101",
			"0000" when "01110110",
			"0000" when "01110111",
			"0000" when "01111000",
			"0000" when "01111001",
			"0000" when "01111010",
			"0000" when "01111011",
			"0000" when "01111100",
			"0000" when "01111101",
			"0000" when "01111110",
			"0000" when "01111111",
			"0000" when "10000000",
			"0000" when "10000001",
			"0000" when "10000010",
			"0000" when "10000011",
			"0000" when "10000100",
			"0000" when "10000101",
			"0000" when "10000110",
			"0000" when "10000111",
			"0000" when "10001000",
			"0000" when "10001001",
			"0000" when "10001010",
			"0000" when "10001011",
			"0000" when "10001100",
			"0000" when "10001101",
			"0000" when "10001110",
			"0000" when "10001111",
			"0000" when "10010000",
			"0000" when "10010001",
			"0000" when "10010010",
			"0000" when "10010011",
			"0000" when "10010100",
			"0000" when "10010101",
			"0000" when "10010110",
			"0000" when "10010111",
			"0000" when "10011000",
			"0000" when "10011001",
			"0000" when "10011010",
			"0000" when "10011011",
			"0000" when "10011100",
			"0000" when "10011101",
			"0000" when "10011110",
			"0000" when "10011111",
			"0000" when "10100000",
			"0000" when "10100001",
			"0000" when "10100010",
			"0000" when "10100011",
			"0000" when "10100100",
			"0000" when "10100101",
			"0000" when "10100110",
			"0000" when "10100111",
			"0000" when "10101000",
			"0000" when "10101001",
			"0000" when "10101010",
			"0000" when "10101011",
			"0000" when "10101100",
			"0000" when "10101101",
			"0000" when "10101110",
			"0000" when "10101111",
			"0000" when "10110000",
			"0000" when "10110001",
			"0000" when "10110010",
			"0000" when "10110011",
			"0000" when "10110100",
			"0000" when "10110101",
			"0000" when "10110110",
			"0000" when "10110111",
			"0000" when "10111000",
			"0000" when "10111001",
			"0000" when "10111010",
			"0000" when "10111011",
			"0000" when "10111100",
			"0000" when "10111101",
			"0000" when "10111110",
			"0000" when "10111111",
			"0000" when "11000000",
			"0000" when "11000001",
			"0000" when "11000010",
			"0000" when "11000011",
			"0000" when "11000100",
			"0000" when "11000101",
			"0000" when "11000110",
			"0000" when "11000111",
			"0000" when "11001000",
			"0000" when "11001001",
			"0000" when "11001010",
			"0000" when "11001011",
			"0000" when "11001100",
			"0000" when "11001101",
			"0000" when "11001110",
			"0000" when "11001111",
			"0000" when "11010000",
			"0000" when "11010001",
			"0000" when "11010010",
			"0000" when "11010011",
			"0000" when "11010100",
			"0000" when "11010101",
			"0000" when "11010110",
			"0000" when "11010111",
			"0000" when "11011000",
			"0000" when "11011001",
			"0000" when "11011010",
			"0000" when "11011011",
			"0000" when "11011100",
			"0000" when "11011101",
			"0000" when "11011110",
			"0000" when "11011111",
			"0000" when "11100000",
			"0000" when "11100001",
			"0000" when "11100010",
			"0000" when "11100011",
			"0000" when "11100100",
			"0000" when "11100101",
			"0000" when "11100110",
			"0000" when "11100111",
			"0000" when "11101000",
			"0000" when "11101001",
			"0000" when "11101010",
			"0000" when "11101011",
			"0000" when "11101100",
			"0000" when "11101101",
			"0000" when "11101110",
			"0000" when "11101111",
			"0000" when "11110000",
			"0000" when "11110001",
			"0000" when "11110010",
			"0000" when "11110011",
			"0000" when "11110100",
			"0000" when "11110101",
			"0000" when "11110110",
			"0000" when "11110111",
			"0000" when "11111000",
			"0000" when "11111001",
			"0000" when "11111010",
			"0000" when "11111011",
			"0000" when "11111100",
			"0000" when "11111101",
			"0000" when "11111110",
			"0000" when "11111111",
			"----" when others;

		process(clk)
		begin
			if rising_edge(clk) then

				-- Data
				data_out((p+1)*WOUT-1 downto p*WOUT) <= loc_next when loc_data(8) = '0' else "1111";

				-- Tag
				loc_valid <= data_valid1;

				-- General clear
				if clear1 = '1' then
					loc_valid <= '0';
				end if;

			end if;
		end process;

		-- Only one component is driving the output valid signal
		gen_out_valid : if p = 0 generate
			data_out_valid <= loc_valid;
		end generate;

	end generate;

	-------------------------------------------------------------------
	-- Output ports
	-------------------------------------------------------------------

	data_in_ready <= buf_in_ready;

end architecture;

