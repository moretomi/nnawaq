
-- This block applies parallel Add operation

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnlayer_add is
	generic(
		WDATA : natural := 8;
		SDATA : boolean := false;
		WOUT  : natural := 8;
		-- Parameters for input and output parallelism
		PAR_IN  : natural := 1;
		PAR_OUT : natural := 1;
		-- Enable/disable input buffering and flow control
		INBUF : boolean := true;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0
	);
	port(
		clk            : in  std_logic;
		clear          : in  std_logic;
		-- Data input
		data_in        : in  std_logic_vector(PAR_IN*WDATA-1 downto 0);
		data_in_valid  : in  std_logic;
		data_in_ready  : out std_logic;
		-- Data output
		data_out       : out std_logic_vector(PAR_OUT*WOUT-1 downto 0);
		data_out_valid : out std_logic;
		-- The output data enters a FIFO. This indicates the available room.
		out_fifo_room  : in  std_logic_vector(15 downto 0)
	);
end nnlayer_add;

architecture synth of nnlayer_add is

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- This check function is called at initialization of a phony constant, so the assert is displayed before VHDL elaboration with a meaningful message
	function check_par(b : boolean) return boolean is
	begin
		assert (PAR_IN mod PAR_OUT) = 0
			report "Error PAR_IN must be a multiple of PAR_OUT"
			severity failure;
		return false;
	end function;

	constant CHECK1 : boolean := check_par(false);

	-- The number of parallel inputs per pooling path
	constant ADD_PAR_IN : natural := PAR_IN / PAR_OUT;

	-- First stage buffers
	signal clear1      : std_logic := '0';
	signal data_in1    : std_logic_vector(PAR_IN*WDATA-1 downto 0) := (others => '0');
	signal data_valid1 : std_logic := '0';

	-- One buffer to reduce routing pressure between input and output FIFOs
	-- This register indicates the input data is accepted
	signal buf_in_ready : std_logic := '0';

	-- Number of layers of the adder tree that can have no buffering in between
	-- For unsigned tree, 2 layers is possible, but only one for signed tree because of the extra sign extension LUTs
	constant ADDER_REGEN : natural := 2 - to_integer(SDATA);

	-- Component declaration: A pipelined adder tree
	component addtree is
		generic (
			-- Data type and width
			WDATA : natural := 8;
			SDATA : boolean := true;
			NBIN  : natural := 20;
			WOUT  : natural := 12;
			-- User-specified radix, for testing purposes (0 means automatic)
			RADIX : natural := 0;
			-- Special considerations about data nature
			BINARY : boolean := false;
			BINARY_RADIX : natural := 0;
			TERNARY : boolean := false;
			TERNARY_RADIX : natural := 0;
			-- An optional tag
			TAGW  : natural := 1;
			TAGEN : boolean := true;
			TAGZC : boolean := true;
			-- How to add pipeline registers
			REGEN  : natural := 1;  -- 0=none, 1=all, else every EN stages
			REGIDX : natural := 0   -- Start index (from the leaves)
		);
		port (
			clk      : in  std_logic;
			clear    : in  std_logic;
			-- Data, input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WOUT-1 downto 0);
			-- Tag, input and output
			tag_in   : in  std_logic_vector(TAGW-1 downto 0);
			tag_out  : out std_logic_vector(TAGW-1 downto 0)
		);
	end component;

begin

	-------------------------------------------------------------------
	-- Input buffers
	-------------------------------------------------------------------

	gen_inbuf : if INBUF = true generate

		process(clk)
		begin
			if rising_edge(clk) then

				-- First stage buffers
				clear1      <= clear;
				data_in1    <= data_in;
				data_valid1 <= data_in_valid and buf_in_ready;

				-- Validate data entry in the pipeline
				buf_in_ready <= '0';
				if unsigned(out_fifo_room) >= 4 + FIFOMARGIN then
					buf_in_ready <= '1';
				end if;

				-- General clear
				if clear1 = '1' then
					data_valid1 <= '0';
				end if;

			end if;  -- Rising edge of clock
		end process;

	else generate

		-- Input is always enabled
		buf_in_ready <= '1';

		-- First stage buffers
		clear1      <= clear;
		data_in1    <= data_in;
		data_valid1 <= data_in_valid;

	end generate;

	-------------------------------------------------------------------
	-- Instantiate the parallel data processing paths
	-------------------------------------------------------------------

	gen_po : for po in 0 to PAR_OUT-1 generate

		signal loc_data : std_logic_vector(ADD_PAR_IN*WDATA-1 downto 0) := (others => '0');
		signal loc_val_out : std_logic := '0';

	begin

		-- Gather inputs for the tree
		-- The order corresponds to the way the previous Concat layer assembles the data
		gen_in : for i in 0 to ADD_PAR_IN-1 generate
			loc_data((i+1)*WDATA-1 downto i*WDATA) <= data_in1((i*PAR_OUT+po+1)*WDATA-1 downto (i*PAR_OUT+po)*WDATA);
		end generate;

		-- Instantiate the adder tree
		adder : addtree
			generic map (
				-- Data type and width
				WDATA => WDATA,
				SDATA => SDATA,
				NBIN  => ADD_PAR_IN,
				WOUT  => WOUT,
				-- User-specified radix, for testing purposes (0 means automatic)
				RADIX => 0,
				-- Special considerations about data nature
				BINARY => false,
				BINARY_RADIX => 0,
				TERNARY => false,
				TERNARY_RADIX => 0,
				-- An optional tag
				TAGW  => 1,
				TAGEN => true,
				TAGZC => false,
				-- How to add pipeline registers
				REGEN  => ADDER_REGEN,
				REGIDX => 0
			)
			port map (
				clk      => clk,
				clear    => clear1,
				-- Data, input and output
				data_in  => loc_data,
				data_out => data_out((po+1)*WOUT-1 downto po*WOUT),
				-- Tag, input and output
				tag_in(0)  => data_valid1,
				tag_out(0) => loc_val_out
			);

		-- Only one component is driving the output valid signal
		gen_out_valid : if po = 0 generate
			data_out_valid <= loc_val_out;
		end generate;

	end generate;

	-------------------------------------------------------------------
	-- Output ports
	-------------------------------------------------------------------

	data_in_ready <= buf_in_ready;

end architecture;

