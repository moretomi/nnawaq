
-- This defines an interface for a custom multiplication for neuron implementation
-- To be replaced by the desired custom implementation

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity neuron_custom_mul is
	generic(
		-- Parameters for data and signedness
		WDATA   : natural := 4;     -- The data bit width
		SDATA   : boolean := true;  -- The data signedness
		WWEIGHT : natural := 5;     -- The weight bit width
		SWEIGHT : boolean := true;  -- The weight signedness
		WMUL    : natural := 8;     -- The multiplication bit width
		-- Identifiers of layer
		LAYER_ID : natural := 0;
		-- Identifier of neuron layer, in case a per-layer operation is desired
		CUSTOM_MUL_ID : natural := 0;
		-- Tag-related information
		TAGW  : natural := 1;
		TAGEN : boolean := true
	);
	port(
		clk       : in  std_logic;
		-- Control signals
		data_in   : in  std_logic_vector(WDATA-1 downto 0);
		weight_in : in  std_logic_vector(WWEIGHT-1 downto 0);
		-- Data output
		mul_out   : out std_logic_vector(WMUL-1 downto 0);
		-- Tag, input and output
		tag_in    : in  std_logic_vector(TAGW-1 downto 0);
		tag_out   : out std_logic_vector(TAGW-1 downto 0)
	);
end neuron_custom_mul;

architecture synth of neuron_custom_mul is

begin

	gen : if CUSTOM_MUL_ID /= 0 generate

		-- Phony operation to be replaced by custom operation
		mul_out <= (others => '0');

	-- Perform normal multiplication

	elsif SDATA = false and SWEIGHT = false generate

		mul_out <= std_logic_vector( resize(unsigned(data_in) * unsigned(weight_in), WMUL) );

	elsif SDATA = false and SWEIGHT = true generate

		mul_out <= std_logic_vector( resize(signed('0' & data_in) * signed(weight_in), WMUL) );

	elsif SDATA = true and SWEIGHT = false generate

		mul_out <= std_logic_vector( resize(signed(data_in) * signed('0' & weight_in), WMUL) );

	else generate

		-- Both inputs are signed
		mul_out <= std_logic_vector( resize(signed(data_in) * signed(weight_in), WMUL) );

	end generate;

	tag_out <= tag_in;

end architecture;

