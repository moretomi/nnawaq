
-- This block applies Batch Normalization on-the-fly

-- Internally, there is one address register
-- The address is cleared only when the port clear = '1'
-- It is automatically incremented at each write or read

-- If enabled, the order of operations is :
--   - addition of bias
--   - multiplication by unsigned factor (either constant or run-time)
--   - shift right and resize
-- Each of these steps (except constant shift) is a pipeline register

-- Usage requirements :
--   In all circumstances : WMEM >= 1
--   In case bias and/or shr is used : WMEM >= WBIAS + WMUL + WSHR

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnlayer_norm is
	generic(
		WDATA : natural := 8;
		SDATA : boolean := false;
		WOUT  : natural := 8;
		FSIZE : natural := 1024;
		PAR   : natural := 1;
		-- Constant multiplication and shift parameters (zero means unused)
		MUL_CST : natural := 0;
		SHR_CST : natural := 0;
		-- The optional run-time bias parameter
		BIAS_EN : boolean := false;
		WBIAS   : natural := 0;
		-- The optional run-time multiplication parameter
		MUL_EN : boolean := false;
		WMUL   : natural := 0;
		-- The optional run-time shift right
		SHR_EN : boolean := false;
		WSHR   : natural := 0;
		-- Activate rounding to nearest integer (default is rounding is towards zero)
		ROUND_NEAR : boolean := false;
		-- The optional memory width
		MEM_EN : boolean := false;
		WMEM   : natural := 1;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false
	);
	port(
		clk            : in  std_logic;
		-- Ports for address control
		addr_clear     : in  std_logic;
		-- Ports for Write into memory (for bias)
		write_mode     : in  std_logic;
		write_data     : in  std_logic_vector(WMEM-1 downto 0);
		write_enable   : in  std_logic;
		-- The user-specified frame size
		user_fsize     : in  std_logic_vector(15 downto 0);
		-- Data input
		data_in        : in  std_logic_vector(PAR*WDATA-1 downto 0);
		data_in_valid  : in  std_logic;
		data_in_ready  : out std_logic;
		-- Data output
		data_out       : out std_logic_vector(PAR*WOUT-1 downto 0);
		data_out_valid : out std_logic;
		-- The output data enters a FIFO. This indicates the available room.
		out_fifo_room  : in  std_logic_vector(15 downto 0)
	);
end nnlayer_norm;

architecture synth of nnlayer_norm is

	-- Definitions for the internal memories
	type ram_type is array (0 to FSIZE-1) of std_logic_vector(WMEM-1 downto 0);

	-- First stage buffers
	signal addr_clear1   : std_logic := '0';
	signal write_mode1   : std_logic := '0';
	signal write_data1   : std_logic_vector(WMEM-1 downto 0) := (others => '0');
	signal write_enable1 : std_logic := '0';
	signal data_in1      : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid1   : std_logic := '0';

	-- Second stage buffers
	signal data_in2    : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid2 : std_logic := '0';

	-- One buffer to reduce routing pressure between input and output FIFOs
	-- This register indicates the input data is accepted
	signal buf_in_ready : std_logic := '0';
	-- This register indicats this is the last neuron value
	signal reg_end_frame : std_logic := '0';
	-- A register to enable Write to memory and disable at end of mem size
	signal reg_we : std_logic_vector(PAR-1 downto 0) := (others => '0');

	-- The internal Address register
	signal reg_addr : unsigned(15 downto 0) := (others => '0');
	-- Convenient intermediate signal
	signal buf_user_fsize : unsigned(15 downto 0);
	-- Utility register to reduce critical path
	signal buf_addr_end : unsigned(15 downto 0);

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : boolean) return natural is
	begin
		if b = true then
			return 1;
		end if;
		return 0;
	end function;

	-- Utility function to generate integer from boolean
	function to_integer(b : std_logic) return natural is
	begin
		if b = '1' then
			return 1;
		end if;
		return 0;
	end function;

begin

	-------------------------------------------------------------------
	-- Configuration parameters
	-------------------------------------------------------------------

	gen_nolock : if LOCKED = false generate

		buf_user_fsize <= unsigned(user_fsize);

		process(clk)
		begin
			if rising_edge(clk) then

				buf_addr_end <= unsigned(user_fsize) - 2;

			end if;
		end process;

	end generate;

	gen_locked : if LOCKED = true generate

		buf_user_fsize <= to_unsigned(FSIZE, 16);
		buf_addr_end   <= to_unsigned(maximum(FSIZE, 2) - 2, 16);

	end generate;

	-------------------------------------------------------------------
	-- State machine
	-------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			-- First stage buffers
			addr_clear1   <= addr_clear;
			write_mode1   <= write_mode;
			write_data1   <= write_data;
			write_enable1 <= write_enable and write_mode;
			data_in1      <= data_in;
			data_valid1   <= data_in_valid and buf_in_ready;

			-- Second stage buffers
			data_in2      <= data_in1;
			data_valid2   <= data_valid1;

			-- Out of write mode, clear the write enable registers
			if write_mode1 = '0' then
				reg_we <= (others => '0');
			end if;
			-- Rising edge on write mode enables write in the first block
			if (write_mode = '1') and (write_mode1 = '0') then
				reg_we(0) <= '1';
			end if;
			-- Inhibit write enable when the last write address is reached
			if write_enable1 = '1' then
				-- Activate the next block when one is full
				if reg_end_frame = '1' then
					reg_we(0) <= '0';
					if PAR > 1 then
						reg_we <= reg_we(PAR-2 downto 0) & '0';
					end if;
				end if;
			end if;

			-- Validate data entry in the pipeline
			buf_in_ready <= '0';
			if (unsigned(out_fifo_room) >= 8 + FIFOMARGIN) and (write_mode1 = '0') then
				buf_in_ready <= '1';
			end if;

			-- The Address register
			if (write_enable1 = '1') or (data_valid1 = '1') then
				reg_addr <= reg_addr + 1;
				reg_end_frame <= '0';
				if (reg_addr = buf_addr_end) or (unsigned(buf_user_fsize) = 1) then
					reg_end_frame <= '1';
				end if;
				if reg_end_frame = '1' then
					reg_addr <= (others => '0');
				end if;
			end if;

			-- Clear the address at entry and exit of write mode
			if write_mode /= write_mode1 then
				reg_addr <= (others => '0');
				reg_end_frame <= '0';
			end if;

			-- General clear
			if (addr_clear1 = '1') then
				write_mode1   <= '0';
				write_enable1 <= '0';
				data_valid1   <= '0';
				data_valid2   <= '0';
				reg_addr      <= (others => '0');
				reg_end_frame <= '0';
				reg_we        <= (others => '0');
			end if;

		end if;  -- Rising edge of clock
	end process;

	-------------------------------------------------------------------
	-- Instantiate the parallel data processing paths
	-------------------------------------------------------------------

	gen_par: for p in 0 to PAR-1 generate

		-- Note : First stage is the buffering of component inputs

		-- Second stage buffers
		-- Read data from RAM (buffered)
		signal bias_ram2 : std_logic_vector(WMEM-1 downto 0) := (others => '0');

		-- Tag propagation, constants to ease code reuse
		constant TAGW  : natural := 1;
		constant TAGEN : boolean := (p = 0);
		constant TAGZC : boolean := true;

		-- Compute data width at output of each stage
		constant WDATA_MAX  : natural := maximum(WDATA, to_integer(BIAS_EN) * WBIAS);
		constant WDATA_BIAS : natural := WDATA_MAX  + to_integer(BIAS_EN);
		constant WDATA_MUL1 : natural := WDATA_BIAS + to_integer(MUL_EN) * WMUL;
		constant WDATA_MUL2 : natural := WDATA_MUL1 + to_integer(MUL_CST > 1) * storebitsnb(maximum(MUL_CST, 1) - 1);
		constant WDATA_SHR  : natural := WOUT;

		signal sig_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');
		signal sig_data : std_logic_vector(WDATA-1 downto 0) := (others => '0');

		-- Signals for the bias stage
		signal postbias_data : std_logic_vector(WDATA_BIAS-1 downto 0) := (others => '0');
		signal postbias_mem  : std_logic_vector(WMEM-1 downto 0) := (others => '0');
		signal postbias_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		-- Signals for the 1st multiplication stage
		signal postmul1_data : std_logic_vector(WDATA_MUL1-1 downto 0) := (others => '0');
		signal postmul1_mem  : std_logic_vector(WMEM-1 downto 0) := (others => '0');
		signal postmul1_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		-- Signals for the 2nd multiplication stage
		signal postmul2_data : std_logic_vector(WDATA_MUL2-1 downto 0) := (others => '0');
		signal postmul2_mem  : std_logic_vector(WMEM-1 downto 0) := (others => '0');
		signal postmul2_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		-- Signal for the total shift value : max size of WSHR and SHR_CST, + 1b if both are used
		constant WSHR_INT : natural := maximum(maximum(WSHR, storebitsnb(SHR_CST) - to_integer(SHR_CST = 0)) + to_integer(WSHR > 0 and SHR_CST > 0), 1);
		signal shift_val : unsigned(WSHR_INT-1 downto 0) := (others => '0');

		-- Signals for the shift stage
		signal postshr_next : std_logic_vector(WDATA_SHR-1 downto 0) := (others => '0');
		signal postshr_data : std_logic_vector(WDATA_SHR-1 downto 0) := (others => '0');
		signal postshr_tag  : std_logic_vector(TAGW-1 downto 0) := (others => '0');

		impure function gen_next_tag(tagi : std_logic_vector(TAGW-1 downto 0)) return std_logic_vector is
			variable tago : std_logic_vector(TAGW-1 downto 0) := (others => '0');
		begin
			tago := tagi;
			if (TAGZC = true) and (addr_clear1 = '1') then
				tago := (others => '0');
			end if;
			if TAGEN = false then
				tago := (others => '0');
			end if;
			return tago;
		end function;

	begin

		gen_nomem : if MEM_EN = false generate
			bias_ram2 <= (others => '0');
		end generate;

		gen_mem : if MEM_EN = true generate

			-- The internal memory
			signal ram : ram_type := (others => (others => '0'));

			-- In case it is necessary to save BRAM resources
			--attribute ram_style : string;
			--attribute ram_style of ram : signal is "distributed";

		begin

			-- Sequential process
			process(clk)
			begin
				if rising_edge(clk) then

					-- Handle Read and Write on the internal RAM
					if (write_enable1 = '1') and (reg_we(p) = '1') then
						ram(to_integer(unsigned(reg_addr))) <= write_data1;
					end if;
					if data_valid1 = '1' then
						bias_ram2 <= ram(to_integer(unsigned(reg_addr)));
					end if;

				end if;  -- Clock edge
			end process;

		end generate;

		-- Wrapper signals to ease code reuse
		sig_tag(0) <= data_valid2;
		sig_data   <= data_in2((p+1)*WDATA-1 downto p*WDATA);

		-- Pipeline stage : Add the bias

		gen_bias : if BIAS_EN = true generate
			signal bias_val : std_logic_vector(WBIAS-1 downto 0) := (others => '0');
		begin

			bias_val <= bias_ram2(WBIAS-1 downto 0);

			process(clk)
			begin
				if rising_edge(clk) then

					-- Data
					if SDATA = true then
						postbias_data <= std_logic_vector(resize(signed(sig_data), WDATA_BIAS) + signed(bias_val));
					else
						postbias_data <= std_logic_vector(resize(unsigned(sig_data), WDATA_BIAS) + unsigned(bias_val));
					end if;

					-- Other parameters stored in memory
					postbias_mem <= bias_ram2;

					-- Tag
					postbias_tag <= gen_next_tag(sig_tag);

				end if;
			end process;

		else generate
			postbias_data <= sig_data;
			postbias_mem  <= bias_ram2;
			postbias_tag  <= gen_next_tag(sig_tag);
		end generate;

		-- Pipeline stage : Non-constant multiplication

		gen_mul1 : if MUL_EN = true generate
			signal mul_val : unsigned(WMUL-1 downto 0) := (others => '0');
		begin

			mul_val <= unsigned(postbias_mem(WBIAS+WMUL-1 downto WBIAS));

			process(clk)
			begin
				if rising_edge(clk) then

					-- Data
					if SDATA = true then
						postmul1_data <= std_logic_vector(resize(signed(postbias_data) * signed('0' & mul_val), postmul1_data'length));
					else
						postmul1_data <= std_logic_vector(resize(unsigned(postbias_data) * mul_val, postmul1_data'length));
					end if;

					-- Other parameters stored in memory
					postmul1_mem <= postbias_mem;

					-- Tag
					postmul1_tag <= gen_next_tag(postbias_tag);

				end if;
			end process;

		else generate
			postmul1_data <= postbias_data;
			postmul1_mem  <= postbias_mem;
			postmul1_tag  <= gen_next_tag(postbias_tag);
		end generate;

		-- Pipeline stage : Constant multiplication

		gen_mul2 : if MUL_CST > 1 generate

			process(clk)
			begin
				if rising_edge(clk) then

					-- Data
					if SDATA = true then
						postmul2_data <= std_logic_vector(resize(signed(postmul1_data) * MUL_CST, postmul2_data'length));
					else
						postmul2_data <= std_logic_vector(resize(unsigned(postmul1_data) * MUL_CST, postmul2_data'length));
					end if;

					-- Other parameters stored in memory
					postmul2_mem <= postmul1_mem;

					-- Tag
					postmul2_tag <= gen_next_tag(postmul1_tag);

				end if;
			end process;

		else generate
			postmul2_data <= postmul1_data;
			postmul2_mem  <= postmul1_mem;
			postmul2_tag  <= gen_next_tag(postmul1_tag);
		end generate;

		-- Descaling :
		-- Rounding towards zero : just use division by 2^SHR
		-- Rounding to nearest integer : add (if positive) or subtract (if negative) the value 2^(SHR-1), then divide by 2^SHR

		-- Generate the total shift value
		gen_sel : if SHR_EN = true generate
			shift_val <= unsigned(postmul2_mem(WBIAS+WMUL+WSHR-1 downto WBIAS+WMUL)) + to_unsigned(SHR_CST, WSHR_INT);
		else generate
			shift_val <= to_unsigned(SHR_CST, WSHR_INT);
		end generate;

		-- Rounding towards zero
		gen_rnd : if ROUND_NEAR = false generate

			-- Main data path
			gen_sgn : if SDATA = true generate
				postshr_next <= std_logic_vector(resize((  signed(postmul2_data)) / (2 ** to_integer(shift_val)), postshr_next'length));
			else generate
				postshr_next <= std_logic_vector(resize((unsigned(postmul2_data)) / (2 ** to_integer(shift_val)), postshr_next'length));
			end generate;

		-- Rounding to nearest
		else generate

			-- Explanations :
			--   The naive implementation of rounding to nearest involves 2 shift operations, this is bad for timing
			--   note : the first would be addition of (1 << (sh-1)), the second would be shifting the result to the right
			-- Better solution :
			--   Do the correction for rounding towards zero of negative data : add 1 if data<0 and SHR>0
			--   Righ-extend data signal : data & '0'
			--   Descale by shift_right(data2, SHR)
			--   Add/sub the correction for rounding to nearest : add/sub 1 if SHR>0
			--   Descale by shift_right(data3, 1)

			-- Input data + correction for data<0, right-extended by one bit '0'
			signal sig_data1 : std_logic_vector(WDATA_MUL2 downto 0) := (others => '0');
			-- After shift_right(data1, SHR)
			signal sig_data2 : std_logic_vector(WDATA_MUL2 downto 0) := (others => '0');
			-- After addition of correction and last shift_right(data2, 1)
			signal sig_data3 : std_logic_vector(WDATA_MUL2 downto 0) := (others => '0');

		begin

			gen_sgn : if SDATA = true generate
				signal sig_neg  : std_logic := '0';
				signal sig_corr : unsigned(WDATA_MUL2 downto 0) := (others => '0');
			begin
				-- Note : Implementation is such that eval of shift_val>0 is done in parallel with all corrections and not sequentally
				-- Helper signals
				sig_neg <= postmul2_data(postmul2_data'high);  -- Just a selection of the sign bit
				sig_corr(0) <= '1';  -- Signal for correction in the data path that assumes shift_val>0
				-- Main data path
				sig_data1 <= std_logic_vector(signed(postmul2_data) + to_integer(sig_neg)) & '0';
				sig_data2 <= std_logic_vector(shift_right(signed(sig_data1), to_integer(shift_val)));
				sig_data3 <= std_logic_vector(shift_right(signed(sig_data2) + (signed(sig_corr) xor sig_neg) + to_integer(sig_neg), 1));
				postshr_next <=
					std_logic_vector(resize(signed(sig_data3), postshr_next'length)) when shift_val > 0 else
					std_logic_vector(resize(signed(postmul2_data), postshr_next'length));
			else generate
				sig_data1 <= postmul2_data & '0';
				sig_data2 <= std_logic_vector(shift_right(unsigned(sig_data1), to_integer(shift_val)));
				sig_data3 <= std_logic_vector(shift_right(unsigned(sig_data2) + to_integer(shift_val > 0), 1));
				postshr_next <= std_logic_vector(resize(unsigned(sig_data3), postshr_next'length));
			end generate;

		end generate;

		-- Pipeline stage : Descale

		gen_shr : if (SHR_EN = true or SHR_CST > 0) and (SDATA = true or ROUND_NEAR = true) generate

			process(clk)
			begin
				if rising_edge(clk) then
					postshr_data <= postshr_next;
					postshr_tag <= gen_next_tag(postmul2_tag);
				end if;
			end process;

		else generate
			postshr_data <= postshr_next;
			postshr_tag  <= gen_next_tag(postmul2_tag);
		end generate;

		data_out((p+1)*WOUT-1 downto p*WOUT) <= postshr_data;

		-- Only one component is driving the output valid signal
		gen_out_valid : if (TAGEN = true) and (p = 0) generate
			data_out_valid <= postshr_tag(0);
		end generate;

	end generate;

	-------------------------------------------------------------------
	-- Output ports
	-------------------------------------------------------------------

	data_in_ready <= buf_in_ready;

end architecture;

