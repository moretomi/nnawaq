
-- This block recodes data on-the-fly
-- Internally, there is one address register
-- The address is cleared only when the port clear = '1'
-- It is automatically incremented at each write or read

-- Specification :
-- Implementation is a 2-steps activation function, the 3 outputs are configurable
-- Note : No need for additional per-neuron bias value, this functionality is covered by the configurable thresholds

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nnlayer_recode_to_ter is
	generic(
		WDATA : natural := 12;
		SDATA : boolean := true;
		WOUT  : natural := 2;
		FSIZE : natural := 1024;
		PAR   : natural := 1;
		-- Take extra margin on the FIFO level, in case there is something outside
		FIFOMARGIN : natural := 0;
		-- Lock the layer parameters to the generic parameter value
		LOCKED : boolean := false
	);
	port(
		clk             : in  std_logic;
		-- Ports for address control
		addr_clear      : in  std_logic;
		-- Ports for Write into memory
		write_mode      : in  std_logic;
		write_data      : in  std_logic_vector(2*WDATA+3*WOUT-1 downto 0);
		write_enable    : in  std_logic;
		-- The user-specified frame size
		user_fsize      : in  std_logic_vector(15 downto 0);
		-- Data input
		data_in         : in  std_logic_vector(PAR*WDATA-1 downto 0);
		data_in_valid   : in  std_logic;
		data_in_ready   : out std_logic;
		-- Data output
		data_out        : out std_logic_vector(PAR*WOUT-1 downto 0);
		data_out_valid  : out std_logic;
		-- The output data enters a FIFO. This indicates the available room.
		out_fifo_room   : in  std_logic_vector(15 downto 0)
	);
end nnlayer_recode_to_ter;

architecture synth of nnlayer_recode_to_ter is

	-- Definitions for the internal memories
	constant RAM_WDATA : natural := 2 * WDATA + 3 * WOUT;
	type ram_type is array (0 to FSIZE-1) of std_logic_vector(RAM_WDATA-1 downto 0);

	-- Utility function to generate std_logic from boolean
	function to_std_logic(b : boolean) return std_logic is
		variable i : std_logic := '0';
	begin
		i := '1' when b = true else '0';
		return i;
	end function;

	-- First stage buffers
	signal addr_clear1   : std_logic := '0';
	signal write_mode1   : std_logic := '0';
	signal write_data1   : std_logic_vector(RAM_WDATA-1 downto 0) := (others => '0');
	signal write_enable1 : std_logic := '0';
	signal data_in1      : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid1   : std_logic := '0';

	-- Second stage buffers
	signal data_in2    : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid2 : std_logic := '0';

	-- Third stage buffers
	signal data_in3    : std_logic_vector(PAR*WDATA-1 downto 0) := (others => '0');
	signal data_valid3 : std_logic := '0';

	-- Fourth stage buffers
	signal data_out4   : std_logic_vector(PAR*WOUT-1 downto 0) := (others => '0');
	signal data_valid4 : std_logic := '0';

	-- One buffer to reduce routing pressure between input and output FIFOs
	-- This register indicates the input data is accepted
	signal buf_in_ready : std_logic := '0';
	-- This register indicates this is the last neuron value
	signal reg_end_frame : std_logic := to_std_logic(LOCKED and (FSIZE=1));
	-- A register to enable Write to memory and to disable it at end of memory size
	signal reg_we : std_logic_vector(PAR-1 downto 0) := (others => '0');

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	-- Function to calculate register sizes better fit for the LOCKED case
	function calc_reg_size(maxval : natural; maxsize : natural) return natural is
		variable v : natural := 0;
	begin
		v := maxsize;
		if LOCKED = true then
			v := storebitsnb(maxval);
		end if;
		if v > maxsize then v := maxsize; end if;
		return v;
	end function;

	constant WREG : natural := calc_reg_size(FSIZE-1, 16);

	-- The internal Address register
	signal reg_addr : unsigned(WREG-1 downto 0) := (others => '0');
	-- Convenient intermediate signal
	signal buf_user_fsize : unsigned(WREG-1 downto 0);
	-- Utility register to reduce critical path
	signal buf_addr_end : unsigned(WREG-1 downto 0);

begin

	-------------------------------------------------------------------
	-- Configuration parameters
	-------------------------------------------------------------------

	gen_nolock : if LOCKED = false generate

		buf_user_fsize <= resize(unsigned(user_fsize), WREG);

		process(clk)
		begin
			if rising_edge(clk) then

				buf_addr_end <= resize(unsigned(user_fsize) - 2, WREG);

			end if;
		end process;

	end generate;

	gen_locked : if LOCKED = true generate

		buf_user_fsize <= to_unsigned(FSIZE, WREG);
		buf_addr_end   <= to_unsigned(maximum(FSIZE, 2) - 2, WREG);

	end generate;

	-------------------------------------------------------------------
	-- State machine
	-------------------------------------------------------------------

	process(clk)
	begin
		if rising_edge(clk) then

			-- First stage buffers
			addr_clear1   <= addr_clear;
			write_mode1   <= write_mode;
			write_data1   <= write_data;
			write_enable1 <= write_enable and write_mode;
			data_in1      <= data_in;
			data_valid1   <= data_in_valid and buf_in_ready;

			-- Second stage buffers
			data_in2      <= data_in1;
			data_valid2   <= data_valid1;

			-- Third stage buffers
			data_in3      <= data_in2;
			data_valid3   <= data_valid2;

			-- Fourth stage buffers
			data_valid4   <= data_valid3;

			-- Out of write mode, clear the write enable registers
			if write_mode1 = '0' then
				reg_we <= (others => '0');
			end if;
			-- Rising edge on write mode enables write in the first block
			if (write_mode = '1') and (write_mode1 = '0') then
				reg_we(0) <= '1';
			end if;
			-- Inhibit write enable when the last write address is reached
			if write_enable1 = '1' then
				-- Activate the next block when one is full
				if reg_end_frame = '1' then
					reg_we(0) <= '0';
					if PAR > 1 then
						reg_we <= reg_we(PAR-2 downto 0) & '0';
					end if;
				end if;
			end if;

			-- Validate data entry in the pipeline
			buf_in_ready <= '0';
			if (unsigned(out_fifo_room) >= 8 + FIFOMARGIN) and (write_mode1 = '0') then
				buf_in_ready <= '1';
			end if;

			-- The Address register
			if (write_enable1 = '1') or (data_valid1 = '1') then
				reg_addr <= reg_addr + 1;
				reg_end_frame <= '0';
				if (reg_addr = buf_addr_end) or (buf_user_fsize = 1) then
					reg_end_frame <= '1';
				end if;
				if reg_end_frame = '1' then
					reg_addr <= (others => '0');
				end if;
			end if;

			-- Clear the address at entry and exit of write mode
			if write_mode /= write_mode1 then
				reg_addr <= (others => '0');
				reg_end_frame <= '1' when buf_user_fsize = 1 else '0';
			end if;

			-- General clear
			if (addr_clear1 = '1') then
				write_mode1   <= '0';
				write_enable1 <= '0';
				data_valid1   <= '0';
				data_valid2   <= '0';
				data_valid3   <= '0';
				data_valid4   <= '0';
				reg_addr      <= (others => '0');
				reg_end_frame <= '1' when buf_user_fsize = 1 else '0';
				reg_we        <= (others => '0');
			end if;

		end if;  -- Rising edge of clock
	end process;


	-------------------------------------------------------------------
	-- Instantiate the recode components
	-------------------------------------------------------------------

	gen_par: for p in 0 to PAR-1 generate

		signal data_in3_loc : std_logic_vector(WDATA-1 downto 0) := (others => '0');

		-- The internal memory
		signal ram : ram_type := (others => (others => '0'));

		-- Second stage buffers
		signal data_ram2 : std_logic_vector(RAM_WDATA-1 downto 0) := (others => '0');
		-- Third stage buffers
		signal data_ram3 : std_logic_vector(RAM_WDATA-1 downto 0) := (others => '0');

		-- Prevent that reg to be absorbed into the BRAM (better for delay)
		-- FIXME Only useful for BRAM based storage, but BRAM storage is not enforced
		attribute keep : string;
		attribute keep of data_ram3 : signal is "TRUE";

		-- These intermediate signals only exist for the purpose of debug
		signal thlow : std_logic_vector(WDATA-1 downto 0) := (others => '0');
		signal thup  : std_logic_vector(WDATA-1 downto 0) := (others => '0');

		-- Comparators and multiplexer
		signal cmplow : std_logic := '0';
		signal cmpup  : std_logic := '0';
		signal muxres : std_logic_vector(WOUT-1 downto 0) := (others => '0');

	begin

		-- Sequential process
		process(clk)
		begin
			if rising_edge(clk) then

				-- Handle Read and Write on the internal RAM
				if (write_enable1 = '1') and (reg_we(p) = '1') then
					ram(to_integer(reg_addr)) <= write_data1;
				end if;
				-- FIXME This syntax may prevent the synthesis tool to use 1-port memory
				if data_valid1 = '1' then
					data_ram2 <= ram(to_integer(reg_addr));
				end if;

				-- Third stage buffers
				data_ram3 <= data_ram2;

				-- Fourth stage buffers
				data_out4((p+1)*WOUT-1 downto p*WOUT) <= muxres;

			end if;  -- Clock edge
		end process;

		-- Extract the threshold values, for the sake of observing these in waveforms
		thlow <= data_ram3(WDATA-1 downto 0);
		thup  <= data_ram3(2*WDATA-1 downto WDATA);

		-- Comparators
		data_in3_loc <= data_in3((p+1)*WDATA-1 downto p*WDATA);
		cmplow <=
			'1' when (SDATA = false) and (unsigned(data_in3_loc) < unsigned(thlow)) else
			'1' when (SDATA = true)  and (  signed(data_in3_loc) <   signed(thlow)) else
			'0';
		cmpup <=
			'1' when (SDATA = false) and (unsigned(data_in3_loc) > unsigned(thup)) else
			'1' when (SDATA = true)  and (  signed(data_in3_loc) >   signed(thup)) else
			'0';

		-- Multiplexer
		muxres <=
			data_ram3(2*WDATA+1*WOUT-1 downto 2*WDATA+0*WOUT) when cmplow = '1' else
			data_ram3(2*WDATA+3*WOUT-1 downto 2*WDATA+2*WOUT) when cmpup = '1' else
			data_ram3(2*WDATA+2*WOUT-1 downto 2*WDATA+1*WOUT);

	end generate;


	-------------------------------------------------------------------
	-- Output ports
	-------------------------------------------------------------------

	data_in_ready  <= buf_in_ready;

	data_out_valid <= data_valid4;
	data_out       <= data_out4;


end architecture;

