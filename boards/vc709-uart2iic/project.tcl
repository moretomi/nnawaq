
# Note: This is a non-project script
# For more information, please refer to the Xilinx document UG894
# "Vivado Design Suite User Guide: Using TCL Scripting", version 2014.1, page 11
# For details about the TCL commands, please refer to the Xilinx document UG835
# "Vivado Design Suite Tcl Command Reference Guide"

# Define the output directory area. Mostly for debug purposes.
set top top
set part xc7vx690tffg1761-2
set outputDir "./"
file mkdir $outputDir

# Set target part to fake current project, to read IPs
# More details: http://www.xilinx.com/support/answers/54317.html
set_part $part
set_property part $part [current_project]


# Setup design sources
read_vhdl ./hdl/top.vhd

read_vhdl ../../hwopt/hdl/fifo_with_counters.vhd

read_vhdl ../../hdl/uart2iic/uart_rx.vhd
read_vhdl ../../hdl/uart2iic/uart_tx.vhd
read_vhdl ../../hdl/uart2iic/i2c_master.vhd
read_vhdl ../../hdl/uart2iic/uart2iic.vhd


# Setup constraints
read_xdc ./VC709_Top.xdc

# No limit for messages about constant propagation, to track design bugs
set_msg_config -id "Synth 8-3333" -limit 100000
set_msg_config -id "Synth 8-3332" -limit 100000
set_msg_config -id "Synth 8-3331" -limit 100000


# Run logic synthesis
#synth_design -top $top -keep_equivalent_registers -flatten_hierarchy full -resource_sharing on
synth_design -top $top -keep_equivalent_registers -resource_sharing on

# Run logic optimization
opt_design

# Post-synthesis reports
report_utilization    -file $outputDir/post_synth_util.rpt
report_timing_summary -file $outputDir/post_synth_timing_summary.rpt


# Run automated placement
place_design

# Post-placement reports
report_utilization       -file $outputDir/post_place_util.rpt
report_clock_utilization -file $outputDir/clock_util.rpt
report_timing_summary    -file $outputDir/post_place_timing_summary.rpt


# Run routing
route_design

# Post-routing reports
report_route_status -file $outputDir/post_route_status.rpt
report_timing_summary -file $outputDir/post_route_timing_summary.rpt
report_power -file $outputDir/post_route_power.rpt
report_drc -file $outputDir/post_route_drc.rpt


# Generate the bitstream
write_bitstream -force $outputDir/$top.bit

