
-- 2 Riffa chanels are used:
--   channel 0: write ID of target register, bit 31 = WE, bit 30 = RE, next 32 bits are WData
--   channel 1: receive config & frame data from PC, send neuron outputs to PC

-- Spec: All frames are contiguous in the PCIe data transfer
-- i.e. there is no padding to fill the last PCIe word

--------------------------------------------------
-- Register mapping
--------------------------------------------------
--
-- reg 0 (read only)
--   07-00:8  : ASCII for character 'N'
--   15-08:8  : ASCII for character 'N'
--   23-16:8  : Major version
--   31-24:8  : Minor version
--
-- reg 1 (R/W)
--   31-00:32 : Write a register value, read a register value.
--              The chain is shifted if the flag is set in reg 3
--
-- reg 2 (unused)
--
-- reg 3 (partial R/W)
--      00:1  : Clear all (not written)
--      01:1  : Free run mode: output data is counted but not send back through PCIe
--      02:1  : Flag that indicates if it is possible to select the output layer
--      03:1  : Flag that indicates if it is possible to monitor the FIFOs
--   07-04:4  : Width of one data item in PCIe words, log base 2
--      08:1  : Shift enable when reading/writing config registers
--      09:1  : Get config registers (copy regs -> scan chain) - this bit is not written
--      10:1  : Set config registers (copy scan chain -> regs) - this bit is not written
--      11:1  : Flag that indicates if the NN parameters are locked
--   17-12:6  : Width of the data interface, in bytes
--   23-18:6  : (unused)
--   31-24:8  : Number of registers in the scan chain
--
-- reg 4 (R/W)
--   05-00:6  : What the PC is sending, layer index
--              All '1' = frame data
--              Other values = config for corresponding layer index
--   15-06:10 : What the PC is sending, split index
--   23-16:8  : What the HW has to send, layer index
--   31-24:8  : What the HW has to send, split index  FIXME NOT IMPLEMENTED
--
-- reg 5 (unused)
--
-- reg 6 (R/W)
--   31-00:32 : Number of result values to send to the main data channel (read: number of values sent)
--
-- reg 7 (read only)
--   31-00:32 : Number of input values sent to the CNN input
--
-- reg 8 (unused)
-- reg 9 (unused)
-- reg 10 (unused)
-- reg 11 (unused)
-- reg 12 (unused)
-- reg 13 (unused)
-- reg 14 (unused)
--
-- reg 15 (partial R/W)
--   07-00:8  : The number of values in the monitored FIFO
--      08:1  : Signal RDY, input side
--      09:1  : Signal ACK, input side
--      10:1  : Signal RDY, output side
--      11:1  : Signal ACK, output side
--   31-24:8  : The index of the FIFO to monitor
--
--------------------------------------------------
-- Protocol description
--------------------------------------------------
--
-- The beginning of each frame is NOT aligned on a data channel word boundary
-- All frames are concatenated


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity cnn_wrapper is
	generic (
		C_PCI_DATA_WIDTH : integer := 128;
		RIFFA_CHANELS_NB : integer := 2
	);
	port (
		CLK : in std_logic;
		RST : in std_logic;
		-- RX Interface
		CHNL_RX_CLK        : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX            : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_ACK        : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_LAST       : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_LEN        : in  std_logic_vector(32*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_OFF        : in  std_logic_vector(31*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_DATA       : in  std_logic_vector(C_PCI_DATA_WIDTH*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_DATA_VALID : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_RX_DATA_REN   : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		-- TX Interface
		CHNL_TX_CLK        : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX            : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_ACK        : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_LAST       : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_LEN        : out std_logic_vector(32*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_OFF        : out std_logic_vector(31*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_DATA       : out std_logic_vector(C_PCI_DATA_WIDTH*RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_DATA_VALID : out std_logic_vector(RIFFA_CHANELS_NB-1 downto 0);
		CHNL_TX_DATA_REN   : in  std_logic_vector(RIFFA_CHANELS_NB-1 downto 0)
	);
end cnn_wrapper;

architecture synth of cnn_wrapper is

	------------------------------------------------
	-- Signals to make reset last longer
	------------------------------------------------

	constant RSTVAL_IN  : std_logic := '1';
	constant RSTVAL_GEN : std_logic := '1';

	constant RESET_DURATION : natural := 64;
	signal reset_counter : unsigned(15 downto 0) := (others => '0');
	signal reset_reg : std_logic := '1';

	------------------------------------------------
	-- Signals to interface with RIFFA chanels
	------------------------------------------------

	constant RIFFA_STATE_WAIT : std_logic_vector(1 downto 0) := "00";
	constant RIFFA_STATE_LEN  : std_logic_vector(1 downto 0) := "01";
	constant RIFFA_STATE_DATA : std_logic_vector(1 downto 0) := "10";
	signal chan0_rx_state, chan0_tx_state : std_logic_vector(1 downto 0) := RIFFA_STATE_WAIT;
	signal chan1_rx_state, chan1_tx_state : std_logic_vector(1 downto 0) := RIFFA_STATE_WAIT;

	signal chan0_rcount : unsigned(31 downto 0);
	signal chan1_rcount : unsigned(31 downto 0);

	signal chan0_rlen : unsigned(31 downto 0);
	signal chan1_rlen : unsigned(31 downto 0);

	signal chan0_rdata : std_logic_vector(C_PCI_DATA_WIDTH-1 downto 0);
	signal chan1_rdata : std_logic_vector(C_PCI_DATA_WIDTH-1 downto 0);

	signal chan0_rx_ren, chan0_tx_valid : std_logic;
	signal chan1_rx_ren, chan1_tx_valid : std_logic;
	signal chan1_tx_len : unsigned(31 downto 0) := (others => '0');

	-- These signals are set to 1 for only one clock cycle
	signal chan1_rx_end : std_logic := '0';
	signal chan1_tx_end : std_logic := '0';

	------------------------------------------------
	-- Signals for config registers
	------------------------------------------------

	-- Width of address register signal
	constant REGADDRW : integer := 4;

	---- Number of Slave Registers 16
	signal slv_reg0 :  std_logic_vector(31 downto 0);
	signal slv_reg1 :  std_logic_vector(31 downto 0);
	signal slv_reg2 :  std_logic_vector(31 downto 0);
	signal slv_reg3 :  std_logic_vector(31 downto 0);
	signal slv_reg4 :  std_logic_vector(31 downto 0);
	signal slv_reg5 :  std_logic_vector(31 downto 0);
	signal slv_reg6 :  std_logic_vector(31 downto 0);
	signal slv_reg7 :  std_logic_vector(31 downto 0);
	signal slv_reg8 :  std_logic_vector(31 downto 0);
	signal slv_reg9 :  std_logic_vector(31 downto 0);
	signal slv_reg10 : std_logic_vector(31 downto 0);
	signal slv_reg11 : std_logic_vector(31 downto 0);
	signal slv_reg12 : std_logic_vector(31 downto 0);
	signal slv_reg13 : std_logic_vector(31 downto 0);
	signal slv_reg14 : std_logic_vector(31 downto 0);
	signal slv_reg15 : std_logic_vector(31 downto 0);

	signal slv_reg_rden   : std_logic := '0';
	signal slv_reg_wren   : std_logic := '0';
	signal slv_reg_rdaddr : std_logic_vector(REGADDRW-1 downto 0) := (others => '0');
	signal slv_reg_wraddr : std_logic_vector(REGADDRW-1 downto 0) := (others => '0');
	signal slv_reg_rddata : std_logic_vector(31 downto 0) := (others => '0');
	signal slv_reg_wrdata : std_logic_vector(31 downto 0) := (others => '0');
	signal slv_reg_addr   : std_logic_vector(REGADDRW-1 downto 0) := (others => '0');

	------------------------------------------------
	-- Non-addressable config registers
	------------------------------------------------

	-- AUTOGEN CONFIG NB BEGIN

	-- AUTOGEN CONFIG NB END

	-- The scan chain of config registers
	signal config_chain_regs : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	signal config_regs       : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	signal config_regs_next  : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	signal config_regs_read  : std_logic_vector(31 downto 0) := (others => '0');

	-- The commands for the scan chain of config registers
	signal config_chain_shift : std_logic_vector(CONFIG_CHAIN_NB-1 downto 0) := (others => '0');
	signal config_chain_get   : std_logic_vector(CONFIG_CHAIN_NB-1 downto 0) := (others => '0');
	signal config_chain_set   : std_logic_vector(CONFIG_CHAIN_NB-1 downto 0) := (others => '0');
	signal config_chain_def   : std_logic := '0';

	----------------------------------------------------
	-- Definitions for the neural network
	----------------------------------------------------

	-- Compute the minimum number of bits needed to store the input value
	function storebitsnb(vin : natural) return natural is
		variable r : natural := 1;
		variable v : natural := vin;
	begin
		loop
			exit when v <= 1;
			r := r + 1;
			v := v / 2;
		end loop;
		return r;
	end function;

	function bool_to_logic(b: boolean) return std_logic is
	begin
		if b = false then
			return '0';
		end if;
		return '1';
	end function;

	function logic_to_vector(b: std_logic) return std_logic_vector is
		variable r : std_logic_vector(0 downto 0);
	begin
		r(0) := b;
		return r;
	end function;

	-- Compute the power of 2 that is greater or equal to the input
	function uppow2(vin : natural) return natural is
		variable v : natural := 1;
	begin
		v := 1;
		loop
			exit when v >= vin;
			v := v * 2;
		end loop;
		return v;
	end function;

	-- AUTOGEN CST DECL BEGIN

	-- AUTOGEN CST DECL END

	-- This function applies constant bits to the config regs vector
	function config_regs_apply_const(vin : std_logic_vector) return std_logic_vector is
		variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0);
	begin
		config_regs_var := vin;

		-- AUTOGEN REGS SETCONST BEGIN

		-- AUTOGEN REGS SETCONST END

		return config_regs_var;
	end function;

	-- This function applies constant bits to the config regs vector, for reset or for locking user fields
	function config_regs_apply_const_locked(vin : std_logic_vector) return std_logic_vector is
		variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0);
	begin
		config_regs_var := vin;

		-- AUTOGEN REGS SETCONST LOCKED BEGIN

		-- AUTOGEN REGS SETCONST LOCKED END

		return config_regs_var;
	end function;

	constant WDATA_ROUND_POW2 : natural := uppow2(FIRSTFIFO_DATAW);
	constant INPAR_ROUND_POW2 : natural := uppow2(FIRSTFIFO_PAR);

	signal req_start_recv : std_logic := '0';
	signal req_start_send : std_logic := '0';

	constant CST_RECV_FRAME   : std_logic_vector(5 downto 0) := (others => '1');
	constant CST_SEND_DEFAULT : std_logic_vector(7 downto 0) := (others => '1');

	signal cur_recv1 : std_logic_vector(5 downto 0);
	signal cur_recv2 : std_logic_vector(9 downto 0);
	signal cur_send  : std_logic_vector(7 downto 0);
	signal send_is_last : std_logic := '1';

	signal cur_freerun : std_logic := '0';

	-- Alias signals to abstract the actual HW interface in the main implementation (RX direction)
	signal alias_rx_data  : std_logic_vector(CONFIG_IFW-1 downto 0);
	signal alias_rx_valid : std_logic := '0';
	signal alias_rx_ready : std_logic := '0';

	-- Alias signals to abstract the actual HW interface in the main implementation (TX direction)
	signal alias_tx_valid : std_logic := '0';
	signal alias_tx_ready : std_logic := '0';

	-- Buffer to read inputs from the main data channel, or from slave registers
	signal rxbuf_data,  rxbuf_data_n  : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal rxbuf_nbits, rxbuf_nbits_n : unsigned(7 downto 0) := (others => '0');  -- Number of bits in the buffer

	-- Counters for input values received from the main data channel
	signal rxcnt_cur, rxcnt_cur_n : unsigned(31 downto 0) := (others => '0');  -- Number of values obtained

	-- Buffer to sent output to the main data channel
	signal txbuf_data,  txbuf_data_n  : std_logic_vector(CONFIG_IFW-1 downto 0) := (others => '0');
	signal txbuf_nbits, txbuf_nbits_n : unsigned(7 downto 0) := (others => '0');  -- Number of bits in the buffer

	-- Enable signal for sending output values to the main data channel
	signal txrun_en, txrun_en_n         : std_logic := '0';
	signal txrun_gotall, txrun_gotall_n : std_logic := '0';

	-- Counters for output values sent to the main data channel
	signal txcnt_want             : unsigned(31 downto 0) := (others => '0');  -- Number of values to send
	signal txcnt_cur, txcnt_cur_n : unsigned(31 downto 0) := (others => '0');  -- Number of values obtained

	-- Signals for distributed MUX, to monitor the FIFOs
	signal monitorfifo_in  : std_logic_vector(CONFIG_FIFOS_NB*12-1 downto 0) := (others => '0');
	signal monitorfifo_out : std_logic_vector(12-1 downto 0) := (others => '0');

	-- Signals for scatter-gather, to select the output layer
	signal selout_en_in   : std_logic;
	signal selout_en_out  : std_logic_vector(CONFIG_SELOUT_NB-1 downto 0) := (others => '0');
	signal selout_gat_in  : std_logic_vector(CONFIG_SELOUT_NB*33-1 downto 0) := (others => '0');
	signal selout_gat_out : std_logic_vector(33-1 downto 0) := (others => '0');
	signal selout_sca_in  : std_logic_vector(16-1 downto 0) := (others => '0');
	signal selout_sca_out : std_logic_vector(CONFIG_SELOUT_NB*16-1 downto 0) := (others => '0');

	-- The FIFO at the output of the scatter-gather component
	signal seloutfifo_clear    : std_logic := '0';
	signal seloutfifo_in_data  : std_logic_vector(32-1 downto 0) := (others => '0');
	signal seloutfifo_in_ack   : std_logic := '0';
	signal seloutfifo_in_cnt   : std_logic_vector(16-1 downto 0) := (others => '0');
	signal seloutfifo_out_data : std_logic_vector(32-1 downto 0) := (others => '0');
	signal seloutfifo_out_rdy  : std_logic := '0';
	signal seloutfifo_out_ack  : std_logic := '0';

	----------------------------------------------------
	-- Components
	----------------------------------------------------

	-- The circular buffer / FIFO component
	component fifo_with_counters is
		generic (
			DATAW : natural := 32;
			DEPTH : natural := 8;
			CNTW  : natural := 16
		);
		port (
			reset         : in  std_logic;
			clk           : in  std_logic;
			fifo_in_data  : in  std_logic_vector(DATAW-1 downto 0);
			fifo_in_rdy   : out std_logic;
			fifo_in_ack   : in  std_logic;
			fifo_in_cnt   : out std_logic_vector(CNTW-1 downto 0);
			fifo_out_data : out std_logic_vector(DATAW-1 downto 0);
			fifo_out_rdy  : out std_logic;
			fifo_out_ack  : in  std_logic;
			fifo_out_cnt  : out std_logic_vector(CNTW-1 downto 0)
		);
	end component;

	-- A distributed multiplexer
	-- Used to observe the status of the FIFOs
	component muxtree_bin is
		generic(
			WDATA : natural := 8;
			NBIN  : natural := 20;
			WSEL  : natural := 12
		);
		port(
			clk      : in  std_logic;
			-- Selection input
			sel      : in  std_logic_vector(WSEL-1 downto 0);
			-- Enable input and output
			en_in    : in  std_logic;
			en_out   : out std_logic_vector(NBIN-1 downto 0);
			-- Data input and output
			data_in  : in  std_logic_vector(NBIN*WDATA-1 downto 0);
			data_out : out std_logic_vector(WDATA-1 downto 0)
		);
	end component;

	-- Scatter-gather component to select output layer, if anabled
	component scattergather is
		generic(
			WGATHER  : natural := 8;
			WSCATTER : natural := 1;
			NBIN     : natural := 20;
			WSEL     : natural := 12;
			EGATHER  : boolean := true;
			ESCATTER : boolean := false;
			RADIX    : natural := 2;
			REGALL   : boolean := false
		);
		port(
			clk         : in  std_logic;
			-- Selection input
			sel         : in  std_logic_vector(WSEL-1 downto 0);
			-- Enable input and output
			en_in       : in  std_logic;
			en_out      : out std_logic_vector(NBIN-1 downto 0);
			-- Gather data, input and output
			gather_in   : in  std_logic_vector(NBIN*WGATHER-1 downto 0);
			gather_out  : out std_logic_vector(WGATHER-1 downto 0);
			-- Scatter data, input and output
			scatter_in  : in  std_logic_vector(WSCATTER-1 downto 0);
			scatter_out : out std_logic_vector(NBIN*WSCATTER-1 downto 0)
		);
	end component;

	-- AUTOGEN COMP DECL BEGIN

	-- AUTOGEN COMP DECL END

	-- AUTOGEN SIG DECL BEGIN

	-- AUTOGEN SIG DECL END

begin

	assert not ( (CONFIG_LOCKREGS = false) and (CONFIG_NOREGS = true) ) report "Error inconsistent setup for parameters LOCKREGS and NOREGS" severity failure;
	assert RIFFA_CHANELS_NB = 2 report "Error wrong number of RIFFA channels" severity failure;

	------------------------------------------------
	-- Interface with RIFFA chanels
	------------------------------------------------

	CHNL_RX_CLK(0)      <= CLK;
	CHNL_RX_ACK(0)      <= '1' when chan0_rx_state = RIFFA_STATE_DATA else '0';
	CHNL_RX_DATA_REN(0) <= chan0_rx_ren;

	CHNL_TX_CLK(0)  <= CLK;
	CHNL_TX(0)      <= '1' when (chan0_tx_state = RIFFA_STATE_LEN) or (chan0_tx_state = RIFFA_STATE_DATA) else '0';
	CHNL_TX_LAST(0) <= '1';
	CHNL_TX_LEN(31 downto 0) <= std_logic_vector(to_unsigned(1, 32));  -- Number of 32-bit words
	CHNL_TX_OFF(30 downto 0) <= (others => '0');
	CHNL_TX_DATA(C_PCI_DATA_WIDTH-1 downto 0) <= chan0_rdata;
	CHNL_TX_DATA_VALID(0) <= chan0_tx_valid;

	chan0_rx_ren   <= '1' when chan0_rx_state = RIFFA_STATE_DATA else '0';
	chan0_tx_valid <= '1' when chan0_tx_state = RIFFA_STATE_DATA else '0';

	process (CLK)
	begin
		if rising_edge(CLK) then

			-- These signals are active for only one clock
			slv_reg_rden <= '0';
			slv_reg_wren <= '0';

			if RST = RSTVAL_IN then

				chan0_rlen   <= (others => '0');
				chan0_rcount <= (others => '0');
				chan0_rdata  <= (others => '0');

				chan0_rx_state <= RIFFA_STATE_WAIT;
				chan0_tx_state <= RIFFA_STATE_WAIT;

				slv_reg_addr <= (others => '0');

			else

				-- RX side
				if chan0_rx_state = RIFFA_STATE_WAIT then
					-- Synchronize with TX side
					if chan0_tx_state = RIFFA_STATE_WAIT then
						chan0_rx_state <= RIFFA_STATE_LEN;
					end if;
				elsif chan0_rx_state = RIFFA_STATE_LEN then
					if CHNL_RX(0) = '1' then
						chan0_rlen     <= unsigned(CHNL_RX_LEN(31 downto 0));
						chan0_rcount   <= (others => '0');
						chan0_rx_state <= RIFFA_STATE_DATA;
					end if;
				elsif chan0_rx_state = RIFFA_STATE_DATA then
					if CHNL_RX_DATA_VALID(0) = '1' then
						chan0_rdata    <= CHNL_RX_DATA(C_PCI_DATA_WIDTH-1 downto 0);
						slv_reg_addr   <= CHNL_RX_DATA(REGADDRW-1 downto 0);
						slv_reg_wren   <= CHNL_RX_DATA(31);
						slv_reg_wrdata <= CHNL_RX_DATA(63 downto 32);
						chan0_rcount   <= chan0_rcount + (C_PCI_DATA_WIDTH/32);
					end if;
					if chan0_rcount >= chan0_rlen then
						chan0_rx_state <= RIFFA_STATE_WAIT;
						if chan0_rdata(30) = '1' then
							-- Ask the TX side to send the register value
							chan0_tx_state <= RIFFA_STATE_LEN;
						end if;
					end if;
				end if;

				-- TX side
				if chan0_tx_state = RIFFA_STATE_WAIT then
					-- The RX side says when to get out of waiting
				elsif chan0_tx_state = RIFFA_STATE_LEN then
					slv_reg_rden <= '1';
					chan0_rdata(31 downto 0) <= slv_reg_rddata;
					chan0_tx_state <= RIFFA_STATE_DATA;
				elsif chan0_tx_state = RIFFA_STATE_DATA then
					-- Only one word is sent through this channel, this is hardcoded
					if CHNL_TX_DATA_REN(0) = '1' then
						chan0_tx_state <= RIFFA_STATE_WAIT;
					end if;
				end if;

			end if;  -- Not reset
		end if;  -- Clock
	end process;

	CHNL_RX_CLK(1)      <= CLK;
	CHNL_RX_ACK(1)      <= '1' when chan1_rx_state = RIFFA_STATE_DATA else '0';
	CHNL_RX_DATA_REN(1) <= chan1_rx_ren;

	CHNL_TX_CLK(1)  <= CLK;
	CHNL_TX(1)      <= '1' when (chan1_tx_state = RIFFA_STATE_LEN) or (chan1_tx_state = RIFFA_STATE_DATA) else '0';
	CHNL_TX_LAST(1) <= '1';
	CHNL_TX_LEN(32*2-1 downto 32) <= std_logic_vector(chan1_tx_len);
	CHNL_TX_OFF(31*2-1 downto 31) <= (others => '0');
	CHNL_TX_DATA(C_PCI_DATA_WIDTH*2-1 downto C_PCI_DATA_WIDTH) <= txbuf_data;
	CHNL_TX_DATA_VALID(1) <= chan1_tx_valid;

	chan1_rx_ren   <= '1' when (chan1_rx_state = RIFFA_STATE_DATA) and (alias_rx_ready = '1') else '0';
	chan1_tx_valid <= '1' when (chan1_tx_state = RIFFA_STATE_DATA) and (alias_tx_ready = '1') else '0';

	-- The number of 32-bit words to send back through PCIe
	-- It is rounded to the upper multiple of 4, for 128-bit frontier
	-- FIXME This rounding formula could be optimized to : (nb + 3) & ~0x3
	chan1_tx_len <=
		txcnt_want when txcnt_want(1 downto 0) = 0 else
		(txcnt_want(31 downto 2) + 1) & to_unsigned(0, 2);

	process (CLK)
	begin
		if rising_edge(CLK) then

			chan1_rx_end <= '0';
			chan1_tx_end <= '0';

			if RST = RSTVAL_IN then

				chan1_rlen   <= (others => '0');
				chan1_rcount <= (others => '0');
				chan1_rdata  <= (others => '0');

				chan1_rx_state <= RIFFA_STATE_WAIT;
				chan1_tx_state <= RIFFA_STATE_WAIT;

			else

				-- RX side
				if chan1_rx_state = RIFFA_STATE_WAIT then
					chan1_rx_state <= RIFFA_STATE_LEN;
				elsif chan1_rx_state = RIFFA_STATE_LEN then
					if CHNL_RX(1) = '1' then
						chan1_rlen     <= unsigned(CHNL_RX_LEN(32*2-1 downto 32));
						chan1_rcount   <= (others => '0');
						chan1_rx_state <= RIFFA_STATE_DATA;
					end if;
				elsif chan1_rx_state = RIFFA_STATE_DATA then
					if (CHNL_RX_DATA_VALID(1) = '1') and (alias_rx_ready = '1') then
						chan1_rdata  <= CHNL_RX_DATA(C_PCI_DATA_WIDTH*2-1 downto C_PCI_DATA_WIDTH);
						chan1_rcount <= chan1_rcount + (C_PCI_DATA_WIDTH/32);
					end if;
					if chan1_rcount >= chan1_rlen then
						chan1_rx_state <= RIFFA_STATE_LEN;
						if CHNL_RX_LAST(1) = '1' then
							chan1_rx_end <= '1';
						end if;
					end if;
				end if;

				-- TX side
				if chan1_tx_state = RIFFA_STATE_WAIT then
					if (req_start_send = '1') and (txcnt_want > 0) then
						chan1_tx_state <= RIFFA_STATE_LEN;
					end if;
				elsif chan1_tx_state = RIFFA_STATE_LEN then
					if CHNL_TX_ACK(1) = '1' then
						chan1_tx_state <= RIFFA_STATE_DATA;
					end if;
				elsif chan1_tx_state = RIFFA_STATE_DATA then
					if (CHNL_TX_DATA_REN(1) = '1') and (alias_tx_ready = '1') then
						if txcnt_cur >= txcnt_want then
							chan1_tx_state <= RIFFA_STATE_WAIT;
							chan1_tx_end <= '1';
						end if;
					end if;
				end if;

			end if;  -- Not reset
		end if;  -- Clock
	end process;

	-- Alias signals to be used by the user design

	slv_reg_rdaddr <= slv_reg_addr;
	slv_reg_wraddr <= slv_reg_addr;

	alias_rx_data  <= CHNL_RX_DATA(C_PCI_DATA_WIDTH*2-1 downto C_PCI_DATA_WIDTH);
	alias_rx_valid <= CHNL_RX_DATA_VALID(1) and chan1_rx_ren;

	alias_tx_valid <= '1' when (chan1_tx_state = RIFFA_STATE_DATA) and (CHNL_TX_DATA_REN(1) = '1') else '0';


	------------------------------------------------
	-- Configuration registers for the network layers
	------------------------------------------------

	-- Combinational process that generates the next value to config registers
	process (all)
		variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
	begin

		-- Init at zero if registers are locked
		if (CONFIG_NOREGS = true) or (CONFIG_LOCKREGS = true) then
			config_regs_var := (others => '0');
		else

			config_regs_var := config_regs;

			for i in 0 to CONFIG_CHAIN_NB-1 loop
				if config_chain_set(i) = '1' then
					config_regs_var(i*32+31 downto i*32) := config_chain_regs(i*32+31 downto i*32);
				end if;
			end loop;

		end if;

		-- Apply constant bits
		config_regs_var := config_regs_apply_const(config_regs_var);

		-- Set default values, or write reset functionality
		-- Or, systematically overwrite that config if the registers are locked or not implemented
		if (config_chain_def = '1') or (CONFIG_LOCKREGS = true) or (CONFIG_NOREGS = true) then
			config_regs_var := config_regs_apply_const_locked(config_regs_var);
		end if;

		config_regs_next <= config_regs_var;

	end process;

	gen_config_regs : if (CONFIG_NOREGS = false) and (CONFIG_LOCKREGS = false) generate

		process (CLK)
		begin
			if rising_edge(CLK) then
				config_regs <= config_regs_next;
			end if;
		end process;

	end generate;

	gen_config_noregs : if (CONFIG_NOREGS = true) or (CONFIG_LOCKREGS = true) generate
		config_regs <= config_regs_next;
	end generate;

	-- If the network config is locked, then the config is stored into a small memory
	gen_config_read_rom : if (CONFIG_NOREGS = false) and (CONFIG_LOCKREGS = true) generate

		type config_regs_rom_type is array (0 to CONFIG_CHAIN_NB-1) of std_logic_vector(31 downto 0);

		function config_regs_gen_mem_init(phony : boolean) return config_regs_rom_type is
			variable config_regs_var : std_logic_vector(CONFIG_CHAIN_NB*32-1 downto 0) := (others => '0');
			variable rom_init_var : config_regs_rom_type := (others => (others => '0'));
		begin
			config_regs_var := (others => '0');
			config_regs_var := config_regs_apply_const(config_regs_var);
			config_regs_var := config_regs_apply_const_locked(config_regs_var);
			for i in 0 to CONFIG_CHAIN_NB-1 loop
				rom_init_var(i) := config_regs_var(i*32+31 downto i*32);
			end loop;
			return rom_init_var;
		end function;

		constant config_regs_rom : config_regs_rom_type := config_regs_gen_mem_init(false);

		constant READ_INDEX_SIZE : natural := storebitsnb(CONFIG_CHAIN_NB);
		signal read_index : unsigned(READ_INDEX_SIZE-1 downto 0) := (others => '0');

	begin

		-- Assign the read word
		config_regs_read <= config_regs_rom(to_integer(read_index));

		-- Update the word index
		process (CLK)
		begin
			if rising_edge(CLK) then

				if config_chain_shift(0) = '1' then
					-- Apply resizing in order to avoid simulation errors due to overflow
					read_index <= resize(resize(read_index, READ_INDEX_SIZE+1) + 1, READ_INDEX_SIZE);
				end if;

				if (config_chain_get(0) = '1') or (reset_reg = RSTVAL_GEN) then
					read_index <= (others => '0');
				end if;

			end if;
		end process;

	end generate;

	-- If the network config is not locked, then the config is read from the registers
	gen_config_read_chain : if (CONFIG_NOREGS = false) and (CONFIG_LOCKREGS = false) generate

		-- Assign the read word
		config_regs_read <= config_chain_regs(31 downto 0);

	end generate;


	------------------------------------------------
	-- Main control of registers, data transfers, etc
	------------------------------------------------

	-- Alias signals
	cur_recv1 <= slv_reg4(5 downto 0);
	cur_recv2 <= slv_reg4(15 downto 6);
	cur_send  <= slv_reg4(23 downto 16);

	cur_freerun <= slv_reg3(1);

	txcnt_want <= unsigned(slv_reg6);

	-- Main sequential process: write to config registers, implement all synchronous registers
	process (CLK)
		variable tmpvar_slv_reg         : std_logic_vector(31 downto 0) := (others => '0');
		variable tmpvar_slv_reg_mask_we : std_logic_vector(31 downto 0) := (others => '0');
	begin
		if rising_edge(CLK) then

			-- Hold reset active for a certain duration
			if reset_counter > 0 then
				reset_counter <= reset_counter - 1;
				reset_reg <= RSTVAL_GEN;
			else
				reset_reg <= not RSTVAL_GEN;
			end if;
			-- Generate reset
			if RST = RSTVAL_IN then
				reset_counter <= to_unsigned(RESET_DURATION, reset_counter'length);
				reset_reg <= RSTVAL_GEN;
			end if;

			-- Default/reset assignments
			req_start_recv <= '0';
			req_start_send <= '0';

			-- Buffers for the main data channel, read direction
			rxbuf_data  <= rxbuf_data_n;
			rxbuf_nbits <= rxbuf_nbits_n;
			rxcnt_cur   <= rxcnt_cur_n;

			-- Buffers for output registers
			txbuf_data  <= txbuf_data_n;
			txbuf_nbits <= txbuf_nbits_n;
			txcnt_cur   <= txcnt_cur_n;
			txrun_en    <= txrun_en_n;
			txrun_gotall <= txrun_gotall_n;

			-- Commands for config registers
			config_chain_shift <= (others => '0');
			config_chain_get   <= (others => '0');
			config_chain_set   <= (others => '0');
			config_chain_def   <= '0';
			if reset_reg = RSTVAL_GEN then
				config_chain_def <= '1';
			end if;

			-- Bufferize the flag to select the output layer, scatter-gather or last FIFO
			send_is_last <= '0';
			if (CONFIG_SELOUT = false) or (cur_send = CST_SEND_DEFAULT) then
				send_is_last <= '1';
			end if;

			-- Write to slave register
			if slv_reg_wren = '1' then
				case slv_reg_wraddr is

					when b"0000" =>
						-- Slave register 0
						-- (read only)
						-- slv_reg0 <= slv_reg_wrdata;

					when b"0001" =>
						-- Slave register 1
						-- Write configuration register
						slv_reg1 <= slv_reg_wrdata;
						-- Shift configuration registers
						if slv_reg3(8) = '1' then
							config_chain_shift <= (others => '1');
						end if;

					when b"0010" =>
						-- Slave register 2
						slv_reg2 <= slv_reg_wrdata;

					when b"0011" =>
						-- Slave register 3
						-- Misc status & control flags. Only some bits are writable.
						tmpvar_slv_reg_mask_we := x"00000102";
						tmpvar_slv_reg := (slv_reg_wrdata and tmpvar_slv_reg_mask_we) or (slv_reg3 and not tmpvar_slv_reg_mask_we);
						slv_reg3 <= tmpvar_slv_reg;

						-- Detect the clear requests
						if slv_reg_wrdata(0) = '1' then
							reset_counter <= to_unsigned(RESET_DURATION, reset_counter'length);
							reset_reg <= RSTVAL_GEN;
						end if;

						-- Config registers
						if CONFIG_NOREGS = false then
							-- Config registers: Get request
							if slv_reg_wrdata(9) = '1' then
								config_chain_get <= (others => '1');
							end if;
							-- Config registers: Set request
							if slv_reg_wrdata(10) = '1' then
								config_chain_set <= (others => '1');
							end if;
						end if;

					when b"0100" =>
						-- Slave register 4
						slv_reg4 <= slv_reg_wrdata;

					when b"0101" =>
						-- Slave register 5
						slv_reg5 <= slv_reg_wrdata;

					when b"0110" =>
						-- Slave register 6
						-- Write the number of result values to send to the main data channel
						slv_reg6 <= slv_reg_wrdata;

						-- Generate a pulse to start sending results on main data channel + clear regs
						req_start_send <= '1';

					when b"0111" =>
						-- Slave register 7
						slv_reg7 <= slv_reg_wrdata;

					when b"1000" =>
						-- Slave register 8
						slv_reg8 <= slv_reg_wrdata;

					when b"1001" =>
						-- Slave register 9
						slv_reg9 <= slv_reg_wrdata;

					when b"1010" =>
						-- Slave register 10
						slv_reg10 <= slv_reg_wrdata;

					when b"1011" =>
						-- Slave register 11
						slv_reg11 <= slv_reg_wrdata;

					when b"1100" =>
						-- Slave register 12
						slv_reg12 <= slv_reg_wrdata;

					when b"1101" =>
						-- Slave register 13
						slv_reg13 <= slv_reg_wrdata;

					when b"1110" =>
						-- Slave register 14
						slv_reg14 <= slv_reg_wrdata;

					when b"1111" =>
						-- Slave register 15
						slv_reg15 <= slv_reg_wrdata;

					when others =>

				end case;  -- Address
			end if;  -- Write enable

			-- Logic for config registers
			if CONFIG_NOREGS = false then

				-- Read config register
				if (slv_reg_rden = '1') and (unsigned(slv_reg_rdaddr) = 1) and (slv_reg3(8) = '1') then
					config_chain_shift <= (others => '1');
				end if;

			end if;

			-- Logic for config registers
			if (CONFIG_NOREGS = false) and (CONFIG_LOCKREGS = false) then

				-- Config registers: operations shift, get, set
				for i in 0 to CONFIG_CHAIN_NB-1 loop
					-- Shift
					if config_chain_shift(i) = '1' then
						-- The slave register 1 is fed at the end of the chain
						if i < CONFIG_CHAIN_NB - 1 then
							config_chain_regs(i*32+31 downto i*32) <= config_chain_regs((i+1)*32+31 downto (i+1)*32);
						else
							config_chain_regs(i*32+31 downto i*32) <= slv_reg1;
						end if;
					end if;
					-- Get
					if config_chain_get(i) = '1' then
						config_chain_regs(i*32+31 downto i*32) <= config_regs(i*32+31 downto i*32);
					end if;
					-- Set : this is implemented in separate process
				end loop;

			end if;  -- (CONFIG_NOREGS = false) and (CONFIG_LOCKREGS = false)

			if reset_reg = RSTVAL_GEN then

				-- Default: initialize at zero
				slv_reg0  <= (others => '0');
				slv_reg1  <= (others => '0');
				slv_reg2  <= (others => '0');
				slv_reg3  <= (others => '0');
				slv_reg4  <= (others => '0');
				slv_reg5  <= (others => '0');
				slv_reg6  <= (others => '0');
				slv_reg7  <= (others => '0');
				slv_reg8  <= (others => '0');
				slv_reg9  <= (others => '0');
				slv_reg10 <= (others => '0');
				slv_reg11 <= (others => '0');
				slv_reg12 <= (others => '0');
				slv_reg13 <= (others => '0');
				slv_reg14 <= (others => '0');
				slv_reg15 <= (others => '0');

				-- The accelerator ID
				slv_reg0 <=
					std_logic_vector(to_unsigned(VERSION_MIN, 8)) &  -- Minor version
					std_logic_vector(to_unsigned(VERSION_MAJ, 8)) &  -- Major version
					std_logic_vector(to_unsigned(78, 8)) &  -- ASCII for N
					std_logic_vector(to_unsigned(78, 8));   -- ASCII for N

				slv_reg3(2)            <= bool_to_logic(CONFIG_SELOUT);
				slv_reg3(3)            <= bool_to_logic(CONFIG_FIFOMON);
				slv_reg3( 7 downto  4) <= std_logic_vector(to_unsigned(storebitsnb(WDATA_ROUND_POW2)-1, 4));
				slv_reg3(11)           <= bool_to_logic(CONFIG_LOCKREGS);
				slv_reg3(17 downto 12) <= std_logic_vector(to_unsigned(CONFIG_IFW / 8, 6));
				slv_reg3(31 downto 24) <= std_logic_vector(to_unsigned(CONFIG_CHAIN_NB, 8));

				slv_reg4( 5 downto  0) <= CST_RECV_FRAME;
				slv_reg4(23 downto 16) <= CST_SEND_DEFAULT;

			end if;  -- Reset

		end if;  -- Clock
	end process;

	-- Combinatorial process - Control signals to receive items from main data channel or from registers
	process (all)
		variable var_buf_end : boolean := false;
	begin

		var_buf_end := false;

		-- Defaults for FIFO channels : no operation
		alias_rx_ready <= '0';
		inst_firstfifo_in_ack <= '0';

		-- Select bits from the buffer of the main data channel
		for i in 0 to FIRSTFIFO_PAR-1 loop
			inst_firstfifo_in_data((i+1)*FIRSTFIFO_DATAW-1 downto i*FIRSTFIFO_DATAW) <= rxbuf_data(i*WDATA_ROUND_POW2+FIRSTFIFO_DATAW-1 downto i*WDATA_ROUND_POW2);
		end loop;

		-- Default next values for registers
		rxbuf_data_n  <= rxbuf_data;
		rxbuf_nbits_n <= rxbuf_nbits;
		rxcnt_cur_n   <= rxcnt_cur;

		-- Receive items from main data channel or from writes to registers
		if cur_recv1 = CST_RECV_FRAME then

			-- There is at least one item in the buffer
			if rxbuf_nbits >= WDATA_ROUND_POW2 * INPAR_ROUND_POW2 then
				-- Validate data for first FIFO
				inst_firstfifo_in_ack <= '1';
				if inst_firstfifo_in_rdy = '1' then
					-- Shift the data buffer
					if CONFIG_IFW > WDATA_ROUND_POW2*INPAR_ROUND_POW2 then
						rxbuf_data_n(rxbuf_data'high-WDATA_ROUND_POW2*INPAR_ROUND_POW2 downto 0) <= rxbuf_data(rxbuf_data'high downto WDATA_ROUND_POW2*INPAR_ROUND_POW2);
					end if;
					-- Update the counters
					rxbuf_nbits_n <= rxbuf_nbits - WDATA_ROUND_POW2 * INPAR_ROUND_POW2;
					rxcnt_cur_n   <= rxcnt_cur + FIRSTFIFO_PAR;
					-- Detect when the buffer becomes empty
					if rxbuf_nbits < 2 * WDATA_ROUND_POW2 * INPAR_ROUND_POW2 then
						var_buf_end := true;
					end if;
				end if;
			end if;  -- End send items to the NN

			-- Get the next data buffer
			if (rxbuf_nbits < WDATA_ROUND_POW2 * INPAR_ROUND_POW2) or (var_buf_end = true) then

				-- Clear the bit counter
				rxbuf_nbits_n <= to_unsigned(0, rxbuf_nbits_n'length);

				-- Fill the next data buffer directly from the main data channel
				alias_rx_ready <= '1';
				if alias_rx_valid = '1' then
					rxbuf_data_n  <= alias_rx_data;
					rxbuf_nbits_n <= to_unsigned(CONFIG_IFW, rxbuf_nbits_n'length);
				end if;

			end if;  -- End get the next data buffer

		-- Handle when the received data is to write config into layers
		else

			-- Indicate to the main data channel that we are always ready to get the next buffer
			alias_rx_ready <= '1';

		end if;

		-- Handle reset
		if reset_reg = RSTVAL_GEN then

			alias_rx_ready <= '0';
			inst_firstfifo_in_ack <= '0';

			rxbuf_nbits_n <= to_unsigned(0, rxbuf_nbits_n'length);
			rxcnt_cur_n   <= to_unsigned(0, rxcnt_cur_n'length);

		end if;

	end process;

	-- Combinatorial process - Control signals for output values
	process (all)
		variable var_buf_end : boolean := false;
	begin

		var_buf_end := false;

		-- Default next values for registers
		txbuf_data_n  <= txbuf_data;
		txbuf_nbits_n <= txbuf_nbits;
		txcnt_cur_n   <= txcnt_cur;
		txrun_en_n    <= txrun_en;
		txrun_gotall_n <= txrun_gotall;

		-- Defaults for FIFO channels : no operation
		alias_tx_ready        <= '0';
		inst_lastfifo_out_ack <= '0';
		seloutfifo_out_ack    <= '0';

		if cur_freerun = '1' then

			-- Free run mode: accept all output values, count them and drop them
			inst_lastfifo_out_ack <= '1';
			seloutfifo_out_ack <= '1';
			if (send_is_last = '1' and inst_lastfifo_out_rdy = '1') or (send_is_last = '0' and seloutfifo_out_rdy = '1') then
				txcnt_cur_n <= txcnt_cur + 1;
			end if;

		elsif txrun_en = '0' then

			txbuf_nbits_n <= (others => '0');

			if (req_start_send = '1') and (txcnt_want > 0) then
				txrun_en_n <= '1';
				txcnt_cur_n <= (others => '0');
			end if;

		else

			-- Send the buffer to the main data channel
			if (txbuf_nbits = CONFIG_IFW) or (txcnt_cur >= txcnt_want) then
				alias_tx_ready <= '1';
				if alias_tx_valid = '1' then
					txbuf_nbits_n <= to_unsigned(0, txbuf_nbits_n'length);
					var_buf_end := true;
				end if;
			end if;

			-- Get the output values out of the FIFO, enqueue them in the output buffer
			if ((txbuf_nbits < CONFIG_IFW) or (var_buf_end = true)) and (txcnt_cur < txcnt_want) then
				inst_lastfifo_out_ack <= '1';
				seloutfifo_out_ack <= '1';
				if (send_is_last = '1' and inst_lastfifo_out_rdy = '1') or (send_is_last = '0' and seloutfifo_out_rdy = '1') then
					-- Shift the data buffer
					if CONFIG_IFW > 32 then
						txbuf_data_n(txbuf_data'high-32 downto 0) <= txbuf_data(txbuf_data'high downto 32);
					end if;
					if send_is_last = '1' then
						txbuf_data_n(txbuf_data'high downto txbuf_data'high-31) <= std_logic_vector(resize(signed(inst_lastfifo_out_data), 32));
					else
						txbuf_data_n(txbuf_data'high downto txbuf_data'high-31) <= seloutfifo_out_data;
					end if;
					-- Increment the output counters
					txbuf_nbits_n <= txbuf_nbits + 32;
					txcnt_cur_n   <= txcnt_cur + 1;
					-- Adapt the count when the previous buffer is precisely being sent to the main data channel
					if var_buf_end = true then
						txbuf_nbits_n <= to_unsigned(32, txbuf_nbits_n'length);
					end if;
				end if;
			end if;

			-- All desired results have been obtained
			if txcnt_cur >= txcnt_want then
				txrun_en_n <= '0';
				txrun_gotall_n <= '1';
			end if;

		end if;

		-- Fill the TX channel with junk data, to ensure the interface generates a last flush operation
		if txrun_gotall = '1' then
			alias_tx_ready <= '1';
		end if;

		-- Handle reset
		if reset_reg = RSTVAL_GEN then

			alias_tx_ready        <= '0';
			inst_lastfifo_out_ack <= '0';
			seloutfifo_out_ack    <= '0';

			txbuf_nbits_n <= (others => '0');
			txcnt_cur_n   <= (others => '0');
			txrun_en_n    <= '0';
			txrun_gotall_n <= '0';

		end if;

	end process;

	-- Combinatorial process - Read register, it's a big MUX
	process (all)
	begin

		slv_reg_rddata <= (others => '0');

		-- Address decoding for reading registers
		case slv_reg_rdaddr is

			when b"0000" =>
				-- Slave register 0
				slv_reg_rddata <= slv_reg0;

			when b"0001" =>
				-- Slave register 1
				--slv_reg_rddata <= slv_reg1;

				-- Read the first config register, or the memory of network config
				slv_reg_rddata <= config_regs_read;

			when b"0010" =>
				-- Slave register 2
				slv_reg_rddata <= slv_reg2;

			when b"0011" =>
				-- Slave register 3
				slv_reg_rddata    <= slv_reg3;
				slv_reg_rddata(0) <= reset_reg;

			when b"0100" =>
				-- Slave register 4
				slv_reg_rddata <= slv_reg4;

			when b"0101" =>
				-- Slave register 5
				slv_reg_rddata <= slv_reg5;

			when b"0110" =>
				-- Slave register 6
				--slv_reg_rddata <= slv_reg6;

				-- Read the number of result values that got out of the CNN
				slv_reg_rddata <= std_logic_vector(txcnt_cur);

			when b"0111" =>
				-- Slave register 7
				--slv_reg_rddata <= slv_reg7;

				-- Read the number of input values sent to the CNN input
				slv_reg_rddata <= std_logic_vector(rxcnt_cur);

			when b"1000" =>
				-- Slave register 8
				slv_reg_rddata <= slv_reg8;

			when b"1001" =>
				-- Slave register 9
				slv_reg_rddata <= slv_reg9;

			when b"1010" =>
				-- Slave register 10
				slv_reg_rddata <= slv_reg10;

			when b"1011" =>
				-- Slave register 11
				slv_reg_rddata <= slv_reg11;

			when b"1100" =>
				-- Slave register 12
				slv_reg_rddata <= slv_reg12;

			when b"1101" =>
				-- Slave register 13
				slv_reg_rddata <= slv_reg13;

			when b"1110" =>
				-- Slave register 14
				slv_reg_rddata <= slv_reg14;

			when b"1111" =>
				-- Slave register 15
				slv_reg_rddata <= slv_reg15;
				slv_reg_rddata(11 downto 0) <= monitorfifo_out;

			when others =>
				slv_reg_rddata <= (others => '0');

		end case;

	end process;


	------------------------------------------------
	-- Miscellaneous components around the network pipeline
	--------------------------------------------------

	-- The MUX to observe the state of FIFOs

	gen_fifomon: if CONFIG_FIFOMON = true generate

		fifomon : muxtree_bin
			generic map (
				WDATA => 12,
				NBIN  => CONFIG_FIFOS_NB,
				WSEL  => 8
			)
			port map (
				clk       => clk,
				-- Selection input
				sel       => slv_reg15(31 downto 24),
				-- Enable input and output
				en_in     => '1',
				en_out    => open,
				-- Data input and output
				data_in   => monitorfifo_in,
				data_out  => monitorfifo_out
			);

	end generate;

	-- The scatter-gather component to select the output layer

	gen_selout: if CONFIG_SELOUT = true generate

		scagat : scattergather
			generic map (
				WGATHER  => 33,
				WSCATTER => 16,
				NBIN     => CONFIG_SELOUT_NB,
				WSEL     => 8,
				EGATHER  => true,
				ESCATTER => true,
				RADIX    => 2,
				REGALL   => true
			)
			port map (
				clk         => clk,
				-- Selection input
				sel         => cur_send,
				-- Enable input and output
				en_in       => selout_en_in,
				en_out      => selout_en_out,
				-- Gather data, input and output
				gather_in   => selout_gat_in,
				gather_out  => selout_gat_out,
				-- Scatter data, input and output
				scatter_in  => selout_sca_in,
				scatter_out => selout_sca_out
			);

		selout_en_in  <= not send_is_last;

		-- Warning: Simply subtracting 8 (or 16 or 32) to the count created bugs,
		-- the FIFO can still get full when the output is stalled, and an overflow was sent to the layers, which didn't stop
		-- The following solution seems OK
		selout_sca_in <= seloutfifo_in_cnt when unsigned(seloutfifo_in_cnt) > 24 else (others => '0');

		seloutfifo_in_data <= selout_gat_out(31 downto 0);
		seloutfifo_in_ack  <= selout_gat_out(32);

		fifo : fifo_with_counters
			generic map (
				DATAW => 32,
				DEPTH => 64,
				CNTW  => 16
			)
			port map (
				clk           => CLK,
				reset         => seloutfifo_clear,
				fifo_in_data  => seloutfifo_in_data,
				fifo_in_rdy   => open,
				fifo_in_ack   => seloutfifo_in_ack,
				fifo_in_cnt   => seloutfifo_in_cnt,
				fifo_out_data => seloutfifo_out_data,
				fifo_out_rdy  => seloutfifo_out_rdy,
				fifo_out_ack  => seloutfifo_out_ack,
				fifo_out_cnt  => open
			);

		seloutfifo_clear <= '1' when reset_reg = RSTVAL_GEN else '0';

	end generate;


	-- AUTOGEN COMP INST BEGIN

	-- AUTOGEN COMP INST END


end architecture;

