
#================================================
# Synthesis with Xilinx Vivado
#================================================

FILENAME_BIT    = VC709Gen2x8If128.bit
FILENAME_XCI128 = ip128/PCIeGen2x8If128.xci
FILENAME_XCI64  = ip64/PCIeGen1x8If64.xci

# This variable can be overridden when calling make
RUNDIR = run

.ONESHELL:
.PHONY: all test-xci localrun postroute manual pcieupd clean \
	rundir-copy rundir-exec rundir-all \
	screen-exec screen-all

all : rundir-all

test-xci:
	@if [ ! -f $(FILENAME_XCI128) ]; then \
		echo "Error: Missing file $(FILENAME_XCI64) -> create it by copying/renaming the appropriate files for your Vivado version"; \
		exit 1; \
	fi;
	@if [ ! -f $(FILENAME_XCI64) ]; then \
		echo "Error: Missing file $(FILENAME_XCI64) -> create it by copying/renaming the appropriate files for your Vivado version"; \
		exit 1; \
	fi;

rundir-copy:
	mkdir -p $(RUNDIR)/hdl
	mkdir -p $(RUNDIR)/hdl/nn
	mkdir -p $(RUNDIR)/hdl/hwopt
	mkdir -p $(RUNDIR)/hdl/riffa
	mkdir -p $(RUNDIR)/hdl/uart2iic
	cp hdl/*.v* $(RUNDIR)/hdl/
	cp ../../hdl/nn/*.vhd       $(RUNDIR)/hdl/nn/
	cp ../../hdl/nn-lns/*.vhd   $(RUNDIR)/hdl/nn/
	cp ../../hwopt/hdl/*.vhd    $(RUNDIR)/hdl/hwopt/
	cp ../../hdl/riffa/*.v*     $(RUNDIR)/hdl/riffa/
	cp ../../hdl/uart2iic/*.vhd $(RUNDIR)/hdl/uart2iic/
	cp *.tcl *.xdc Makefile $(RUNDIR)/
	git log -n 1 | head -n 1 > $(RUNDIR)/commit-info.txt
	git status >> $(RUNDIR)/commit-info.txt
	$(MAKE) -C $(RUNDIR) clean

rundir-exec: test-xci
	$(MAKE) -C $(RUNDIR) clean
	$(MAKE) -C $(RUNDIR) localrun

rundir-all: test-xci
	$(MAKE) rundir-copy
	$(MAKE) -C $(RUNDIR) localrun

# FIXME Create an equivalent command for tmux
screen-exec:
	screen -m -S $(RUNDIR) make RUNDIR="$(RUNDIR)" rundir-exec

screen-all:
	screen -m -S $(RUNDIR) make RUNDIR="$(RUNDIR)" rundir-all


#================================================
# Launch Vivado
#================================================

localcopy:
	$(MAKE) RUNDIR=$$(basename $$PWD) -C .. rundir-copy

localrun:
	vivado -mode batch -messageDb vivado.pb -log vivado.vdi -source project.tcl

# Launch post-route optimization
postroute:
	vivado -mode batch -messageDb vivado.pb -log vivado.vdi -source postroute.tcl

# Launch commands from a manually-created TCL file. Useful to start from a particular checkpoint.
manual:
	vivado -mode batch -messageDb vivado.pb -log vivado.vdi -source manual.tcl

# Launch commands from a manually-created TCL file. Useful to start from a particular checkpoint.
pcieupd:
	vivado -mode batch -messageDb vivado.pb -log vivado.vdi -source pcieupd.tcl

clean:
	rm -rf *.jou *.log *.pb *.vdi *.bit *.dcp .*.rst *.rpt *.os .Xil \
	doc sim source synth \
	PCIeGen2x8If128_* PCIeGen2x8If128.veo PCIeGen2x8If128.xml


#================================================
# Program the FPGA with xc3sprog
#================================================

.PHONY: program xc3sprog-program

# This variable can be overridden when calling make
BITSTREAM = $(shell ls *.bit | head -n 1)

program: xc3sprog-program
xc3sprog-program:
	xc3sprog -c jtaghs1_fast -v -p 0 $(BITSTREAM):w::bit

