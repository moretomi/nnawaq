
# Note: This is a non-project script
# For more information, please refer to the Xilinx document UG894
# "Vivado Design Suite User Guide: Using TCL Scripting", version 2014.1, page 11
# For details about the TCL commands, please refer to the Xilinx document UG835
# "Vivado Design Suite Tcl Command Reference Guide"

# Get interface width from VHDL file
#set vhdl_ifw [exec sh -c "cat hdl/cnn_wrapper.vhd | grep 'constant CONFIG_IFW' | head -n 1 | tr -d ':=;' | awk '{print \$4}'"]
set vhdl_ifw 64
if { "$vhdl_ifw" eq "" } {
	puts "ERROR: Can't find PCIe interface width"
	exit 1
}

set pciegen 0
set pciesz 0

if { $vhdl_ifw eq 64 } {
	set pciegen 1
	set pciesz $vhdl_ifw
}
if { $vhdl_ifw eq 128 } {
	set pciegen 2
	set pciesz $vhdl_ifw
}
if { $pciegen eq 0 } {
	puts "ERROR: Invalid PCIe interface width: $vhdl_ifw"
	exit 1
}

# Miscellaneous useful variables
#set top VC709Gen${pciegen}x8If$pciesz
set top cnn_wrapper

# To find the available parts
#foreach p [get_parts] { puts $p }

#set part xc7v2000tflg1925-2
#set part xc7vx1140tflg1930-2
# This is board Zybo
set part xc7z010clg400-1
# This is board VC709
#set part xc7vx690tffg1761-2
# This is board VCU128
#set part xcvu37p-fsvh2892-2L-e
# This is board ZCU102
#set part xczu9eg-ffvb1156-2-e

# Define the output directory area. Mostly for debug purposes.
set outputDir "./"
file mkdir $outputDir

# Set target part to fake current project, to read IPs
# More details: http://www.xilinx.com/support/answers/54317.html
set_part $part
set_property part $part [current_project]

set riffadir ./hdl/riffa
set pmbusdir ./hdl/uart2iic
set hwoptdir ./hdl/hwopt
set nndir    ./hdl/nn

# Setup design sources

set sources_v [concat \
	[glob ./hdl/*.v] \
]

#foreach f $sources_vhd {
#	read_verilog $f
#}

set sources_vhd [concat \
	[glob ./hdl/*.vhd] \
	[glob $pmbusdir/*.vhd] \
	[glob $hwoptdir/*.vhd] \
	[glob $nndir/*.vhd] \
]

foreach f $sources_vhd {
	read_vhdl -vhdl2008 $f
}

# Setup constraints
#read_xdc ../VC709_Top.xdc

# No limit for message: Sequential element unused
set_msg_config -id {[Synth 8-3333]} -limit 1000000
# No limit for message: Constant propagation
set_msg_config -id {[Synth 8-3332]} -limit 1000000
# No limit for message: Unconnected port
set_msg_config -id {[Synth 8-3331]} -limit 1000000
# No limit for message: Tying undriven pin to constant 0
set_msg_config -id {[Synth 8-3295]} -limit 1000000
# Hide messages: Sub-optimal pipelining for BRAM
set_msg_config -id {[Synth 8-4480]} -suppress

# Run logic synthesis
#synth_design -top $top -keep_equivalent_registers -resource_sharing on -generic C_PCI_DATA_WIDTH=64
#synth_design -top $top -keep_equivalent_registers -flatten_hierarchy full -resource_sharing on
#synth_design -top $top -keep_equivalent_registers -resource_sharing on -directive AreaOptimized_high

#synth_design -top adderlut6_7in_ter -keep_equivalent_registers -resource_sharing on
synth_design -top distriadd -keep_equivalent_registers -resource_sharing on -generic WDATA=1 -generic SDATA=false -generic NBIN=1024 -generic BINARY=true -generic REGEN=false
#synth_design -top distriadd -keep_equivalent_registers -resource_sharing on -generic WDATA=2 -generic SDATA=true -generic NBIN=16 -generic TERNARY=true
#synth_design -top distriadd -keep_equivalent_registers -resource_sharing on -generic WDATA=16 -generic SDATA=false -generic NBIN=16 -generic RADIX=0

# Run logic optimization
opt_design
#opt_design -directive ExploreArea

# Report timing and utilization estimates
report_utilization    -file $outputDir/post_synth_util.rpt -packthru
report_timing_summary -file $outputDir/post_synth_timing.rpt -report_unconstrained

# Write design checkpoint
write_checkpoint -force $outputDir/post_synth.dcp

# Note: For debug purposes, if you want to start from post- logic synthesis state,
# uncomment these lines and remove all above commands (except setting of parameters)
#read_checkpoint $outputDir/post_synth.dcp
#link_design -top $top -part $part

exit 0


# Use this option if you really want Vivado to try
set_param place.skipUtilizationCheck 1

# Run automated placement of the remaining of the design
place_design
#place_design -directive Explore
#place_design -directive WLDrivenBlockPlacement
#place_design -directive ExtraNetDelay_high

# Physical logic optimization
phys_opt_design -directive AggressiveExplore

# Report utilization and timing estimates
report_utilization       -file $outputDir/post_place_util.rpt -packthru
report_clock_utilization -file $outputDir/clock_util.rpt
report_timing_summary    -file $outputDir/post_place_timing.rpt -report_unconstrained

# Write design checkpoint
write_checkpoint -force $outputDir/post_place.dcp



# Note: For debug purposes, if you want to start from post- placement state,
# uncomment these lines and remove all above commands (except setting of parameters)
#read_checkpoint $outputDir/post_place.dcp
#link_design -top $top -part $part

# Run first routing
#route_design
route_design -tns_cleanup -directive Explore
#route_design -tns_cleanup -preserve

# Note: For more optimization, perform another placement optimization and re-route
#place_design -post_place_opt
#phys_opt_design -directive AggressiveExplore
#route_design

# Report the routing status, timing, power, design rule check
report_route_status -file $outputDir/post_route_status.rpt

report_utilization    -file $outputDir/post_route_util.rpt -packthru
report_timing_summary -file $outputDir/post_route_timing.rpt -report_unconstrained
report_power          -file $outputDir/post_route_power.rpt
report_drc            -file $outputDir/post_route_drc.rpt

# Write the post-route design checkpoint
write_checkpoint -force $outputDir/post_route.dcp



# Note: For debug purposes, if you want to start from post- routing state,
# uncomment these lines and remove all above commands (except setting of parameters)
#read_checkpoint $outputDir/post_route.dcp
#link_design -top $top -part $part

# Optionally generate post- routing simulation model
#set postparDir $outputDir/vhdl-postpar/
#file mkdir $postparDir
#write_vhdl -force $postparDir/top.vhd
#write_sdf -force $postparDir/top.sdf

# Generate the bitstream
write_bitstream -force $outputDir/$top.bit

